#!/bin/bash
# Bryant Hansen

# ./drawCalendar.sh "Dec 6" "Dec 20"
# dates must be compatible with the bash date --date="..." utility

# early single-liners that inspired the script:
# convert -size 280x180 xc:black -fill white -font /usr/share/fonts/corefonts/cour.ttf -pointsize 20 -draw "text 20,20 '$(cal)'" test.png
# convert -size 280x180 xc:black -fill "#808080" -draw "roundrectangle 60,60 100,100 10,10" -fill white -font /usr/share/fonts/corefonts/cour.ttf -pointsize 20 -draw "text 20,20 '$(cal)'" test.png
# day of date goes in, line number in cal output comes out
# for dt in `seq 1 31` ; do echo -n "${dt}: " ; cal | grep -n " $dt \|^${dt} \| ${dt}$" ; done
# for firstDay in `seq 1 31` ; do echo -e -n "day=${firstDay}:\t" ; c=0 ; line="$(cal | grep " $firstDay \|^${firstDay} \| ${firstDay}$")" ; echo -e -n "line=$line \t" ; prefix="${line%${firstDay}*}" ; prefixLen="${#prefix}" ; echo "prefix=\"${prefix}\"  startPos=$prefixLen  endPos=$(expr $prefixLen + ${#firstDay})" ; done
# cal | while read l ; do echo "${l##* }" ; done
# cal | while read l ; do echo "${l%% *}" ; done

###################################################
# Default Params
###################################################

FONT="/usr/share/fonts/corefonts/cour.ttf"
FONT_SIZE=20
OUTFILE="test3.png"

firstDay=25
lastDay=28

[[ "$1" ]] && firstDay=$1
[[ "$2" ]] && lastDay=$2


###################################################
# Functions
###################################################

getFontMetrics() {
    # get font metrics
    STR="$(cal | sed -n "4p")"
    numChars="${#STR}"
    #echo "numChars=$numChars" >&2
    convert -background black -fill white -font $FONT -pointsize $FONT_SIZE label:"$STR" font_metrics.png
    metricSize="$(identify -format "%wx%h" font_metrics.png)"
    heightPerCharacter="$(expr ${metricSize#*x} - 1)"
    widthPerCharacter="$(expr ${metricSize%x*} / $numChars)"
    echo "getFontMetrics: pixel-size-per-character=${widthPerCharacter}x${heightPerCharacter}" >&2
    echo "${widthPerCharacter}x${heightPerCharacter}"
}


getLineNumOfDay() {
    grep -n " $1 \|^${1} \| ${1}$" | sed "s/:.*$//"
}


firstEntry() {
    while read l ; do echo "${l%% *}" ; done | sed -n "${1}p"
}


lastEntry() {
    while read l ; do echo "${l##* }" ; done | sed -n "${1}p"
}


getStartCharOfDay() {
    c=0
    firstDay=$1
    line="$(grep " $firstDay \|^${firstDay} \| ${firstDay}$")"
    echo -e -n "line=$line \t" >&2
    prefix="${line%${firstDay}*}"
    prefixLen="${#prefix}"
    startPos=$prefixLen
    endPos=$(expr $prefixLen + ${#firstDay})
    echo "char=$1  startPos=$prefixLen  endPos=$(expr $prefixLen + ${#firstDay})" >&2
    echo $startPos
}


setHighlight() {
    local firstDay=$1
    local lastDay=$2

    echo "setHighlight: firstDay=$firstDay  lastDay=$lastDay" >&2

    # calText="$(while read line ; do echo "$line" ; done)"
    calText="$(cal)"

    fontSize="$(getFontMetrics)"
    charWidth="${fontSize%x*}"
    charHeight="${fontSize#*x}"
    echo "charWidth=$charWidth  charHeight=$charHeight" >&2

    startLineNum=$(echo "$calText" | getLineNumOfDay $firstDay)
      endLineNum=$(echo "$calText" | getLineNumOfDay $lastDay)

    startCharNum=$(echo "$calText" | getStartCharOfDay $firstDay)
     lastCharNum=$(echo "$calText" | getStartCharOfDay $lastDay)
     lastCharNum=$(expr $lastCharNum + ${#lastDay} + 1)

    echo "startCharNum=$startCharNum  lastCharNum=$lastCharNum" >&2

    echo "startLineNum=$startLineNum  endLineNum=$endLineNum" >&2
    if [[ $startLineNum != $endLineNum ]] ; then
        echo "startLineNum ($startLineNum) is not equal to endLineNum ($endLineNum)" >&2
        echo "  multi-line is not yet implemented" >&2
    fi

    lineNum=$startLineNum
    startChar=6
    endChar=15
    startChar=$startCharNum
    endChar=$lastCharNum
    topMargin=2
    leftMargin=12

     highlightTop=$(expr $topMargin  + $charHeight \* $(expr $lineNum - 1))
    highlightLeft=$(expr $leftMargin + $charWidth  \* $startChar)
  highlightBottom=$(expr $topMargin  + $charHeight \* $lineNum)
   highlightRight=$(expr $leftMargin + $charWidth  \* $endChar)

    cornerRadius=6

    echo "roundrectangle ${highlightLeft},${highlightTop} ${highlightRight},${highlightBottom} ${cornerRadius},${cornerRadius}"
}

drawHighlightedMonth() {

    local month="$1"
    local firstDay=$2
    local lastDay=$3

    if [[ "$lastDay" -lt "$firstDay" ]] ; then
        echo "drawHighlightedMonth ERROR: lastDay is before firstDay" >&2
        echo "Exiting abnormally." >&2
        
    fi

    echo "firstDay=$firstDay  lastDay=$lastDay" >&2
    calText="$(cal)"

    echo "Calendar:" >&2
    echo "$calText" | sed "s/^/\ \ /" >&2

    startLineNum=$(echo "$calText" | getLineNumOfDay $firstDay)
      endLineNum=$(echo "$calText" | getLineNumOfDay $lastDay)

    CCMD="convert \t\
        -size 280x180 \t\
        xc:black \t\
        -fill \"#808080\" \t\
    "
    if [[ $startLineNum == $endLineNum ]] ; then
        echo -e "$calText" | lastEntry $startLineNum
        highlight="$(setHighlight $firstDay $lastDay)"
        echo "highlight=$highlight" >&2
        CCMD="$CCMD \
            -draw \"${highlight}\" \t\
        "
    else
        for hl in `seq $startLineNum $endLineNum` ; do
            b=$(echo -e "$calText" | lastEntry $hl)
            a=$(echo -e "$calText" | firstEntry $hl)
            [[ "$hl" == "$startLineNum" ]] && a=$firstDay
            [[ "$hl" == "$endLineNum" ]]   && b=$lastDay
            highlight="$(setHighlight $a $b)"
            echo "highlight=$highlight" >&2
            CCMD="$CCMD \
                -draw \"${highlight}\" \t\
            "
        done
    fi
    CCMD="$CCMD \
        -fill white \t\
        -font $FONT \t\
        -pointsize $FONT_SIZE \t\
        -draw \"text 20,20 '${calText}'\" \t\
        "$OUTFILE" \
    "

    echo "$CCMD"
}

VERBOSE=1

day1="$(date --date="${1}" "+%d")"
day1="${day1#0}"
day2="$(date --date="${2}" "+%d")"

month1="$(date --date="${1}" "+%m %Y")"
month2="$(date --date="${2}" "+%m %Y")"

echo "day1=$day1  day2=$day2" >&2

#exit 4

CONVERT_CMD=$(drawHighlightedMonth "$month1" $day1 $day2)
SED_FORMAT_CONVERT_COMMAND="s/\t/\ \t\\\\\n\ \ /g"
[[ "$VERBOSE" ]] && (( "$VERBOSE" )) && \
    echo -e "CONVERT_CMD = ${CONVERT_CMD}" | sed "$SED_FORMAT_CONVERT_COMMAND" >&2
echo -e "$CONVERT_CMD" | /bin/bash

exit 0


# original command:
convert \
    -size 280x180 \
    xc:black \
    -fill "#808080" \
    -draw "roundrectangle 60,60 100,100 10,10" \
    -fill white \
    -font $FONT \
    -pointsize $FONT_SIZE \
    -draw "text 20,20 '$(cal)'" \
    test2.png
