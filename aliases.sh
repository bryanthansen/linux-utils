alias days_since='days_since() { expr $(date +%s --date 2017-05-23) / 3600 / 24 - $(date +%s --date "$1") / 3600 / 24 ; } ; days_since '

alias BA='nice nohup BuildAlbum >> BA_$(date "+%Y%m%d_%H%M%S").log 2>&1 &'
alias bitrate='function getAvgBitrate() { echo "scale=1 ; $(stat --format="%s" "$1") / $(mp3info -p "%S" "$1") * 8 / 1000" | bc | tr "\n" " " ; echo "kbps" ; } ; getAvgBitrate'
alias cdemand='cpufreq-set -c 0 -g ondemand    ; cpufreq-set -c 1 -g ondemand'
alias compress_logs="mv ~/emerge_world_*.log ~/revdep_rebuild_*.log ~/emerge_logs ; du -hs ~/emerge_logs/ ; time xz ~/emerge_logs/*.log ; du -sh ~/emerge_logs/"
alias cinfo='cpufreq-info'
alias cperf='cpufreq-set -c 0 -g performance ; cpufreq-set -c 1 -g performance'
alias csave='cpufreq-set -c 0 -g powersave   ; cpufreq-set -c 1 -g powersave'
alias d='date "+%Y%m%d"'
alias d='ls --color'
alias dec2hex='printf "0x%X"'
alias divide_with_rounding='_div() { echo 1="${1}"  2="${2}" ; expr $1 / $2 + $1 % $2 \* 2 / $1 ; } ; _div '
alias dmw='dmesg -wH'

# alias dtfm='do_dtfm() { date --date="$(stat --format="%y" "$1")" "+%Y%m%d_%H%M%S" ; } ; do_dtfm'

alias dtfm='do_dtfm() { fdt1="$(stat --format="%y" "$1")" ; fdt2="$(date --date="$fdt1" "+%Y%m%d_%H%M%S_%N")" ; fdt3="$(echo "cp -a ${1} .${1}.${fdt2}")" ; echo "$fdt3" ; } ; do_dtfm'

#DiskStation:/volume1/backups/reports -> datestamp_filename() { echo "mv $f ${f%.*}_$(date --reference=$f +%Y%m%d_%H%M%S).${f#*.}" ; } ; datestamp_filename vdi.lst 
#mv vdi.lst vdi_20111031_200322.lst
alias datestamp_filename='datestamp_filename() { echo "mv $1 ${1%.*}_$(date --reference=$1 +%Y%m%d_%H%M%S).${1#*.}" ; } ; datestamp_filename'


alias ea='ea() { f="$(readlink -f "$1")" ; echo "thunderbird -compose attachment=${f}" ; thunderbird -compose attachment=${f} ; } ; ea'
alias egrep='egrep --colour=auto'
#alias ep='emerge --update --deep --new-use --pretend --verbose --verbose-conflicts world'
alias ep='emerge -uDNvp --verbose-conflicts world'
alias es='emerge --sync | grep -v "/\$"'
alias fep='formatepoch'
alias fgrep='fgrep --colour=auto'
alias fnw='firefox --new-window'
alias full-rebuild='echo -e "\nStarting emerge -ev world on `date "+%Y-%m-%d %H:%M:%S"`...\n\n" >> /root/emerge_rebuild_world_`date +%Y%m%d`.log && nice nohup emerge -ev --exclude googleearth world >> "/root/emerge_rebuild_world_`date +%Y%m%d`.log" 2>&1 & sleep 5 ; tail  --follow "/root/emerge_rebuild_world_`date +%Y%m%d`.log"'
# xterm -geometry <width>x<height>+<left>+<top>
# Examples:
# xterm -geometry 100x24+0+0
# Will open an XTerm 100 chars wide, 24 chars high in the top left hand corner of the screen
alias gitdiff='kstart --ontop xterm -geometry 125x101-15+20 -e "git diff -b | colordiff | less" > /dev/null 2>&1 &'
alias grep='grep --colour=auto'
alias gt='gnome-terminal'
alias h50='history | tail -n 50'
alias hex2dec='printf "%d"'
alias hexdumpasc="hexdump -e '\"   %08.8_ax: \"  256/1 \"%_p\"' -e '\"\\n\"'"
alias hexdumpbin="hexdump -e '\"%08.8_ax: \" 8/4 \"%08x \"' -e '\" \"  32/1 \"%_p\"' -e '\"\\n\"'"
alias ipconfig='ifconfig'
alias ipt=iptables
alias iptl='echo "iptables -L -v" ; iptables -L -v ; echo -e "\niptables -t nat -L -v" ; iptables -t nat -L -v'
alias kst='kstart --ontop xterm -e'
alias l='ls -alhtr --full-time --color'
alias lastpdf='ls -alhtr ~/cups-pdf/ | tail -n 5'
alias lastsync='date +%Y%m%d_%H%M%S%z --date="$(cat /usr/portage/metadata/timestamp.chk)"'
alias listeners='netstat -anp | grep LISTEN'
alias llt='doit() { lines=20 ; if [[ "$1" ]] ; then ls -alhtr --full-time $* | tail -n $lines ; else ls -alhtr | tail -n $lines; fi } ; doit'
alias logwatch='tail -f $(find /var/log -type f -mtime -10 -not -name "*.[xgb]z") | sed "s/^/$(hostname):\ /;/.*\/usr\/sbin\/run-crons.*/d"'
alias ls='ls --color=auto'
alias mkdir='mkdir -p'
alias mpbg='playme() { xterm -e "mplayer -ontop -nosound \"${1}\"" & } ; playme'
alias mpl2='mplayVideo() { cmd="mplayer -ontop -geometry 100% -edlout \"${1}.$(date "+%Y%m%d_%H%M%S").edl\" \"${1}\"" ; echo "cmd=$cmd" ; echo $cmd | /bin/bash ; } ; mplayVideo'
alias mpl='mplayer -ontop -geometry 100%'
alias mplid='mplayer -vo null -ao null -identify -frames 0'
alias pico='nano'
#alias pico='vim'
alias playrecentfiles1='mplayer $(ls -1t files/*.mp3 | head -n 10)'
alias playrecentfiles2='doplay() { mplayer $(ls -1r files/* | head -n $1) } ; doplay '
alias plotcsv="gnuplot -p -e \"set datafile separator ','; plot '-' using 1:2 with points\""

# to script getting the screen dimensions:
#   xdpyinfo | grep "^screen\| dimensions:"
alias rdwin1='rdesktop -u "HEPTAGON\brh" -g 1440x870 -a 16 -k en-us -z WSN7072ZHR'

alias rl='readlink -f'
alias rm='rm -i'
alias sal='sa-learn --spam ~/Maildir/.Filtered-Spamassassin.Spam/cur'
alias savehist='history | gzip > .bash_history.$(date +%Y%m%d).txt.gz'
alias sc=systemctl
alias sortspam='nice nohup ~/.spamassassin/launch_exclusive.sh ~/.spamassassin/sort_spam.sh >> ~/logs/sort_spam.log 2>&1 &'
alias startcritter='DIR=/data/os/virtualbox/gentoo_vm_baseline_20120927b_20140213 ; LOGFILE=/data/os/VirtualBox/gentoo_vm_baseline_20120927b_20140213_$(date "+%Y%m%d_%H%M%S").log ; nohup VBoxHeadless -s gentoo_vm_baseline_20120927b_20140213 >> $LOGFILE 2>&1 & sleep 1 ; tail --follow $LOGFILE $DIR/Logs/VBox.log'
alias t='date "+%H%M%S"'
alias tailemerge='tail -n 20 -f /var/log/emerge.log | feda'
alias teather="watch 'dmesg | tail ; iwconfig ; ifconfig ; ping -c 1 google.com'"
alias turn_off_monitor='vbetool dpms off'
#alias u2='opts="-u -D -v -N --keep-going=y" ; dt="$(date +%Y%m%d_%H%M%S)" ; echo -e "\nStarting emerge $opts world on ${dt}...\n\n" >> /root/emerge_world_${dt}.log ; nice nohup emerge $opts world >> "/root/emerge_world_${dt}.log" 2>&1 & sleep 5 ; tail --follow "/root/emerge_world_${dt}.log"'
alias u2='opts="-u -D -v -N --keep-going=y" ; DT="$(date +%Y%m%d_%H%M%S)" ; LOGFILE="/root/emerge_world_${DT}.log" ; echo -e "\nStarting emerge $opts world on ${DT}...\n\n" >> "$LOGFILE" ; emerge $opts -f world >> /dev/null 2> "$LOGFILE" && nice nohup emerge $opts world >> "$LOGFILE" 2>&1 & sleep 5 ; tail --follow "$LOGFILE"'
alias unblank='echo 0 >/sys/class/graphics/fb0/blank'
alias vbm='VBoxManage'
alias wgo='wget -O-'

alias hdmi_to_usb1='mplayer -fs tv:// -tv driver=v4l2:width=1920:height=1080:device=/dev/video0'
alias mpusb='mplayer -fs tv:// -tv driver=v4l2:width=1920:height=1080:device=/dev/video0'
alias copyscans='mount /dev/sda1 /mnt/sda1 ; rsync -avDHu /mnt/sda1/HP*/* /data/scans/$(date +%Y%m%d_%H%M%S)/ ; umount /dev/sda1'

alias cpl='cp -al'
alias cl='function _copy_hard_link() { [[ "$2" ]] && cp -al "$1" "$2" ; [[ ! "$2" ]] && cp -al "$1" ./ ; } ; _copy_hard_link'
