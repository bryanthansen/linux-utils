#!/bin/bash
# Bryant Hansen

# initial 1-liner:
# emerge --depclean -p | grep "^\ .*-.*/\|^     omitted: " | while read l ; do [[ ! "${l}" ]] && continue ; [[ ! "${l//*omitted:*/}" ]] && printf "%s  $l\n" "$ll" ; ll="$l" ; done | sort | grep "omitted: none" | less

# second 1-liner:
# emerge --depclean -p | grep -B 3 "omitted: none" | grep -v "protected:\|--\|omitted:" | while read a ; do [[ ! "${a/*selected:*/}" ]] && echo "$la  $a" ; la="$a" ; done | sort

emerge --depclean -p | \
grep "^\ .*-.*/\|^     omitted: " | \
(
    while read l ; do
        [[ ! "${l}" ]] && continue
        [[ ! "${l//*omitted:*/}" ]] && printf "%s  $l\n" "$ll"
        [[ ! "${l//*omitted:*/}" ]] && printf "%s  $l\n" "$ll" >&2
        ll="$l"
    done
    [[ ! "${l//*omitted:*/}" ]] && printf "%s  $l\n" "$ll"
    [[ ! "${l//*omitted:*/}" ]] && printf "%s  $l\n" "$ll" >&2
    echo -e "\nSorted Results: " >&2
) | \
sort

# a possible filter:
# | \
#grep "omitted: none"
