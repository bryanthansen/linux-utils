#!/bin/bash
# Bryant Hansen

d=/sys/class/power_supply/BAT0

param_set="
    alarm
    capacity
    charge_full
    charge_full_design
    charge_now
    current_avg
    current_now
    cycle_count
    manufacturer
    model_name
    status
    temp
    type
    voltage_min_design
    voltage_now
"

dump_info() {
  clear
  date
  for f in $param_set ; do
    f2=${d}/${f}
    if [[ -f $f2 ]] ; then
      data="$(cat $f2)"
      data="${data%000}"
      printf "%20s:%s\n" "$f" "$data"
    else
      echo "# $f2 does not exist as a file." >&2
    fi
  done
}

while [[ 1 ]] ; do
  dump_info
  sleep 5
done
