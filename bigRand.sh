#!/bin/bash
# Bryant Hansen
# 20110618

# description: this function produces a double-precision random number and then scales in to the maximum value that's provided as an argument
#   The current output contains the following 4 values:
#       unscaled_random  max_random  scaled_random  max_scaled_random

USAGE="$0 MAX_VALUE"

val=0
RAND_MAX=32768
max="1"

[[ "$1" ]] && max_out="$1"

for it in {1..2} ; do
	val=$(expr $val \* $RAND_MAX)
	max=$(expr $max \* $RAND_MAX)
	val=$(expr $val + $RANDOM)
done

if [[ "$max_out" ]] ; then
	out=$(expr $val \* $max_out / $max)
	echo "$val $max $out $max_out"
else
	echo "variable \'MAX_VALUE\' not provided" >&2
	echo "USAGE: $USAGE"
fi

exit 0



# here is a test of the randomness of the function

############################################################
# first the 1-liner (with output):
############################################################

gink@silvia -> iter=1000 ; max=10 ; total=0 ; max_1=`expr $max - 1` ; for n in `seq 0 $max_1` ; do x[$n]=0 ; done ; for n in `seq 1 $iter` ; do n="$(./bigRand.sh 10 | 
cut -f 3 -d ' ')" ; total=$(expr $total + 1) ; [[ ! ${x[$n]} ]] && x[$n]=0 ; x[$n]=$(expr ${x[$n]} + 1) ; done ; for n in `seq 0 $max_1` ; do echo "x[$n]=${x[$n]}" ; 
done ; echo "iterations=$total"
x[0]=88
x[1]=99
x[2]=104
x[3]=103
x[4]=104
x[5]=99
x[6]=96
x[7]=118
x[8]=98
x[9]=91
iterations=1000


############################################################
# and formatted:
############################################################

iter=1000
max=10
total=0
max_1=`expr $max - 1`

for n in `seq 0 $max_1` ; do 
	x[$n]=0
done
for n in `seq 1 $iter` ; do 
	n="$(./bigRand.sh 10 | tail -n 1 | cut -f 3 -d ' ')"
	total=$(expr $total + 1)
	[[ ! ${x[$n]} ]] && x[$n]=0
	x[$n]=$(expr ${x[$n]} + 1)
done
for n in `seq 0 $max_1` ; do 
	echo "x[$n]=${x[$n]}"
done
echo "iterations=$total"


############################################################
# another test with more iterations and timing data:
############################################################

gink@silvia -> time ( iter=10000 ; max=10 ; total=0 ; max_1=`expr $max - 1` ; for n in `seq 0 $max_1` ; do x[$n]=0 ; done ; for n in `seq 1 $iter` ; do n="$(./bigRand.sh 
10 | cut -f 3 -d ' ')" ; total=$(expr $total + 1) ; [[ ! ${x[$n]} ]] && x[$n]=0 ; x[$n]=$(expr ${x[$n]} + 1) ; done ; for n in `seq 0 $max_1` ; do echo "x[$n]=${x[$n]}" 
; done ; echo "iterations=$total" )
x[0]=963
x[1]=1007
x[2]=998
x[3]=978
x[4]=1050
x[5]=1022
x[6]=1007
x[7]=987
x[8]=989
x[9]=999
iterations=10000

real    4m5.142s
user    0m12.165s
sys     0m50.887s

