#!/bin/sh
# Bryant Hansen

# Description
# a sort wrapper that can sort huge files which are beyond the memory of the system
# normally sort requires the entire file to be read into memory

SPLIT_DB=1
CACHE_DIR="./.cache"

if [[ "$SPLIT_DB" ]] ; then
    if ! mkdir -p "$CACHE_DIR" ; then
        echo "$0 ERROR: could not make CACHE_DIR $CACHE_DIR" >&2
        exit 2
    fi

    # read the files in
    while read line ; do
        echo "$line" >> "$CACHE_DIR"/${line:0:2}.txt
    done

    # dump them out
    if cd "$CACHE_DIR" ; then
        ls -1 "$f" | \
        sort $* | \
        while read f ; do
            [[ ! -f "$f" ]] && continue
            cat "$f" | sort $*
        done
    else
        echo "ERROR: failed to cd to CACHE_DIR $CACHE_DIR" >&2
        exit 3
    fi

else
    # just do the normal thing
    sort $*
fi
