#!/bin/sh
# Bryant Hansen
# wf8c -> echo 0 > /sys/class/graphics/fb0/blank

BLANK=4
WAKE=0

screens="$(cat  ~/.screens.lst)"
[[ "$screens" ]] || screens="host1\nhost2"

echo -e "$screens" \
| sed "s/\#.*//;/^[ \t]*$/d" \
| while read host ; do
    if ls /sys/class/graphics/fb[0-9]*/blank ; then
        ssh $host '
            for fb in /sys/class/graphics/fb[0-9]*/blank
                echo $BLANK > $fb
            done
        ' &
    else
        echo "ERROR: no /sys/class/graphics/fb[0-9]*/blank interface found" >&2
        exit 2
    fi
done
