#!/bin/bash
# Bryant Hansen
# Screen Blanking Server
# consider defining variables

BLANK=4
ON=0
PORT=55169

nc -l localhost -p $PORT \
| while read onoff ; do
    #[[ "${#onoff}" == 1 ]] || continue
    if [[ "$onoff" == $BLANK || "$onoff" == $ON ]] ; then
        echo "set onoff $onoff" >&2
        echo "$onoff" > /sys/class/graphics/fb0/blank
        # take it easy on the hardware
        # give it a pause to prevent rapid-cycling requests
        # buffer control
        sleep 0.5
    fi
done
