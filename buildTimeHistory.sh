#!/bin/bash
# Bryant Hansen
# 20111124

ME="$(basename "$0")"
USAGE="$ME  package_name"

if [[ ! "$#" == 1 ]] ; then
	echo "ERROR: $ME takes 1 argument." >&2
	echo "USAGE=$USAGE" >&2
	echo "Exiting abnormally" >&2
	exit 2
fi

PACKAGE_NAME="$1"

# initial 1-liner:
# cat /var/log/emerge.log | grep thunderbird | grep "  === " | grep "Compiling\|Merging" | tail -n 30 | while read l ; do echo "$l" ; [[ ! "${l//* Compiling*/}" ]] && compiletime="${l%%:*}" && mergetime="" ; [[ ! "${l//* Merging */}" ]] && mergetime="${l%%:*}" && [[ "$compiletime" ]] && echo "compiletime=$compiletime  mergetime=$mergetime  buildtime=$(expr $mergetime - $compiletime) seconds" ; done

cat /var/log/emerge.log | \
	grep "$PACKAGE_NAME" | \
	grep "  === " | \
	grep "Compiling\|Merging" | \
	tail -n 30 | \
	while read l ; do 
		echo "$l"
		[[ ! "${l//* Compiling*/}" ]] && \
			compiletime="${l%%:*}" && \
			mergetime=""
		[[ ! "${l//* Merging */}" ]] && \
			mergetime="${l%%:*}" && \
			[[ "$compiletime" ]] && \
			echo "buildtime=$(expr $mergetime - $compiletime) seconds" && \
			echo ""
	done
