#!/bin/sh
# Bryant hansen

# initial 1-liner:
# find /sys/class/net/eth3/ -type f | while read f ; do printf "%60s: %s\n" "$f" "$(cat "$f")" ; done > log/sys_eth3_$(dt).log 2>&1

(
    cmd='ifconfig eth3'
    printf "\n$cmd\n"
    $cmd
    cmd='dmesg | grep eth3 | tail'
    printf "\n$cmd\n"
    echo $cmd | sh
    printf "\nfind /sys/class/net/eth3/\n"
    find /sys/class/net/eth3/ -type f \
    | while read f ; do
        val="$(cat "$f" 2>/dev/null | tr '\n' ',')"
        if [[ "$val" ]] ; then
            printf "%60s: %s\n" "$f" "${val%,}"
        fi
    done
    printf "\nfind /sys/class/net/eth3/device/\n"
    find /sys/class/net/eth3/device/ -type f \
    | while read f ; do
        val="$(cat "$f" 2>/dev/null | tr '\n' ',')"
        printf "%60s: %s\n" "$f" "${val%,}"
    done
)
#> log/sys_eth3_$(dt).log 2>&1
#)> log/sys_eth3_$(dt).log 2>&1
