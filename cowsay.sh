#!/bin/bash
# Bryant Hansen

numCows="$(ls -1 /usr/share/cowsay/cows | wc -l)"

cowFile="calvin.cow"
cowsDir="/usr/share/cowsay/cows"

if (( "$numCows" > 0 )) ; then
        number=$RANDOM
        let "number %= $numCows"
        newCowFile="$(ls -1 "$cowsDir" | sed -n "${number}p")"
        echo -e "Selecting random cow ${number} of ${numCows}: ${newCowFile}" >&2
        if [[ -f "${cowsDir}/${newCowFile}" ]] ; then
                cowFile="$newCowFile"
        fi
fi

echo -n "" > /tmp/motd

fortune | cowsay -f "$cowFile" && echo -e "\n" | tee -a /tmp/motd
