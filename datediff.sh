#!/bin/bash
# Bryant Hansen
# 20111124

ME="$(basename "$0")"
USAGE="${ME}  date1  date2"

if [[ ! "$#" == 2 ]] ; then
	echo "ERROR: $0 requires 2 arguments" >&2
	echo "USAGE: " >&2
	echo "    $USAGE" >&2
	echo "exiting abnormally" >&2
	exit 2
fi

# date -d "2011-11-24 13:15:11" "+%s"

d1=$(date -d "$1" "+%s")
d2=$(date -d "$2" "+%s")

diff=$(expr $d2 - $d1)

echo "The difference between $1 and $2 is $diff seconds." >&2

echo $diff
