#!/bin/bash
# Bryant Hansen

# TODO: see if this information can be obtained more reliably, like via /proc or /sys and not have the formatting dependencies of the df util
dev="$(df "$1" | sed 1d | cut -f 1 -d ' ')"

blkid | \
grep "^${dev}.*$" | \
sed "s/.*UUID=\"//;s/\".*//"
