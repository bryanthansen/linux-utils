#!/bin/sh
# Bryant Hansen

# initial 1-liner:
#   cat /proc/meminfo | head | while read l ; do [[ ! "${l##MemTotal*}" ]] && MemTotal="${l#MemTotal:}" ; MemTotal="${MemTotal// /}" ; MemTotal=${MemTotal%kB} ; echo "MemTotal=$MemTotal  $l" ; done

TAGS="MemFree Buffers Cached SwapCached"

#
# verona -> echo "TAGS=\"$(cat /proc/meminfo | sed "s/\:.*/ /" | tr -d "\n")\""
# TAGS="MemTotal MemFree Buffers Cached SwapCached Active Inactive Active(anon) Inactive(anon) Active(file) Inactive(file) Unevictable Mlocked SwapTotal SwapFree Dirty Writeback AnonPages Mapped Shmem Slab SReclaimable SUnreclaim KernelStack PageTables NFS_Unstable Bounce WritebackTmp CommitLimit Committed_AS VmallocTotal VmallocUsed VmallocChunk HugePages_Total HugePages_Free HugePages_Rsvd HugePages_Surp Hugepagesize DirectMap4k DirectMap2M "
#TAGS="MemTotal MemFree Buffers Cached SwapCached Active Inactive Active(anon) Inactive(anon) Active(file) Inactive(file) Unevictable Mlocked SwapTotal SwapFree Dirty Writeback AnonPages Mapped Shmem Slab SReclaimable SUnreclaim KernelStack PageTables NFS_Unstable Bounce WritebackTmp CommitLimit Committed_AS VmallocTotal VmallocUsed VmallocChunk HugePages_Total HugePages_Free HugePages_Rsvd HugePages_Surp Hugepagesize DirectMap4k DirectMap2M "
TAGS="$(cat /proc/meminfo | sed "s/\:.*/ /" | tr -d "\n")\""

extract_number() {
	local tag="$2"
	x=${1#${2}:}
	x="${x// /}"
	x=${x%kB}
	echo "$x"
}

show_line() {
	for tag in $TAGS ; do
		if [[ ! "${line##${tag}:*}" ]] ; then
			x="$(extract_number "$1" $tag)"
			echo -n "" | bar -sw 64 -c $x -s $MemTotal -ns -nt -nth -nc
			return 0
		fi
	done
	echo ""
	return 1
}

cat /proc/meminfo | \
	while read line ; do
		printf "%35s" "$line     "
		[[ ! "${line##MemTotal*}" ]] && MemTotal=$(extract_number "$line" MemTotal)
		show_line "$line"
	done
