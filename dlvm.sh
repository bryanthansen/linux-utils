#!/bin/bash
# Bryant Hansen

# /mnt/DATA_004/data/os
# gink@verona2 -> qemu-system-x86_64 -m 2048M -cdrom knoppix/KNOPPIX_V7.4.2DVD-2014-09-28-EN.iso -boot d -hdb 30GBa.qcow2

# To pass all available host processor features to the guest, use the command line switch
#  qemu -cpu host
# if you wish to retain compatibility, you can expose selected features to your guest. If all your hosts have these features, compatibility is retained:
#  qemu -cpu qemu64,+ssse3,+sse4.1,+sse4.2,+x2apic


DIR=/data/os
ISO="$DIR"/knoppix/KNOPPIX_V7.4.2DVD-2014-09-28-EN.iso
ISO="$DIR"/knoppix/KNOPPIX_V7.6.1DVD-2016-01-16-EN.iso
ISO="$DIR"/knoppix/knoppix_remastered_20161009_221453.iso
ISO="$DIR"/knoppix/KNOPPIX_V7.2.0CD-2013-06-16-EN.iso
ISO="$DIR"/knoppix/KNOPPIX_V7.6.1DVD-2016-01-16-EN/KNOPPIX_V7.6.1DVD-2016-01-16-EN.iso
ISO="$DIR"/knoppix/KNOPPIX_V7.7.1DVD-2016-10-22-EN/KNOPPIX_V7.7.1DVD-2016-10-22-EN.iso
ISO="$DIR"/knoppix/KNOPPIX_V8.1-2017-09-05-EN/KNOPPIX_V8.1-2017-09-05-EN.iso
ISO="$DIR"/knoppix/KNOPPIX_V8.2-2018-05-10-EN/KNOPPIX_V8.2-2018-05-10-EN.iso
ISO="$DIR"/knoppix/KNOPPIX_V8.6-2019-08-08-EN.iso
#HD="$DIR"/30GBa.qcow2
#HD=/mnt/DATA_004/data/os/30GBa.qcow2
#HD=/data/os/virtualbox/32GB.vdi
HD=/data/os/qemu/10GB.qcow2

[[ "$CONF" ]] && [[ -f "$CONF" ]] && . "$CONF"

ENABLE_KVM=
ENABLE_KVM=-enable-kvm

#su - gink -c \

    pushd "$DIR" && \
    (
        set -o verbose
        qemu-system-x86_64 \
            ${ENABLE_KVM} \
            -m 2048M \
            -boot d \
            -cdrom "$ISO" \
            -vga std \
            -soundhw hda \
            -hdb "$HD" \

# extra drive
#            -hdb "$HD" \

# interesting soundhw options
#ac97        Intel 82801AA AC97 Audio
#hda         Intel HD Audio
#
#-soundhw all will enable all of the above
#
# Not specifying means no audio in knoppix
# 'all' sounds horrible; try the others

#        qemu-system-x86_64 \
#            ${ENABLE_KVM} \
#            -m 2048M \
#            -boot d \
#            -cdrom "$ISO" \
#            -hdb "$HD" \
#            -vga std \
#            -soundhw all \

        set +o verbose
        popd > /dev/null
    )

#            -full-screen \
#            -hdb "$HD"
    
#            -enable-kvm \
#            -cpu host \

# Didn't work:

#            -kernel knoppix \
#            -append screen=1920x1080

#            -kernel /boot/isolinux/ifcpu64.c32 \
#            -append 'knoppix64 -- knoppix'

#            -kernel /boot/isolinux/linux64 \
#            -append 'lang=en apm=power-off initrd=minirt.gz nomce libata.force=noncq hpsa.hpsa_allow_any=1 loglevel=1 screen=1920x1080'

#            -soundhw ac97 \


#gink@verona -> qemu-system-x86_64 -soundhw help
#Valid sound card names (comma separated):
#sb16        Creative Sound Blaster 16
#es1370      ENSONIQ AudioPCI ES1370
#ac97        Intel 82801AA AC97 Audio
#adlib       Yamaha YM3812 (OPL2)
#gus         Gravis Ultrasound GF1
#cs4231a     CS4231A
#hda         Intel HD Audio
#pcspk       PC speaker

#-soundhw all will enable all of the above


# http://www.linux-kvm.org/page/Tuning_KVM
# https://wiki.archlinux.org/index.php/QEMU#Enabling_KVM
# https://wiki.archlinux.org/index.php/KVM
# http://www.linux-kvm.org/page/FAQ#General_KVM_information
# https://en.wikibooks.org/wiki/QEMU/Monitor

