#!/bin/bash
# Bryant Hansen

# Show the dmesg log in a polling loop of the tail with a static refresh (something like top)

# from the alias directive:
# alias dmw='ROWS="`stty -a | head -n 1 | sed "s/^.* rows //;s/\;.*$//"`" ; [[ "$ROWS" ]] && (( "$ROWS" > 5 )) && ROWS="`expr $ROWS - 5`" ; [[ ! "$ROWS" ]] && ROWS=30 ; watch "dmesg | tail -n $ROWS"'

ROWS="`stty -a | head -n 1 | sed "s/^.* rows //;s/\;.*$//"`"
[[ "$ROWS" ]] && (( "$ROWS" > 5 )) && ROWS="`expr $ROWS - 5`"
[[ ! "$ROWS" ]] && ROWS=30

watch "dmesg | tail -n $ROWS"

