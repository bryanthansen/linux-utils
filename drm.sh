#!/bin/bash
# Bryant Hansen

f_old="$1"
f_new="${1}.$(date +%Y%m%d_%H%M%S_%Z --reference=${f_old})"
echo "# pipe to sh to execute: $0 '$1' | sh"
echo "mv '${f_old}' '${f_new}'"
