#!/bin/sh
# Bryant Hansen
# Format the date of a file

## dtf2 format:
#> sh dtf.sh dtf.sh
#20161022_204626_557388975

## dtf1 format:
#> sh dtf.sh dtf.sh
#2017-08-05 09:36:42.226836887 +0200

do_dtf() {
    fdt1="$(stat --format="%y" "$1")"
    fdt2="$(date --date="$fdt1" "+%Y%m%d_%H%M%S_%N")"
    echo "$fdt2"
}
do_dtf "$1"
