#!/bin/bash
# Bryant Hansen

# initial 1-liner:
#   FILE=kuow2.$(date "+%Y%m%d_%H%M%S")_00_00_30.mp3 ; URL="http://128.208.34.80:8010/" ; /projects/utils/runfor.sh 20 mplayer -dumpstream -dumpfile "$FILE" -ss 0:00 -endpos 40 "$URL" ; echo -e "\nmplayer terminated" ; /projects/utils/mp3fileinfo.sh "$FILE"

USAGE="${0} File URL Duration"

# test defaults
FILE=kuow2.$(date "+%Y%m%d_%H%M%S")_00_00_30.mp3
URL="http://128.208.34.80:8010/"
DURATION=20

[[ "$1" ]] && FILE="$1"
shift
[[ "$1" ]] && URL="$1"
shift
[[ "$1" ]] && DURATION=$1
shift

echo "$(date): ${0} (FILE=${FILE}, URL=${URL}, DURATION=${DURATION})" >&2

#dir="$(dirname "$(readlink -f "$FILE")")"
#echo "${0} dir=$dir" >&2
dir="$(dirname "$FILE")"
echo "${0} dir=$dir" >&2
dir="$(readlink -f "$dir")"
echo "${0} dir=$dir" >&2
[[ ! -d "$dir" ]] && mkdir -p "$dir"

#/projects/utils/runfor.sh $DURATION mplayer -dumpstream -dumpfile "$FILE" -ss 0:00 -endpos 40 "$URL"
/projects/utils/runfor.sh $DURATION mplayer -dumpstream -dumpfile "\"${FILE}\"" "$URL"
echo -e "\n${0}: mplayer terminated" >&2
/projects/utils/mp3fileinfo.sh "$FILE" >&2
