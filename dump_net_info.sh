#!/bin/bash
# Bryant Hansen

# Initial 1-liner:
# ( cmd='lspci -v | grep "Eth\|Kernel\|Serial" | grep -A 3 Eth' ; printf "\n\n==============\n$cmd\n==============\n" ; echo $cmd | /bin/bash ; cmd="ip link show" ; printf "\n\n==============\n$cmd \n==============\n" ; $cmd ; cmd) | sed "s/^/\#\ /"

# Global Commands
g_commands="
    lspci -v | grep 'Eth\\\\\|Kernel\\\\\|Serial' | grep -A 3 Eth
    ip link show
    cat /proc/net/dev
    cat /proc/net/if_inet6
"

# Interface Commands
i_commands="
    ethtool -P
    ethtool
    ifconfig
"

echo -e "$g_commands" \
| sed "s/\#.*//" \
| while read cmd ; do
        [[ ! "$cmd" ]] && continue
        printf "==============\n$cmd\n==============\n"
        echo "$cmd" | /bin/bash
        printf "\n\n"
done \
| sed "s/^/\#\ /"


# Possible methods of listing the ethernet devices:
#   cat /proc/net/dev | sed 1,2d |  cut -f 1 -d ':' \
#   cat /proc/net/if_inet6 | while read a b c d e f ; do echo $f ; done

cat /proc/net/if_inet6 \
| while read a b c d e f g ; do
    echo $f
done \
| while read if ; do
    printf "==============\nNetwork data for $if\n==============\n"
    echo -e "$i_commands" \
    | sed "s/\#.*//" \
    | while read cmd ; do
            [[ ! "$cmd" ]] && continue
            printf "\n--- $cmd $if --- \n"
            echo "$cmd $if" | /bin/bash
    done \
    | sed "s/^/\ \ /"
    printf "\n"
done \
| sed "s/^/\#\ /"
