#!/bin/bash
# Bryant Hansen

# This script will take an edl file, as produced by mplayer with the -edlout option (and hitting the 'i' key) and cut clips out of the video corresponding to the segments that should be played, with the exception of the last one

# initial 1-liner:
# f=20b.edl ; cat "$f" | sed "s/\#.*$//" | grep -v "^[ \t]*$" | while read a b c ; do [[ "$lb" ]] && diff=`echo "${a}-${lb}" | bc` && echo mencoder -oac copy -ovc copy -ss $lb -endpos $diff 20.avi -o 20g.avi ; lb=$b ; done

vidIn=unknown_path/unknown_file.avi
edl="unknown_path/unknown_file.edl"

inverse=""
[[ "$1" == "-i" ]] && inverse=1 && shift

[[ "$1" ]] && vidIn="$1"
shift

echo "vidIn = $vidIn" >&2
[[ -f "${vidIn/.avi/.edl}" ]] && edl="${vidIn/.avi/.edl}"
[[ -f "${vidIn}.edl" ]] && edl="${vidIn}.edl"
ls -al "${vidIn}"* >&2
testEdl="$(ls -1 "${vidIn}"*.edl 2>/dev/null | tail -n 1)"

[[ "$2" ]] && testEdl="$2"

echo "testEdl=$testEdl"
[[ -f "$testEdl" ]] && edl="$testEdl"

[[ "$1" ]] && edl="$1"

echo "vidIn=$vidIn" >&2
echo "edl=$edl" >&2

#echo "manual exit (debug)" >&2
#exit -1

# edl's are made to select regeons to remove, not regeons to include
#   extra steps must be taken to use it to select clips for addition, rather than removal
#   below is the simple way, that work opposite the API, when doing an mplayer -edl myclips.edl myvideo.avi
#   _inverse means that the output will be the inverse of what will be played when doing the mplayer -edl ...
function edl_inverse() {
    cat "$edl" | \
        sed "s/\#.*$//" | \
        grep -v "^[ \t]*$" | \
        while read a b c ; do 
            [[ ! "$clip" ]] && clip=0
            clip=$(expr $clip + 1)
            diff=`echo "${b}-${a}" | bc`
            # echo "mencoder -oac copy -ovc copy -ss $a -endpos $diff \"${vidIn}\" -o \"${vidIn}.${clip}.avi\""
            echo "mencoder -oac copy -ovc copy -ss $a -endpos $diff -oac faac -faacopts mpeg=4:object=2:raw:br=128 \"${vidIn}\" -o \"${vidIn}.${clip}.avi\""
        done
}

# this function will set the clips the way mplayer would, but extra steps must be done to preserve the first and last clip
#   manully, this involves extra tags.  problem is that there isn't a special tag that I've found to say "exclude everything until the end of the file"  stupid reason, but all experiments (-1, 0, 1000000) fail somehow...the extra step of calculating file length must be done before this can work
function edl_mplayer() {
    cat "$edl" | \
        sed "s/\#.*$//" | \
        grep -v "^[ \t]*$" | \
        while read a b c ; do 
            echo "a=$a  b=$b  c=$c" >&2
            [[ ! "$clip" ]] && clip=0
            [[ "$lb" ]] && \
                diff=`echo "${a}-${lb}" | bc` && \
                clip=$(expr $clip + 1) && \
                #echo mencoder -oac copy -ovc copy -ss $lb -endpos $diff $vidIn -o ${vidIn}.${clip}.avi
                echo mencoder -ovc copy -ss $lb -endpos $diff -oac faac -faacopts mpeg=4:object=2:raw:br=128 $vidIn -o ${vidIn}.${clip}.avi
                
                lb=$b
        done
}

if [[ "$inverse" ]] ; then
    echo "# edl_inverse" >&2
    edl_inverse
else
    echo "# edl_mplayer" >&2
    edl_mplayer
fi
