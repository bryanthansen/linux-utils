#!/bin/bash
# Bryant Hansen

if [[ ! "$1" ]] ; then
    printf "$0 ERROR: this script requires 1 argument\n" >&2
    exit 2
fi
if [[ ! -f "$1" ]] ; then
    printf "$0 ERROR: file '$1' does not exist\n" >&2
    exit 3
fi

dt="$(dt)"
b="$(basename "$1")"
pst="$1"
mbox="${pst}"
mbox="${mbox%.pst}"
mbox="${mbox%.PST}"
mbox="${mbox%.ost}"
mbox="${mbox%.OST}"
mdir="${mbox}.mdir"
mbox="${mbox}.mbox"

printf "# Outlook file: ${pst}\n" >&2
printf "# mbox dir: ${mbox}\n" >&2
printf "# mdir dir: ${mdir}\n" >&2

[[ ! -d "$mbox" ]] && echo "mkdir -p '${mbox}'"
echo "
    if [[ ! -d '$mbox' ]] ; then
        printf '$0 ERROR: could not create mbox $mbox in $PWD \n >&2'
        exit 2
    fi
"

[[ ! -d "$mdir" ]] && echo "mkdir -p '${mdir}'"
echo "
    if [[ ! -d '$mdir' ]] ; then
        printf '$0 ERROR: could not create mdir $mdir in $PWD \n >&2'
        exit 2
    fi
"

echo "
    if pushd '${mbox}' > /dev/null ; then
        time readpst -u -w -M -r -D -cv '../${pst}' 2>&1 | tee .readpst.log.$dt
        popd
    fi
    time perl /projects/mb2md/mb2md.pl -u -S -R -r mbox -s '${PWD}/${mbox}' -d '${PWD}/${mdir}' 2>&1 | tee '${mdir}/.mb2md.log.$dt'
"

printf "# to execute, pipe the output to bash:\n" >&2
printf "#    $0 $* | bash\n" >&2

exit 0
