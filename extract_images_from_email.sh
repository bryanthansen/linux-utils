#!/bin/bash
# Bryant Hansen

# called like this:
# rm -f *.base64 *.header *.jpg ; cat ./saved_mail1.eml | /projects/utils/extract_images_from_email.sh

# writes image files to local folder with name as specified in the Content-Type string

emailheader=1

tr -d '\r' | 
while read l ; do

	unset blank
	[[ ! "${l}" ]]     && blank=1
	[[ "$blank" ]]     && unset emailheader && unset imageheader
	[[ "$blank" ]]     && continue

	[[ ! "${l/*Content-Type: image\/jpg*/}" ]] && contentstring=1 || unset contentstring
	if [[ "$contentstring" ]] ; then
		name="${l#*; name=\"}"
		name="${name%\"*}"
		echo "name=$name" >&2
		image=1
		imageheader=1
		unset isbase64
		imageline=0
	fi

	[[ ! "${1/*Content-Transfer-Encoding: base64*/}" ]]  && isbase64=1
	[[ "${l:0:2}" == "--" ]] && checksum=1 || unset checksum

	[[ "$image" ]]          && \
	[[ "$isbase64" ]]       && \
	[[ "$imageheader" ]]    && \
	[[ "$name" ]]           && \
		echo "$l" >> "$name".header

	[[   "$image" ]]        && \
	[[   "$isbase64" ]]     && \
	[[ ! "$imageheader" ]]  && \
	[[ ! "$checksum" ]]     && \
	[[   "$name" ]]         && \
		echo "$l" >> "$name".base64

	if [[ "$checksum" ]] ; then
		md5="${l:2}"
		[[ "$image" ]] && echo "   ${name}: md5=$md5" >&2
		unset image
		unset name
		unset imageheader
		unset isbase64
	fi

done

for f in *.base64 ; do
	echo "base64 -d \"${f}\" > \"${f%.base64}\"" >&2
	base64 -d "${f}" > "${f%.base64}"
done
