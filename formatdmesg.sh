#!/bin/bash
# Bryant Hansen

# TODO: timestamp seconds decimal does not wrap at 0 - time can theoretically move backwards, making problems for sorting - do raw clock translation

uptime=`cat /proc/uptime | cut -f 1 -d ' '`
uptime_s=${uptime%.*}
up_date_s=`date --date="now - $uptime_s secs" "+%s"`

#next_date=0

# line iterator - read from stdin
# could not parse in `read` due to occasional extra spaces before timestamp (ie. [   n.nnnnnn])
while read line ; do
    # test for timestamp; just print raw line if not detected
    timestamp_missing_detector="${line/*\[*[0-9]\.[0-9]*\]*/}"
    if [[ "$timestamp_missing_detector" ]] ; then
        # line with timestamp not found
        echo "[   timestamp not found   ] $line"
        continue
    fi
    tstamp="${line%%]*}"           # strip everything after the ] to isolate timestamp
    tstamp2=${tstamp//[][]/}       # strip the brackets from the time
    tstamp_s=${tstamp2%.*}         # truncate seconds
    # add the timestamp seconds to the boottime in seconds, yielding timestamp date in seconds
    #tstamp_date_s=`expr $up_date_s + $tstamp_s`
    tstamp_date_s=$(( $up_date_s + $tstamp_s ))
    #tstamp_date_s=1327621571
    # an ugly and slow function to seconds back to a real date
    #OPTIMIZATION_001=1
    if [[ "$OPTIMIZATION_001" ]] ; then
        if [[ ! "$next_date" ]] || (( $tstamp_date_s > $next_date )) ; then
            dt=`date -u -d "1970-1-1 ${tstamp_date_s} sec UTC" "+%Y-%m-%d %H:%M:%S"`
            day_start_s=`expr $tstamp_date_s / 86400 \* 86400`
            next_date=`expr $day_start_s + 86400`
            last_date="${dt% *}"
            last_day=${last_date:6:2}
            echo "day_start_s=$day_start_s  next_date=$next_date  tstamp_date_s=$tstamp_date_s" >&2
            echo "setting last date to $last_date" >&2
        else
            hrs=$(( $tstamp_date_s % 86400 / 3600 ))
            mins=$(( $tstamp_date_s % 3600 / 60))
            secs=$(( $tstamp_date_s % 60))
            dt="${last_date} ${hrs}:${mins}:${secs}"
        fi
    else
        dt=`date -u -d "1970-1-1 ${tstamp_date_s} sec UTC" "+%Y-%m-%d %H:%M:%S"`
    fi
    # dump the time, the formating and the original line without the original timestamp
    echo "[${dt}.${tstamp2#*.}] ${line#*]}"
done
