#!/bin/bash
# from http://serverfault.com/questions/366392/how-to-convert-dmesg-time-format-to-real-time-format

ut=`cut -d' ' -f1 </proc/uptime`
ts=`date +%s`
date -d"70-1-1 + $ts sec - $ut sec + $1 sec" +"%F %T"

# dmesg | tail -n 10 | tr '][' '  ' | while read a b ; do echo -n "[" ; ./formatdmesgtime.sh $a | tr -d '\n' ; echo -n "] " ; echo "$b" ; done
