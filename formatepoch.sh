#!/bin/bash

# (c) Bryant Hansen

#input: stdin stream, line-based ascii
#	line starts with epoch, as in gentoo emerge log files
#	epoch is formatted in human-readable type

while read 'line' ; do echo "`date -d "1970-1-1 ${line/:*/} sec UTC" "+%Y-%m-%d %H:%M:%S"`:${line#*:}" ; done
#while read 'line' ; do echo "`date -u -d "1970-1-1 ${line/:*/} sec UTC" "+%Y-%m-%d %H:%M:%S"`:${line#*:}" ; done
#while read 'line' ; do echo "`date -u -d "1970-1-1 ${line/:*/} sec" "+%Y-%m-%d %H:%M:%S"`:${line#*:}" ; done
#while read 'line' ; do echo "`date -d "1970-1-1 ${line/:*/} sec" "+%Y-%m-%d %H:%M:%S"`:${line#*:}" ; done
