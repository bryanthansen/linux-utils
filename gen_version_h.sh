#!/bin/bash

echo "// Generating git version file" >&2
(
    echo "#ifndef _GIT_VERSION_H"
    echo "#define _GIT_VERSION_H"
    echo
    echo "#define GIT_BRANCH       \"$(git symbolic-ref -q --short HEAD)\""
    echo "#define GIT_COMMIT_HASH  \"$(git log -1 --pretty='%H')\""
    echo "#define GIT_AUTHOR       \"$(git log -1 --pretty='%an <%ae>')\""
    echo "#define GIT_DATE         \"$(git log -1 --pretty='%ci')\""
    echo "#define GIT_COMMIT_COUNT $(git rev-list --count HEAD)"
    if git status -s | grep -v usr/console/git_version.h | grep "^A. \|.M \|.D \|^R. " > /dev/null ; then
        echo "#define GIT_IS_DIRTY     true"
    else
        echo "#define GIT_IS_DIRTY     false"
    fi
    echo "#define GIT_DIFF_HASH    \"$(git diff | md5sum | head -c 7)\""
    echo
    echo "#endif"
)

