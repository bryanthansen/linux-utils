#!/bin/bash
# Bryant Hansen

# initial 1-liner:
# find . -maxdepth 1 -type f -and -not -name "*.txt" | sort -n | while read f ; do [[ "${#f}" -gt 22 ]] && continue ; f="${f#.\/}" ; f="${f%.*}" ; echo "$f" ; done | head

f="$1"

echo "# input: $f" >&2

f="${f#.\/}"
f="${f%.*}"
echo "# id: $f" >&2

if [[ "${#f}" -gt 22 ]] ; then
    echo "${f}: Name too long: 22 char max; ${f} is ${#f} chars" >&2
    echo exiting >&2
    exit 0
fi

url="https://www.youtube.com/watch?v=${f}"
echo "# url: $url" >&2

cmd="wget -O- ${url}"
echo "# ${cmd}: $cmd" >&2

echo "$cmd" \
| /bin/sh 2>/dev/null \
| grep -i "<title>" \
| sed "s/.*<title[^>]*>//;s/<\/title>.*//"
