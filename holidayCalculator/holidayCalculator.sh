#!/bin/sh
# Bryant Hansen

Y=$(date +%Y)
[[ $1 ]] && Y=$1
echo -e "=======\n  $Y\n======="

SED_STRIP_COMMENTS_REGEX="s/\#.*$//g"
SED_DELETE_BLANK_LINES_REGEX="/^[ \t]*$/d"

f="holidays.txt"
[[ -f "$f" ]] || f="$(dirname "$0")/${f}"

cat "$f" \
| sed "${SED_STRIP_COMMENTS_REGEX};${SED_DELETE_BLANK_LINES_REGEX}" \
| while read dt ; do
    date --date="$dt $Y" "+%d %b: ISO Week %V, %A"
done
