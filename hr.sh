# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# hr.sh
# Author: Bryant Hansen <bash_scripts_on_web@bryanthansen.net>
# Last Updated: 20080621

# This is a simple script to format large numbers to human-readable
# format.  It is intended to function in a similar manner to the -h
# option in ls, du, df, etc.  Input should be numerical and decimal.
# Hex, octal, binary, etc, are not supported in this version.  Input
# can be either an argument or standard input, carriage-return
# delimited.

#!/bin/bash

hr()
{
	hr_val=$1
	(( $1 > 100000000000 )) && echo "`expr $1 \/ 1000000000`G" && return
	(( $1 > 100000000 )) && echo "`expr $1 \/ 1000000`M" && return
	(( $1 > 100000 )) && echo "`expr $1 \/ 1000`k" && return
	echo $1
}

if [[ ! -z "$1" ]] ; then
	hr "$1"
else
	while read 'line' ; do
		hr "$line"
	done
fi
