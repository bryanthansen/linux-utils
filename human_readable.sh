#!/bin/bash
# Bryant Hansen

ME="$(basename "$0")"
USAGE="$ME  number"

DESCRIPTION="
    This program will take a large number is abbreviate it in a human-readable form
"

hr_val=$1

# very large number example: 1.52519e+10 bytes
# can be a comma with German language options: 1,52519e+10 bytes
if [[ `expr match "$hr_val" ".*\([0-9][0-9]e+[0-9][0-9]\).*"` ]] ; then
        val="${hr_val/,/}"
        val="${val/./}"
        base=${val/e+*/}
        exp=${val/*e+/}
        [[ exp == 9 ]] && echo "${base:0:1}.${base:1:3}G" && return
        [[ exp == 10 ]] && echo "${base:0:2}.${base:2:2}G" && return
        [[ exp == 11 ]] && echo "${base:0:3}.${base:3:1}G" && return
        [[ exp == 12 ]] && echo "${base:0:1}.${base:1:3}T" && return
else
        (( $1 > 100000000000 )) && echo "`expr $1 \/ 1000000000`G" && return
        (( $1 > 100000000 )) && echo "`expr $1 \/ 1000000`M" && return
        (( $1 > 100000 )) && echo "`expr $1 \/ 1000`k" && return
        echo $1
fi
