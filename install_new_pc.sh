#!/bin/bash
# Bryant Hansen

# the intent of this script is to fetch and install standard scripts for the system being used

# Feature List:
#   Detect host version (distribution, dependencies, busybox?, Linux/Win/Mac/Android, 
#   Start with a Gentoo system

# intended to be run as:
#   wget -O- https://bryanthansen.net/utils/standard-install.sh | /bin/sh

# Make test-install script too:
#   wget -O- https://bryanthansen.net/utils/test-install.sh | /bin/sh

# Make the identify-system:
#   wget -O- https://bryanthansen.net/utils/test-install.sh | /bin/sh

# Make the report-home.sh script!
