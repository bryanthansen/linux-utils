#!/bin/bash
# Bryant Hansen

# Description:
# Unlock the display from the command line and turn on the backlight

# NOTE: KDE-only (currently tested with 4.14.3
# TODO: make a version that is compatible with dbus-send, since it is already included with the base dbus package

export DISPLAY=:0
pid="$(
	grep -a kscreenlocker_greet /proc/[1-9]*/cmdline \
	| tr "\0" " " \
	| sed '/grep /d;s/^\/proc\///;s/\/.*//'
)"
echo "kscreenlocker_greet PID = $pid" >&2
qdbus org.kde.kscreenlocker_greet-${pid} /MainApplication quit

if [[ ${EUID} == 0 ]] ; then
    # requires root privileges:
    echo 90 > /sys/class/backlight/acpi_video0/brightness
else
    # does not require root privileges
    qdbus org.freedesktop.PowerManagement /org/kde/Solid/PowerManagement/Actions/BrightnessControl setBrightness 90
fi
