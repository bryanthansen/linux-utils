#!/bin/bash
# Bryant Hansen

ME="$(basename "$0")"

ts_file=/usr/portage/metadata/timestamp.chk
ts="$(cat "$ts_file")"
ts_s=$(date --date="$ts" +%s)
now=$(date +%s)
elapsed_s=$(expr $now - $ts_s)

S_in_MI=60
S_in_H=$((S_in_MI * 60))
S_in_D=$((S_in_H * 24))
S_in_W=$((S_in_D * 7))

printf "%20s: %s\n" "timestamp file" "$ts_file" >&2
printf "%20s: %s\n" "timestamp" "$ts" >&2
printf "%20s: %s\n" "now" "$(date)" >&2
printf "%20s: %s\n" "elapsed(s)" "$elapsed_s s" >&2

printf "%20s: " "elapsed" >&2
es=$elapsed_s
for n in $S_in_W $S_in_D $S_in_H $S_in_MI ; do
    (( es < $n )) && continue
    nf=$(((es / $n) % $n))
    es=$((es - nf * n))
    printf "%02d:" "$nf"
done
printf "%02d\n" "$((es % 60))"
