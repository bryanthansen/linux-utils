#!/bin/bash
# Bryant Hansen

ts_file=/usr/portage/metadata/timestamp.chk
ts="$(cat "$ts_file")"
ts_s=$(date --date="$ts" +%s)
now=$(date +%s)
elapsed_s=$(expr $now - $ts_s)
echo $elapsed_s
