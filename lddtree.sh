#!/bin/bash
# Bryant Hansen

# initial 1-liner:
# ldd /usr/sbin/sshd | sed "s/^.*=>//;s/\ (.*$//;s/^[ \t]*//" ; while read f ; do echo "" ; echo "$f" ; echo "" ;  ldd "$f" ; echo "" ; done

dump_deps() {
        while [[ "$1" ]] ; do
                #echo "dump_deps $1" >&2
                dps="$(ldd $1 | sed "s/^.*=>//;s/\ (.*$//;s/^[ \t]*//" | grep -v "statically linked")"
                echo -e "dump_deps: $1 deps: \n$dps \n" | sed "s/^/\ \ \ \ /" >&2
                echo "$dps"
                shift
        done
}

while [[ "$1" ]] ; do
        newdeps="$1"
        deps=""
        while [[ "$newdeps" ]] ; do
                echo "newdeps: $newdeps" >&2
                echo "$newdeps"
                lastdeps="$deps"
                deps="$(dump_deps $newdeps)"
                echo "deps: $deps" >&2
                newdeps="$(echo -e "${deps}\n${lastdeps}" | sort | uniq -u)"
                echo "newdeps: $newdeps" >&2
        done
        shift
done | \
sort | \
uniq

