#!/bin/bash
# Bryant Hansen

# alias logwatch='tail -f /var/log/everything/current /var/log/apache/*.log /var/log/qmail/*/current /var/log/critical/current /var/log/apache2/*.log'

watch_list='
    /var/log/everything/current
    /var/log/apache/*.log
    /var/log/qmail/*/current
    /var/log/critical/current
    /var/log/apache2/*.log
'

#tail -f /var/log/everything/current /var/log/apache/*.log /var/log/qmail/*/current /var/log/critical/current /var/log/apache2/*.log'

watch_list2="$(
    for filepat in $watch_list ; do
        echo "filepat = $filepat" >&2
        #echo "ls -1 $filepat" | /bin/bash
        ls -1 $filepat
        echo "" >&2
    done
)"

watch_list3="$watch_list2
    $(find /var/log/ -name current)
"
watch_list4="$watch_list3
    $(find /var/log/ -mtime -1 -type f)
"
watch_list5="$(
    echo -e "$watch_list4" \
    | sed "s/^[ \t]*//;/^[ \t]*$/d" \
    | sort \
    | uniq
)"

echo -e "watch_list:" >&2
echo -e "$watch_list5"

echo -e "Try this:\n  tail -f \$($0)" >&2
