#!/bin/bash
# Bryant Hansen

ME="$(basename "$0")"

trace() {
    local LINE_PREFIX="`date "+%Y%m%d %H%M%S"`: "
    echo -e "${LINE_PREFIX}$*"
}

header() {
    h=$*
    printf "$h\n"
    #printf '=%.0s' {1..60}
    #printf '\n'
}

footer() {
    #printf '=%.0s' {1..60}
    printf '\n'
}

# Places for report
#   - motd
#     - updated via cron job
#     - initiated from pam
#   - email
#   - filesystem
#   - browser dashboard (make a good report CSS)
#     (probably implies filesystem)
#   - write-only backup


do_web_logfile_report() {
    # Web Service
    header "Log File Statistics"
    indent="$(printf ' %.0s' {1..38})"
    for f in /var/log/apache2/*.log ; do
        numlines="$(cat "$f" | wc -l | cut -f 1 -d ' ')"
        printf "%24s: %s\n" "Filename" "$f"
        printf "%24s: %s\n" "Number of Lines" "$numlines"
        if [[ "$numlines" -lt 13 ]] ; then
            cat "$f" | sed "s/^/${indent}/"
        else
            printf "%24s:\n"    "First Lines"
            cat "$f" 2>&1 | head -n 3 | sed "s/^/${indent}/"
            printf "%24s\n" "Last Lines: "
            cat "$f" 2>&1 | head -n 10 | sed "s/^/${indent}/"
        fi
        printf "\n"
    done
    printf "%30s: %s\n" "Number of Access Records" "$(wc -l /var/log/apache/*access*.log 2>&1 | tr '\n' '/')"
    printf "%30s: %s\n" "Number of Error Records" "$(wc -l /var/log/apache/*error*.log 2>&1 | tr '\n' '/')"
}

# Report Details
indent="$(printf '#%.0s' {1..80})"
printf '#%.0s' {1..80}
printf "\n# System Report\n"
printf '#%.0s' {1..80}
printf "\n## %32s: %s\n" "Hostname:" "$(hostname)"
printf "## %32s: %s\n" "Report Date" "$(date "+%Y%m%d %H%M%S")"
printf "## %32s: %s\n" "System" "$(uname -a)"
printf "## %32s: %s\n" "Uptime" "$(uptime)"

# Update/Upgrade Status
#   - Date/Time of last synchronization with update server
printf "## %32s: %s\n" "Last Sync with the Update Server" "$(grep "^.*emerge[ \t].*sync[ \t]*$" /var/log/emerge.log 2>&1 | tail -n 1 | /usr/local/bin/feda)"


# Authorization failures
#   - date/time
#   - service
#   - originating information
#   -
printf "TODO: Authorization failures\n"

# Service Status
#   - Key statistics for each service
#   - Links to individual service reports and raw log files


indent="$(printf ' %.0s' {1..8})"
#   - Results of last attempted upgrade
#   - Significant Currently-installed versions of OS, Drivers, and Apps

header "Xorg Errors"
cat /var/log/Xorg.0.log 2>&1 | grep '(EE)' | tail | sed "s/^/${indent}/"

# System Status
#   - Drives that are nearly full

#   - Errors in significant logs

header "Drive Usage"
df -h 2>&1 | sed "s/^/${indent}/"

header "dmesg | tail"
dmesg 2>&1 | tail -n 5 | sed "s/^/${indent}/"

header "dmesg errors"
dmesg 2>&1 | grep -i "error\|fail" | tail -n 8 | sed "s/^/${indent}/"

header "Memory Usage"
free 2>&1 | sed "s/^/${indent}/"

printf '#%.0s' {1..80}

header "Fortune"
fortune | cowsay

