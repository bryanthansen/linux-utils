#!/bin/bash
# Bryant Hansen

OPTS="-m"
[[ "$1" ]] && OPTS="$*"

first="1"
free -s 2 $OPTS \
| while read l ; do
    echo -n "    $l"
    [[ ! "${l}" ]] && echo ""
done \
| while read a b c e f g h i j k l m n o p q r s t u v w x y z ; do

    [[ "$first" ]] && \
        printf "$(date): %8s %8s %8s %8s %8s %8s %18s %8s %8s\n" $a $b $c $d $e $f $g $p $s "(total/used/free)"

    printf     "$(date): %8s %8s %8s %8s %8s %8s %8s %8s %8s %8s %8s\n" $i $j $k $l $m $n $q $r $t $u $v

    first=""
done
