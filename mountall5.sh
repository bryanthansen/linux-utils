#!/bin/bash
# Bryant Hansen
# 20110225

# latest 1-liner - Don Apr  5 13:29:19 CEST 2018
# blkid | grep sdb | cut -f 1 -d: | xargs -n1 basename | while read p ; do d=/dev/$p ; m=/mnt/$p ; [[ -d $m ]] || echo mkdir -p $m ; mount -l | grep ^$d || echo mount /dev/$p /mnt/$p ; done
# mount /dev/sdb3 /mnt/sdb3
# /dev/sdb4 on /mnt/sdb4 type ext2 (rw,relatime,block_validity,barrier,user_xattr,acl)
# mount /dev/sdb1 /mnt/sdb1
# mount /dev/sdb2 /mnt/sdb2


# 1-liners
# parted -l -s | grep "/dev/" | sed "s/:.*$//;s/^.*\ //" | grep "/dev/" | while read disk ; do drive="${disk/dev/mnt}" ; parted "$disk" print | while read l ; do num="${l/ */}" ; [[ "$l" ]] && [[ ! "${l/[0-9]*/}" ]] && ( [[ ! -d "${drive}${num}" ]] && echo "mkdir ${drive}${num}" ; echo "mount ${disk}${num} ${drive}${num}" ) ; done ; done
# parted -l -s | grep "/dev/" | sed "s/:.*$//;s/^.*\ //" | grep "/dev/" | while read disk ; do drive="${disk/dev/mnt}" ; parted "$disk" print | grep -v linux-swap | while read l ; do num="${l/ */}" ; [[ "$l" ]] && [[ ! "${l/[0-9]*/}" ]] && ( [[ ! -d "${drive}${num}" ]] && echo "mkdir ${drive}${num}" ; echo "# $l" ; echo "mount ${disk}${num} ${drive}${num}" ) ; done ; done
# blkid | while read l ; do dev="${l/:*/}" ; UUID="${l/*UUID=\"/}" ; UUID="${UUID/\"*/}" ; [[ "${l/*LABEL=\"*/}" ]] && LABEL="" || LABEL="${l/*LABEL=\"/}" ; LABEL="${LABEL/\"*/}" TYPE="${l/*TYPE=\"/}" ; TYPE="${TYPE/\"*/}" ; echo "$l" ; echo "dev=$dev  UUID=$UUID  LABEL=$LABEL  TYPE=$TYPE" ; echo "" ; done

VERBOSE=0
DEFAULT_UID=1000

[[ "$1" ]] && [[ "$1" == "-v" ]] && VERBOSE=1 && shift

isMounted() {
  if [[ "$( cat /etc/mtab | cut -f 1 -d ' ' | grep "^${1}$")" ]] ; then
	return 0
  fi
  return 1
}

isMountpointUsed() {
	hits="$(cat /etc/mtab | cut -f 2 -d ' ' | grep -E "^${1}$" | wc -l)"
	[[ ! "$hits" ]] && echo "# failed to detect whether anything is mounted on mountpoint $1" && return 0
	case "$hits" in
	0)
	 	#echo "# mountpoint $1 is not used"
		return 1
		;;
	1) 	echo "# mountpoint $1 is used"
		return 0
		;;
	*) 	echo "# bad answer querying for mountpoint count: $hits"
		return 0
		;;
	esac
}

isMountAllowed() {
	# cases to disallow:
	# 	device is already mounted on mountpoint
	# 	device is already mounted on alternate mountpoint
	# 	mountpoint is already used for another device

	local partition=$1
	local mountpoint=$2
	local type=$3

	if [[ "$type" == "swap" ]] ; then
		[[ "$VERBOSE" ]] && (( "$VERBOSE" )) && \
		echo "# not automounting swap partition $partition"
		return 7
	fi

	if [[ "$(grep "$partition $mountpoint " /etc/mtab)" ]] ; then
		echo "# $partition is already mounted on $mountpoint"
		return 8
	fi

	if isMounted $partition ; then
		if isMountpointUsed $mountpoint ; then
			echo "# STRANGE ERROR: partition $partition is already mounted, mountpoint $mountpoint is already used, but $partition is not mounted on $mountpoint" >&2
			return 2
		else
			[[ "$VERBOSE" ]] && (( "$VERBOSE" )) && \
				echo -n "# $partition is already mounted on " && \
				cat /etc/mtab | grep -E "^${partition}\ .*$" | cut -f 2 -d ' ' | tr "\n" " " | sed "s/\ $//;s/\ /\ and\ /g" && \
				echo ""
			return 5
		fi
	else
		if isMountpointUsed $mountpoint ; then
			echo "# another device is already mounted on $mountpoint"
			return 4
		else
			[[ "$VERBOSE" ]] && (( "$VERBOSE" )) && \
				echo "# $partition is not already mounted on $mountpoint"
			return 0
		fi
	fi

	return 6
}

getMountOpts() {
	local read_only=1
    local uid=""
	type="$1"
	[[ "$type" == "ext2" ]] && read_only=0
	[[ "$type" == "ext3" ]] && read_only=0
	[[ "$type" == "ext4" ]] && read_only=0
	[[ "$type" == "xfs" ]]  && read_only=0
	if [[ "$type" == "vfat" ]] ; then
        read_only=0
        uid=$DEFAULT_UID
    fi
	local mount_opts=""
	(( "$read_only" )) && mount_opts="-o ro"
    if [[ "$uid" ]] ; then
        if [[ "$mount_opts" ]] ; then
            mount_opts="$mount_opts,uid=$uid"
        else
            mount_opts="-o uid=$uid"
        fi
    fi
	echo "$mount_opts"
}

echo "# to execute, pipe my output to /bin/bash"
echo "# $0 | /bin/bash"
echo ""

echo "set -o verbose"

echo "mount -a" # mount everything that has a defined mount point in fstab


blkid | while read l ; do 

	[[ "$VERBOSE" ]] && (( "$VERBOSE" )) && \
	echo ""

	[[ "$VERBOSE" ]] && (( "$VERBOSE" )) && \
	echo "# $l"

	partition="${l/:*/}"

	[[ "${l/*UUID=\"*/}" ]] && UUID="" || UUID="${l/*UUID=\"/}"
	UUID="${UUID/\"*/}"

	TYPE="${l/*TYPE=\"/}"
	TYPE="${TYPE/\"*/}"
	TYPE="${TYPE/ */}"

	[[ "${l/*LABEL=\"*/}" ]] && LABEL="" || LABEL="${l/*LABEL=\"/}"
	LABEL="${LABEL/\"*/}"

    mountpoint="${partition/dev/mnt}"
 	if [[ "$UUID" ]] ; then
		mountpoint="/mnt/${UUID}"
	fi
 	if [[ "$LABEL" ]] ; then
		mountpoint="/mnt/${LABEL}"
		[[ "$VERBOSE" ]] && (( "$VERBOSE" )) && \
		echo "# partition=\"${partition}\"  UUID=\"${UUID}\"  LABEL=\"${LABEL}\"  TYPE=\"${TYPE}\"  mountpoint=\"${mountpoint}\""
	else
		[[ "$VERBOSE" ]] && (( "$VERBOSE" )) && \
		echo "# partition=\"${partition}\"  UUID=\"${UUID}\"  LABEL=(LABEL not defined)  TYPE=\"${TYPE}\"  mountpoint=\"${mountpoint}\""
	fi

	isMountAllowed $partition $mountpoint $TYPE || continue

	[[ ! -d "$mountpoint" ]] && echo "mkdir '${mountpoint}'"

	echo "mount $(getMountOpts $TYPE) '${partition}' '${mountpoint}'"

done

echo "set +o verbose"
