#!/bin/bash
# Bryant Hansen

# original 1-liner:
# 	mp3info -r a -p "Filename: %F  \nPlaytime: %S seconds  \nFile Size: %k kB  \nBitrate:  %r kbps  \nMPEG Version %v Layer %L  \nSampling Rate: %q kHz  \nChannel(s): %o  \nCRC Error protection: %E  \nOriginal material flag: %O" kuow2.20110829_010413_00_00_30.mp3info

mp3info \
	-r a \
	-p \
	"Filename: %F
Playtime: %S seconds
File Size: %k kB
Bitrate:  %r kbps
MPEG Version %v Layer %L
Sampling Rate: %q kHz
Channel(s): %o
CRC Error protection: %E
Original material flag: %O" \
	"$1"
