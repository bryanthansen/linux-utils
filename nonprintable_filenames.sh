#!/bin/sh
# Bryant Hansen

# Initial 1-liner:
# LC_ALL=C find . -name '*[![:print:]]*' | cat | while read f ; do echo "$f" ; echo "$f" | hexdump -C ; echo "" ;  done

ME="$(basename "$0")"
USAGE="$ME [ DIR ]"

DIR=.
[[ "$1" ]] && DIR="$1"

LC_ALL=C find . -name '*[![:print:]]*' \
| cat \
| while read f ; do
    echo "$f"
    echo -e "$f" | hexdump -C

    echo -n "non-printable characters: "
    echo "$f" \
    | hexdump -v -e '1/1 "%3d\n"' \
    | while read n ; do
        # printf "n=$n " >&2
        [[ "$n" -gt 128 ]] && printf "%02x " "$n"
    done \
    | tr '\n' ' '

    echo ""
    echo ""
done

