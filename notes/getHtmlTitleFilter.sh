#!/bin/bash
# Bryant Hansen

# dump the title from an html file
# read from stdin; dump to stdout
# takes no args

grep -i "<title>" | sed "s/.*<title[^>]*>//;s/<\/title>.*//"
