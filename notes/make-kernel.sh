#!/bin/bash
# Bryant Hansen

if ! mount -l | grep "^/dev/sda1 on /mnt/sda1 " > /dev/null ; then
	mount /dev/sda1 /mnt/sda1
fi
if mount -l | grep "^/dev/sda1 on /mnt/sda1 " > /dev/null ; then
	cd /usr/src/linux \
	&& make \
	&& make modules_install \
	&& /projects/utils/updateKernel2.sh | /bin/bash \
	&& grub2-mkconfig -o /mnt/sda1/boot/grub/grub.cfg \
	&& echo "kernel upgraded and installed" \
	|| echo "failed to update kernel"
fi
