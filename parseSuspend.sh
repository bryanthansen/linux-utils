#!/bin/bash
# Bryant Hansen

f=/var/log/pm-suspend.log

if [[ "$1" ]] ; then
    if [[ -f "$1" ]] ; then
        f="$1"
    else
        echo ERROR
        exit 1
    fi
fi

insert_tag() {
    header="\n\n############################################"
    footer="$header"
    echo -e "${header:2}"
    echo "$1"
    echo -e "$footer"
    return 0
}

cat "$f" | \
while read l ; do
    XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
done
