#!/bin/sh
# Bryant Hansen
# 20120531

# tuned for scanning black & white documents
# works with a single-page PDF input
# dumps a single-page png output

ME="$(basename "$0")"
USAGE="$ME  pdf_infile  [ png_outfile ]"

DPI=150
DEVICE=xcf
DEVICE=pngalpha
DEVICE=png16
DEVICE=pnggray

COMPRESS_OUTPUT=1
#unset COMPRESS_OUTPUT

echo "$ME - a script by Bryant" >&2

while [[ "${1:0:1}" == "-" ]] ; do
	echo "option detected: $1" >&1
	case $1 in
	"-d")
		echo "-d option detected" >&2
		shift
		echo "command-line-specified dpi: $1" >&2
		DPI=$1
		;;
	*)
		echo "unknown option: $1" >&2
		echo "exiting abnormally" >&2
		exit 2
		;;
	esac
	shift
done

echo "DPI=$DPI" >&2
dpi_invalid_chars="${DPI//[0-9]/}"
if [[ "$dpi_invalid_chars" ]] ; then
	echo "ERROR: bad dpi: \"${DPI}\"" >&2
	echo "Exiting Abnomally" >&2
	exit 3
fi

pdfin="$1"
pngout="$2"
[[ ! "$2" ]] && pngout="${pdfin%.pdf}".png


# deal_with_multipage
numPages="$(pdftk "$pdfin" dump_data | grep NumberOfPages | sed "s/NumberOfPages: //")"
echo "numPages=$numPages" >&2
if [[ ! "$numPages" ]] ; then
	echo "$ME WARNING: failed to calculate the number of pages for $pdfin" >&2
elif [[ "${numPages//[0-9]/}" ]] ; then
	echo "$ME WARNING: invalid result for numPages of ${pdfin} (contains at least 1 non-numeric character): $numPages" >&2
	# assume 1 and move on
	numPages=1
elif [[ "$numPages" -gt 1 ]] ; then
	echo "$ME $pdfin is a ${numPages}-page document.  First break this into single-page pdf's like this:" >&2
	echo "#   pdftk $pdfin burst" >&2
	echo "Exiting abnormally"
	exit 2
fi

echo "pdfin=$pdfin  pngout=$pngout" >&2

gs                                   \
	-dSAFER                      \
	-dBATCH                      \
	-dNOPAUSE                    \
	-dNOPROMPT                   \
	-dDOINTERPOLATE              \
	-sDEVICE=${DEVICE}           \
	-r${DPI}x${DPI}              \
	-sOutputFile="${pngout}"     \
	"${pdfin}"

COMPRESS_OUTPUT=1
#unset COMPRESS_OUTPUT

# reduce color depth at for the advantage of major space saving
if [[ "$COMPRESS_OUTPUT" ]] ; then
	mogrify -type Palette -colors 64 -colorspace Gray -quality 90 "${pngout}"
fi
