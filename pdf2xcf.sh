#!/bin/sh
# Bryant Hansen
# 20120531

# tuned for scanning black & white documents

ME="$(basename "$0")"
USAGE="$ME  pdf_infile  [ xcf_outfile ]"

# consider starting with:
# pdftk ../../3479_001.pdf burst

DPI=150
DEVICE=pngalpha
DEVICE=png16
DEVICE=pnggray
DEVICE=xcf

COMPRESS_OUTPUT=1
#unset COMPRESS_OUTPUT

while [[ "${1:0:1}" == "-" ]] ; do
	echo "option detected: $1" >&1
	case $1 in
	"-d")
		echo "-d option detected" >&2
		shift
		echo "command-line-specified dpi: $1" >&2
		DPI=$1
		;;
	*)
		echo "unknown option: $1" >&2
		echo "exiting abnormally" >&2
		exit 2
		;;
	esac
	shift
done

echo "DPI=$DPI" >&2
dpi_invalid_chars="${DPI//[0-9]/}"
if [[ "$dpi_invalid_chars" ]] ; then
	echo "ERROR: bad dpi: \"${DPI}\"" >&2
	echo "Exiting Abnomally" >&2
	exit 3
fi

#echo "test exit" >&2
#exit 1

pdfin="$1"
xcfout="$2"
[[ ! "$2" ]] && xcfout="${pdfin%.pdf}".xcf

echo "pdfin=$pdfin  xcfout=$xcfout" >&2
echo "# for multipage support, consider doing the following first: pdftk $pdfin burst" >&2

gs                                   \
	-dSAFER                      \
	-dBATCH                      \
	-dNOPAUSE                    \
	-dNOPROMPT                   \
	-dDOINTERPOLATE              \
	-sDEVICE=${DEVICE}           \
	-r${DPI}x${DPI}              \
	-sOutputFile="${xcfout}"     \
	"${pdfin}"

## reduce color depth at for the advantage of major space saving
if [[ "$COMPRESS_OUTPUT" ]] ; then
 	# ImageMagick mogrify is not yet tested (by me) on an Gimp xcf image
	mogrify -type Palette -colors 64 -colorspace Gray -quality 90 "${xcfout}"
fi
