#!/bin/bash
# Bryant Hansen

results="
    var1:val1
   vari2:valu2
  varia3:value3
"

echo "Original data: "
echo -e "$results"

echo "First attempt:"
echo -e "$results" | \
	while read result ; do
		echo "$(date "+%Y%m%d_%H%M%S"): $result"
	done
echo ""

echo "Corrected script:"
echo -e "$results"   | \
	sed "s/^/:\ /" | \
	while read result ; do
		echo "$(date "+%Y%m%d_%H%M%S")$result"
	done
echo ""
