#!/bin/bash
# Bryant Hansen

echo -n "(Y/n): "
a="$(read -n 1 t ; echo "${t}")"
if [[ "${a}" == "Y" ]] || [[ "${a}" == "y" ]] ; then
	echo -e " => answer is positive"
	exit 0
elif [[ "${a}" == "N" ]] || [[ "${a}" == "n" ]] ; then
	echo -e " => answer is negative"
	exit -1
else
	echo -e " => answer is invalid"
	exit -2
fi
