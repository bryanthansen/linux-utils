#!/usr/bin/env python3
# Bryant Hansen

# The goal of this is to create a 2D array from an entire file *without* loops
# For instance, a line can be split into substrings with a single .split() command, without using iterators
# Can this be done for files, splitting on a linefeed?
# Can the 2D split be done without iterators?

def readfile(filepath):
    print("Open %s" % filepath)
    f = open(filepath, 'r')
    contents = f.read()
    lines = split(contents, 

if __name__ == "__main__":
    readfile()
