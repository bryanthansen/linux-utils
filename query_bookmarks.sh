#!/bin/bash
# bha

# Dump open Firefox tabs to stdout

# Initial 1-liner
#cat query_bookmarks.sql.5 | sqlite3 /home/bha/.mozilla/firefox/mcl4r4h4.default/places.sqlite


FIREFOX_DIR=~/.mozilla/firefox/
SQL_PATH=.:/projects/utils

for dir in ${SQL_PATH//:/ } ; do
    SQL_FILE=${dir}/query_bookmarks.sql.5
    if [[ ! -f "$SQL_FILE" ]] ; then
        echo "Using SQL file $SQL_FILE to query bookmarks" >&2
    fi
done

if [[ ! -f "$SQL_FILE" ]] ; then
    echo "query_bookmarks.sql.5 not found in SQL PATH $SQL_PATH" >&2
    exit 2
fi

SQL_STATEMENT="$(cat "$SQL_FILE")"
echo -e "\n=================\nSQL STATEMENT\n-----------\n${SQL_STATEMENT}\n=================\n" >&2

sqlite_file="$(ls -1t $(find "$FIREFOX_DIR" -name places.sqlite) | head -n 1)"
echo "sqlite_file found at $sqlite_file" >&2

echo -e "$SQL_STATEMENT" | sqlite3 "$sqlite_file"


#echo -e '
#.separator ""
#SELECT \'<li><a href="', moz_places.url, '">\', moz_bookmarks.title, "</a>: ", moz_places.url, "</li>" FROM moz_bookmarks JOIN moz_places ON fk = moz_places.id;
#' | \
#sqlite3 /home/bha/.mozilla/firefox/mcl4r4h4.default/places.sqlite
