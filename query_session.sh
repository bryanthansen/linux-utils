#!/bin/bash
# Bryant Hansen

FIREFOX_DIR=~/.mozilla/firefox/

if [[ "$1" ]] ; then
    session_file="$1"
else
    session_file="$(ls -1t $(find "$FIREFOX_DIR" -name recovery.js) | head -n 1)"
fi

# query_session.sh 2>&1 | sed '/title: Bryant Hansen'\''s home page/d' | less

filter="
       title: Bryant Hansen's home page
         url: http://www.bryanthansen.net/
         url: about:blank
"
#echo -e "$filter"
echo -e "$filter" \
| (
    filter_regex=""
    while read f ; do
        [[ ! "$f" ]] && continue
        [[ "$filter_regex" ]] && filter_regex="${filter_regex}\|"
        # only the backslash must be escaped, but this is a bit complicated
        f2="${f//\//\\\/}"
        filter_regex="${filter_regex}${f2}"
    done
    echo "filter_regex: '${filter_regex}'"
    echo "filter expression: 'sed \"/${filter_regex}/d\"'"
)

if [[ -f "$session_file" ]] ; then
    printf "session_file found at ${session_file}\n\n" >&2
else
    printf "ERROR: session_file not found in ${FIREFOX_DIR}\n\n" >&2
    exit 2
fi

tokens='"tabs": \|"url": \|"originalURI": \|"title": '
tokens='"tabs": \|"url": \|"originalURI": '
tokens='"tabs": \|"url": \|"title": '

cat "$session_file" \
| python -m json.tool \
| grep -A 1 -B 1 "\"[a-zA-Z0-9]*\": "

exit 0

cat "$session_file" \
| python -m json.tool \
| grep -A 1 -B 1 "\"[a-zA-Z0-9]*\": " \
| grep "$tokens" \
| sed "s/^[ \t]*//;s/\,$//;s/\ \[//;s/\"//g;s/^\(title\|url\|originalURI\)/\t&/" \
| sed "s/originalURI/URI/" \
| sed "s/[ \t]*tabs:/\n&/" \
| sed "/^[ \t]*title: Yahoo News[ \t]*$/d" \
| sed "/^[ \t]*title: DuckDuckGo[ \t]*$/d" \
| sed "/^[ \t]*title: Facebook[ \t]*$/d" \
| sed "/^[ \t]*title: Firebug[ \t]*$/d" \
| sed "/^[ \t]*url: https:\/\/duckduckgo.com\/[ \t]*$/d"

exit 0

cat "$session_file" \
| python -m json.tool \
| grep -A 1 -B 1 "\"[a-zA-Z0-9]*\": " \
| grep "$tokens" \
| sed "s/^[ \t]*//;s/\,$//;s/\ \[//;s/\"//g;s/^\(title\|url\|originalURI\)/\t&/" \
| sed "
    s/originalURI/URI/;
    s/[ \t]*tabs:/\n&/;
    /^[ \t]*title: Yahoo News[ \t]*$/d;
    /^[ \t]*title: DuckDuckGo[ \t]*$/d;
    /^[ \t]*title: Facebook[ \t]*$/d;
    /^[ \t]*title: Firebug[ \t]*$/d;
    /^[ \t]*url: https:\/\/duckduckgo.com\/[ \t]*$/d
"

exit 0

#| sed "s/[ \t]*title: /\n&/" \
#| sed "/${filter_regex}/d"
