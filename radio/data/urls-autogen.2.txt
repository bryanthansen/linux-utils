# bash 1-liner used to generate:
# wgo http://www.listenlive.eu/germany.html 2>/dev/null | grep -A 1 -B 5 -i news | sed "s/<\/*t[dr]>//g;s/<\/[ab]>//g;s/<a\ href=\"//g" | grep -v "^[ \t]*$\|^<img.*" | while read l ; do [[ ! "${l//*<b>*/}" ]] && echo "${l##*<b>}" ; echo "${l%%<b>*}" ; done

# second-stage bash 1-liner:
# cat urls-autogen.txt | sed "s/\">/\n/g;s/\,\ http\:\/\//\n/g;s/<br\ \/>/\n/g" > urls-autogen.2.txt

Bayern 1
http://www.br-online.de/bayern1/
Munich
http://streams.br-online.de/bayern1_1.m3u
56 Kbps
streams.br-online.de/bayern1_2.m3u
128 Kbps
http://streams.br-online.de/bayern1_1.asx
48 Kbps
streams.br-online.de/bayern1_2.asx
128 Kbps
News/Oldies/Schlager
--
B5 Aktuell
http://www.br-online.de/b5aktuell/
Munich
http://streams.br-online.de/b5aktuell_1.m3u
56 Kbps
streams.br-online.de/b5aktuell_2.m3u
128 Kbps
http://streams.br-online.de/b5aktuell_1.asx
48 Kbps
streams.br-online.de/b5aktuell_2.asx
128 Kbps
News
--
B5 Plus
http://www.br-online.de/programm/digital-radio/digital-radio-DID1195677346958633/dab-b5-plus-digital-ID671195677341696766.xml
Munich
http://streams.br-online.de/b5plus_2.m3u
128 Kbps
News/Parliamentary debate
--
Deutschlandfunk
http://www.dradio.de/
Cologne
http://www.dradio.de/streaming/dlf.m3u
128 Kbps
News/information
--
hr-info
http://www.hr-online.de/website/radio/hr-info/
Frankfurt
http://metafiles.gl-systemhaus.de/hr/hrinfo_2.m3u
128 Kbps
News/Information
--
MDR 1 Radio Sachsen
http://www.mdr.de/mdr1-radio-sachsen/
Dresden
http://avw.mdr.de/livestreams/mdr1_radio_sachsen_live_128.m3u
128 Kbps
News/Information/Music
--
MDR 1 Radio Sachsen-Anhalt
http://www.mdr.de/mdr1-radio-sachsen-anhalt/
Magdeburg
http://avw.mdr.de/livestreams/mdr1_radio_sachsen-anhalt_live_128.m3u
128 Kbps
News/Information/Music
--
MDR 1 Radio Thüringen
http://www.mdr.de/mdr1-radio-thueringen/
Erfurt
http://avw.mdr.de/livestreams/mdr1_radio_thueringen_live_128.m3u
128 Kbps
News/Information/Music
--
MDR INFO Livestream
http://www.mdr.de/mdr-info/
Halle
http://avw.mdr.de/livestreams/mdr_info_live_56.m3u
56 Kbps
avw.mdr.de/livestreams/mdr_info_live_128.m3u
128 Kbps
News
--
Alternative/Indie
NE-WS 89.4
http://www.news894.de/

Neuss
javascript:void(window.open('http://213.200.64.229/freestream/download/news894/frameset.html','news894','resizable=1,width=660,height=510,top=60,left=60'))
WebPlayer
Top 40
--
NDR 90,3
http://www.ndr.de/903/
Hamburg
http://ndrstream.ic.llnwd.net/stream/ndrstream_ndr903_hi_mp3.m3u
128 Kbps
http://ndr-ndr903-hi-wma.wm.llnwd.net/ndr_ndr903_hi_wma
96 Kbps
News/Features
--
NDR 1 Niedersachsen
http://www.ndr.de/niedersachsen/
Hannover
http://ndrstream.ic.llnwd.net/stream/ndrstream_ndr1niedersachsen_hi_mp3.m3u
128 Kbps
News/Features
--
NDR 1 Radio MV
http://www.ndr.de/radiomv/
Schwerin
http://ndrstream.ic.llnwd.net/stream/ndrstream_ndr1radiomv_hi_mp3.m3u
128 Kbps
News/Features
--
NDR 1 Welle Nord
http://www.ndr.de/wellenord/

Flensburg
http://ndrstream.ic.llnwd.net/stream/ndrstream_ndr1wellenord_hi_mp3.m3u
128 Kbps
News/Features/Music
--
NDR Info
http://www.ndrinfo.de/

Hamburg
http://ndrstream.ic.llnwd.net/stream/ndrstream_ndrinfo_hi_mp3.m3u
128 Kbps
http://ndr-ndrinfo-hi-wma.wm.llnwd.net/ndr_ndrinfo_hi_wma
64 Kbps
News
--
NDR Info Spezial
http://www.ndrinfo.de/

Hamburg
http://ndrstream.ic.llnwd.net/stream/ndrstream_ndrinfo_spezial_hi_mp3.m3u
128 Kbps
http://ndr-ndrinfo-spezial-hi-wma.wm.llnwd.net/ndr_ndrinfo_spezial_hi_wma
64 Kbps
News
--
RBB InfoRadio
http://www.inforadio.de/

Berlin
http://www.inforadio.de/live.wax
128 Kbps
http://www.inforadio.de/live.m3u
128 Kbps
News/Information
--
SR 3 Saarlandwelle
http://www.sr-online.de/sr3/

Saarbrücken
http://streaming01.sr-online.de/sr3_1.m3u
56 Kbps
streaming01.sr-online.de/sr3_2.m3u
128 Kbps
News/Information
--
SWR1 RP
http://www.swr.de/swr1/

Mainz
http://wma-live.swr.de/swr1rp_m.asx
128 Kbps
http://mp3-live.swr.de/swr1rp_s.m3u
48 Kbps
mp3-live.swr.de/swr1rp_m.m3u
128 Kbps
News/AC/Oldies
--
SWR1 BW
http://www.swr.de/swr1/

Stuttgart
http://wma-live.swr.de/swr1bw_m.asx
128 Kbps
http://mp3-live.swr.de/swr1bw_s.m3u
48 Kbps
mp3-live.swr.de/swr1bw_m.m3u
128 Kbps
News/AC/Oldies
--
WDR5
http://www.wdr5.de/

Cologne
http://www.wdr.de/wdrlive/media/wdr5.m3u
128 Kbps
News/specialist music
