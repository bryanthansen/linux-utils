#!/bin/bash
# Bryant Hansen

# intended to be called like this:
#  wget -O- "$1" | dump_http_urls

ME="$(basename "$0")"
USAGE="$ME"

# files ending in .mp3 that may or may not have some whitespace following until the end of the line
FILTER=".*\.mp3[ \t]*$\|.*\.txt[ \t]*$"
FILTER=".*\.html[ \t]*"

SED_ADD_NEWLINE_AFTER_QUOTE="s/[\"\']/\n/g"
SED_ADD_NEWLINE_BEFORE_HTTP="s/http\:/\nhttp\:/g"
SED_DELETE_BLANKS="/^[ \t]*$/d"
SED_PRINT_MP3_URLS="/^http.*\.mp3[ \t]*$/p"
SED_PRINT_HTML_URLS="/^http.*\.html[ \t]*$/p"
# sed "s/[\"\']/\n/g;s/http\:/\nhttp\:/g;/^[ \t]*$/d" | \
# = sed "${SED_ADD_NEWLINE_AFTER_QUOTE};${SED_ADD_NEWLINE_BEFORE_HTTP};${SED_DELETE_BLANKS}" | \

sed "${SED_ADD_NEWLINE_AFTER_QUOTE};${SED_ADD_NEWLINE_BEFORE_HTTP};${SED_DELETE_BLANKS}" | \
sed "s/[\"\']/\n/g;s/http\:/\nhttp\:/g;/^[ \t]*$/d" | \
sed -n "/^http.*$/p" | \
sort -r | \
uniq
