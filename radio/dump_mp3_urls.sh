#!/bin/bash
# Bryant Hansen

# Dump all mp3 urls on a page
# Note: can not have newlines, spaces or whatever non-url-like syntax in the name

ME="$(basename "$0")"
USAGE="$ME"

SED_ADD_NEWLINE_AFTER_QUOTE="s/[\"\']/\n/g"
#SED_ADD_NEWLINE_BEFORE_HTTP="s/http\:/\nhttp\:/g"
SED_ADD_NEWLINE_BEFORE_HTTP="s/http\:/\n&/g"
SED_DELETE_BLANKS="/^[ \t]*$/d"
SED_GREP_HTTP_MP3="/^http.*\.mp3[ \t]*$/p"

sed "${SED_ADD_NEWLINE_AFTER_QUOTE};
        ${SED_ADD_NEWLINE_BEFORE_HTTP};
        ${SED_DELETE_BLANKS}" | \
sed -n "$SED_GREP_HTTP_MP3" | \
sort -r | \
uniq
