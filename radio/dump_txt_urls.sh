#!/bin/bash
# Bryant Hansen

ME="$(basename "$0")"
USAGE="$ME"


###############################################################
# Functions
###############################################################
dump_txt_urls() {
    SED_ADD_NEWLINE_AFTER_QUOTE="s/[\"\']/\n/g"
    SED_ADD_NEWLINE_BEFORE_HTTP="s/http\:/\nhttp\:/g"
    SED_DELETE_BLANKS="/^[ \t]*$/d"
    SED_PRINT_TXT_URLS="/^http.*\.txt[ \t]*$/p"
    # sed "s/[\"\']/\n/g;s/http\:/\nhttp\:/g;/^[ \t]*$/d" | \
    # = sed "${SED_ADD_NEWLINE_AFTER_QUOTE};${SED_ADD_NEWLINE_BEFORE_HTTP};${SED_DELETE_BLANKS}" | \

    sed "s/[\"\']/\n/g;s/http\:/\nhttp\:/g;/^[ \t]*$/d" | \
    sed -n "/^http.*$/p" | \
    sed -n "/^http.*\.txt[ \t]*$/p" | \
    sort -r | \
    uniq
}

dump_txt_urls

