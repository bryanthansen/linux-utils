#!/bin/bash
# Bryant Hansen

# Dump all urls on a page
# Note: can not have newlines, spaces or whatever non-url-like syntax in the name

ME="$(basename "$0")"
USAGE="$ME"

SED_ADD_NEWLINE_BEFORE_HREF="s/href=/\n&/g"
SED_DELETE_BLANKS="/^[ \t]*$/d"
SED_GREP_HTTP="/\(^http.*\|^href=.*\)/!d"
SED_STRIP_QUOTES="s/href=\"//g;s/\".*//"

sed "
    ${SED_ADD_NEWLINE_BEFORE_HREF};
    ${SED_DELETE_BLANKS};
" \
| sed "
    ${SED_GREP_HTTP};
    ${SED_STRIP_QUOTES};
" \
| sort -r \
| uniq
