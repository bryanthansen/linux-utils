#!/bin/bash
# Bryant Hansen

# Description:
# Search for mp3 files in a URL, fetch them and save them to a local directory

ME="$(basename "$0")"
USAGE="$ME  [  URL  [  DIR  ]  ]"

# URL="http://www.alternativeradio.org/arpodcast.xml"
DIR="./files"
# doesn't seem to get any urls since 20130329
URL="http://dlfwissbildung.radio.de/"
# Updated 20130409
URL="http://www.dradio.de/portale/wissenschaft/"

#WGET_OPTS='-U "Mozilla/5.0 (X11; Linux x86_64; rv:24.0) Gecko/20100101 Firefox/24.0"'

[[ -f "site.conf" ]] && . site.conf

# command line overrides site.conf settings
[[ "$1" ]] && URL="$1"
[[ "$2" ]] && DIR="$2"

[[ ! -d "$DIR" ]] && mkdir -p "$DIR"
[[ ! -d "$DIR" ]] && echo "$DIR does not exist and we failed to create it.  Exiting abnormally." >&2 && exit 2

[[ ! "$URL" ]] && echo "URL not specified.  Exiting abnormally." >&2 && exit 2


cd "$DIR" && \
echo -e "\n  *** checking $URL for mp3 files ***\n  *** wget -O- $WGET_OPTS $URL ***" >&2 && \
wget -O- "$WGET_OPTS" "$URL" \
| tee ./fetch_$(dt).html \
| /projects/utils/radio/dump_mp3_urls.sh \
| while read url ; do
    f="$(basename "$url")"
    if [[ -f "$f" ]] ; then
        echo "Currently-available episode: $url" >&2
        echo "$f as already been fetched locally to ${DIR}" >&2
        ls -lh "$f" >&2
    else
        echo "Fetching $url to $DIR ..." >&2
        wget "$WGET_OPTS" "$url"
    fi
done
