#!/bin/bash
# Bryant Hansen

ME="$(basename "$0")"
USAGE="$ME"

dir=files
logdir=${dir}/fetch_$(dt)

[[ ! -d "$dir" ]] && mkdir -p "$dir"
if [[ ! -d "$dir" ]] ; then
        echo "$0 ERROR: failed to create $dir in $PWD" >&2
        echo "Exiting abnormally" >&2
        exit 2
fi
[[ ! -d "$logdir" ]] && mkdir -p "$logdir"
if [[ ! -d "$logdir" ]] ; then
        echo "$0 ERROR: failed to create $logdir in $PWD" >&2
        echo "Exiting abnormally" >&2
        exit 2
fi


###############################################################
# Constants
###############################################################

OUTFILE="${logdir}/index.html"
OUTFILE_MP3_LIST="${logdir}/mp3_list.txt"
OUTFILE_URL_LIST="${logdir}/url_list.txt"


###############################################################
# Functions
###############################################################

dump_mp3_urls() {
    SED_ADD_NEWLINE_AFTER_QUOTE="s/[\"\']/\n/g"
    SED_ADD_NEWLINE_BEFORE_HTTP="s/http\:/\nhttp\:/g"
    SED_DELETE_BLANKS="/^[ \t]*$/d"
    SED_PRINT_MP3_URLS="/^http.*\.mp3[ \t]*$/p"
    # sed "s/[\"\']/\n/g;s/http\:/\nhttp\:/g;/^[ \t]*$/d" | \
    # = sed "${SED_ADD_NEWLINE_AFTER_QUOTE};${SED_ADD_NEWLINE_BEFORE_HTTP};${SED_DELETE_BLANKS}" | \

    wget -O- "$1" | \
        tee "$OUTFILE" | \
        sed "s/[\"\']/\n/g;s/http\:/\nhttp\:/g;/^[ \t]*$/d" | \
        sed -n "/^http.*$/p" | \
        tee "$OUTFILE_URL_LIST" | \
        sed -n "/^http.*\.mp3[ \t]*$/p" | \
        grep -v "gateway\.mp3" | \
        sort -r | \
        uniq | \
        tee "$OUTFILE_MP3_LIST"
}

fetch_mp3_urls() {
    while read url ; do
        name="$(basename "$url")"
        if [[ ! -f "${dir}/$name" ]] ; then
            echo "fetching $url" >&2
            wget -O- "$url" > "${dir}/${name}"
        #else
        #    echo "# ${dir}/$name already exists.  It appears that $url has already been fetched" >&2
        fi
    done
}


###############################################################
# Main
###############################################################

dump_mp3_urls "http://www.bbc.co.uk/podcasts/series/discovery/all" | fetch_mp3_urls
