#!/bin/bash
# Bryant Hansen

# examples:
# ./fetch.sh
# ./fetch.sh 20121228

lastFriday=$(date --date='last friday' "+%Y%m%d")

[[ ! -d files ]] && mkdir files
pushd files

[[ "$1" ]] && lastFriday="$1"

for n in 1 2 3 4 5 6 7 8 9 ; do
    [[ ! -f "scifri${lastFriday}${n}.mp3" ]] && \
    wget http://www.podtrac.com/pts/redirect.mp3/traffic.libsyn.com/sciencefriday/scifri${lastFriday}${n}.mp3
done

popd
