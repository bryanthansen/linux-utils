#!/bin/bash
# (c)2006-2011 Bryant Hansen

DATDIR=/projects/utils/radio/
URLS=/projects/utils/radio/data/urls.txt
#konsole --profile "MPlayer Window" -e 'mplayer $(cat /projects/utils/radio/urls-autogen.5.edit.txt | grep "^http.*")' &
#xterm -e "mplayer $(cat $URLS | grep "^http.*")" &
#mplayer -playlist "$URLS"
xterm -e "mplayer -playlist \"$URLS\"" &
