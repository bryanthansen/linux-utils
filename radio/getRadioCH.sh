#!/bin/bash
# Bryant Hansen
# 20110416

# initial 1-liner:
# wget -O- http://www.radio.ch/de/podcasts/ 2>/dev/null | grep -n "podcast.*\.mp3" | sed "s/^.*\"\/media\/podcast\//media\/podcast\//;s/\.mp3\".*$/\.mp3/" | while read f ; do mplayer http://www.radio.ch/${f} ; done

# notes:
# 1) get a list of podcasts available from the HTML source code of the default page
#       wget -O- http://www.radio.ch/de/podcasts/ 2>/dev/null | grep -n "podcast.*\.mp3"
# 2) (optional / TODO): save the output to a file
# 3) play the source with a media player, such as
#       mplayer, which does a good job of:
#          recognizing and playing back many stream types from either:
#             the filesystem
#             the net, or
#             a stdin stream

USAGE="$0 [ -s ]"

if [[ "$1" == "-s" ]] ; then
    SIMULATE=1
else
    SIMULATE=0
fi

PREFIX="media\/podcast\/"
SUFFIX="\.mp3"

OUT_DIR="/data/media/radio/www.radio.ch"
[[ ! -d "$OUT_DIR" ]] && mkdir -p "$OUT_DIR"
[[ ! -d "$OUT_DIR" ]] && echo "FAILED TO CREATE DIRECTORY $OUT_DIR  Exiting abnormally" >&2 && exit 10
touch "$OUT_DIR"/.test.tmp || ( echo "no write permissions to directory $OUT_DIR  Exiting abnormally" >&2 && exit 11 )

wget -O- http://www.radio.ch/de/podcasts/ 2>/dev/null | \
    grep -n "podcast.*\.mp3" | \
    sed "s/^.*\"\/${PREFIX}/${PREFIX}/;s/${SUFFIX}\".*$/${SUFFIX}/" | \
    while read f ; do
        # This old podcast keeps showing up:
        #   http://www.radio.ch/media/podcast/podcasts00202502_1.mp3
        # Must filter out
        b="$(basename "$f")"
        [[ "$b" ]] && [[ ! "${b%%podcast*}" ]] && continue
        cmd="mplayer http://www.radio.ch/${f}"
        cmd="wget -O- http://www.radio.ch/${f} | tee \"${OUT_DIR}/$(basename "$f")\" | mpg123 -"
        cmd="wget -O- http://www.radio.ch/${f} | tee \"${OUT_DIR}/$(basename "$f")\" | mplayer -cache 8192 -"
        echo "${cmd}"
        [[ ! "$SIMULATE" ]] || (( ! "$SIMULATE" )) && \
            echo ${cmd} | /bin/bash

    done
