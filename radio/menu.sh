#!/bin/bash
# Bryant Hansen

# Simple internet radio script

ME="$(basename "$0")"
USAGE="$ME [ URL ]"

CONF="$(dirname "$0")/site.conf"
[[ -f "$CONF" ]] && . "$CONF"

CONF="$(dirname "$0")/${ME%.sh}.conf"
[[ -f "$CONF" ]] && . "$CONF"

[[ "$1" ]] && URL="$1"

if [[ ! "$URL" ]] ; then
    echo "$ME ERROR: URL is not defined.  Exiting abnormally." >&2
    exit 2
fi

tmp="$(tempfile)"

#DELETE_FIRST=1
urls=$(
        wget -O- $URL \
        | /projects/utils/radio/dump_mp3_urls.sh \
        | ( [[ "$DELETE_FIRST" ]] && sed 1d || cat - )
    )

echo "urls=$urls" >&2

filenames=(
    $(
        for f in ${urls[@]} ; do
            basename "$f"
        done
    )
)
numfiles="$(echo -e "${filenames[@]}" | tr ' ' '\n' | wc -l)"

echo "numfiles=$numfiles  filenames=$filenames" >&2

n=0
for item in ${filenames[@]} ; do
    menuitems="$menuitems $n ${item// /_}" # subst. Blanks with "_"
    let n+=1
done

if [[ ! "$menuitems" ]] ; then
    echo "${ME}: no new mp3 files found at $URL" >&2
    exit 3
fi

echo "menuitems=$menuitems" >&2

dialog \
    --backtitle "Simple Internet Radio Script - $URL" \
    --title " $numfiles MP3 files found on $URL " \
    --menu "Choose a title to play" \
    16 \
    100 \
    8 \
    $menuitems \
    2> "$tmp"

if [ $? -eq 0 ]; then
    item=`cat "$tmp"`
    url=${urls[$(cat "$tmp")]}
    filename=${filenames[$(cat "$tmp")]}
    (( item++ ))
    echo "Item: $item" >&2
    echo "URL: $url" >&2
    echo "Filename: $filename" >&2
    echo "echo playing ${url}..." >&2
    mplayer -cache 8192 $url
fi
rm "$tmp"
