#!/bin/bash
# Bryant Hansen

# Simple dialog for displaying mp3 urls and allowing multiple to be selected and played

ME="$(basename "$0")"
USAGE="$ME [ URL ]"

#SKIP_FIRST=1

CONF="$(dirname "$0")/site.conf"
[[ -f "$CONF" ]] && . "$CONF"

CONF="$(dirname "$0")/${ME%.sh}.conf"
[[ -f "$CONF" ]] && . "$CONF"

[[ "$1" ]] && URL="$1"

if [[ ! "$URL" ]] ; then
    echo "$ME ERROR: URL is not defined.  Exiting abnormally." >&2
    exit 2
fi

tmp="$(tempfile)"
playlist_tmpfile="$(tempfile)"

urls=(
    $(
        wget -O- $URL \
        | /projects/utils/radio/dump_mp3_urls.sh \
        | ( [[ "$SKIP_FIRST" ]] && sed 1d || cat - )
    )
)

echo "urls: ${urls[*]}" >&2
echo "url indexes: ${!urls[*]}" >&2

filenames=(
    $(
        for f in ${urls[@]} ; do
            basename "$f"
        done
    )
)
#numfiles="$(echo -e "${filenames[@]}" | tr ' ' '\n' | wc -l)"
numfiles="$(echo -e "${#filenames[*]}")"
echo "numfiles=$numfiles  filenames=${filenames[*]}" >&2

for urlidx in ${!urls[*]} ; do
    echo "  urlid: ${urlidx}, url: ${urls[${urlidx}]}, file: ${filenames[${urlidx}]}"
done

n=0
menuitems=""
for item in ${filenames[@]} ; do
    menuitems="$menuitems $n ${item// /_} off" # subst. Blanks with "_"
    let n+=1
done

if [[ ! "$menuitems" ]] ; then
    echo "${ME}: no new mp3 files found at $URL" >&2
    exit 3
fi

echo -e "menuitems=$menuitems" >&2

# TODO: consider auto-sizing for the screen
TERM_ROWS="$(stty -a | head -n 1 | sed "s/^.* rows //;s/\;.*$//")"
if [[ "$TERM_ROWS" ]] ; then
    if (( "$TERM_ROWS" > 25 )) ; then
        ROWS="`expr $TERM_ROWS - 20`"
    elif (( "$TERM_ROWS" > 20 )) ; then
        ROWS=20
    else
        ROWS=10
    fi
else
    ROWS=10
fi
echo "TERM_ROWS=$TERM_ROWS  ROWS=$ROWS" >&2
TERM_COLS="$(stty -a | head -n 1 | sed "s/^.* columns //;s/\;.*$//")"
if [[ "$TERM_COLS" ]] ; then
    if (( $TERM_COLS > 100 )) ; then
        COLS=$TERM_COLS
    elif (( $TERM_COLS > 10 )) ; then
        COLS=10
    else
        COLS=10
    fi
else
    COLS=10
fi
echo "TERM_COLS=$TERM_COLS  COLS=$COLS" >&2

dialog \
    --backtitle "Simple Internet Radio Script - $URL" \
    --title " $numfiles MP3 files found on $URL " \
    --checklist "Select titles to play" \
    $(expr $ROWS + 8) \
    100 \
    $ROWS \
    $menuitems 2> "$tmp"
ret=$?
echo "" >> "$tmp"

echo "Selected URLs: $(cat "$tmp")" >&2
if [ $ret -eq 0 ] ; then
    cat "$tmp" | \
    tr ' ' '\n' | \
    while read urlidx ; do
        echo "   urlidx: $urlidx" >&2
        [[ ! "$urlidx" ]] && continue
        #(( urlidx-- )) # displaying 1-based, but indexing zero-based
        url=${urls[$urlidx]}
        echo "${url}" >> "$playlist_tmpfile"
        echo "   url[${urlidx}]: ${url}" >&2
        echo "   filename[$urlidx]=${filenames[$urlidx]}" >&2
    done
    echo "mplayer -playlist \"$playlist_tmpfile\""
    mplayer -playlist "$playlist_tmpfile"
fi
rm -f "$tmp" "$playlist_tmpfile"
