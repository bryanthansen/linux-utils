#!/bin/bash
# Bryant Hansen

ME="$(basename "$0")"
USAGE="$ME"

dir=files
logdir=${dir}/fetch_$(dt)

[[ ! -d "$dir" ]] && mkdir -p "$dir"
if [[ ! -d "$dir" ]] ; then
        echo "$0 ERROR: failed to create $dir in $PWD" >&2
        echo "Exiting abnormally" >&2
        exit 2
fi
[[ ! -d "$logdir" ]] && mkdir -p "$logdir"
if [[ ! -d "$logdir" ]] ; then
        echo "$0 ERROR: failed to create $logdir in $PWD" >&2
        echo "Exiting abnormally" >&2
        exit 2
fi

###############################################################
# Constants
###############################################################
OUTFILE="${logdir}/index.html"
OUTFILE_MP3_LIST="${logdir}/mp3_list.txt"
OUTFILE_URL_LIST="${logdir}/url_list.txt"
OUTFILE_PLAYLIST="${logdir}/playlist.txt"


###############################################################
# Functions
###############################################################
dump_mp3_urls() {
    SED_ADD_NEWLINE_AFTER_QUOTE="s/[\"\']/\n/g"
    SED_ADD_NEWLINE_BEFORE_HTTP="s/http\:/\nhttp\:/g"
    SED_DELETE_BLANKS="/^[ \t]*$/d"
    SED_PRINT_MP3_URLS="/^http.*\.mp3[ \t]*$/p"
    # sed "s/[\"\']/\n/g;s/http\:/\nhttp\:/g;/^[ \t]*$/d" | \
    # = sed "${SED_ADD_NEWLINE_AFTER_QUOTE};${SED_ADD_NEWLINE_BEFORE_HTTP};${SED_DELETE_BLANKS}" | \

    echo "fetching list of available mp3s from $1" >&2

    wget -O- "$1" | \
        tee "$OUTFILE" | \
        sed "s/[\"\']/\n/g;s/http\:/\nhttp\:/g;/^[ \t]*$/d" | \
        sed -n "/^http.*$/p" | \
        tee "$OUTFILE_URL_LIST" | \
        sed -n "/^http.*\.mp3[ \t]*$/p" | \
        grep -v "gateway\.mp3" | \
        sort -r | \
        uniq
}

fetch_mp3_urls() {
    while read url ; do
        name="$(basename "$url")"
        if [[ -f "${dir}/$name" ]] ; then
            echo "${dir}/$name already exists.  It appears that $url has already been fetched" >&2
        else
            echo "fetching $url" >&2
            wget -O- "$url" > "${dir}/${name}"
        fi
    done
}

make_playlist() {
    while read link ; do
        echo -n "${dir}/"
        basename "$link"
    done
}

wait_for_file_to_cache() {
    cachesize=10000
    [[ "$CACHE_SIZE" ]] && cachesize=$CACHE_SIZE
    while read f ; do
        # wait up to 20 seconds, checking every 2
        for n in seq `1 10` ; do
            filesize="$(stat --format="%s" "${f}")"
            [[ "$filesize" -gt "$cachesize" ]] && break
            sleep 2
        done
        filesize="$(stat --format="%s" "${f}")"
        if [[ "$filesize" -lt "$cachesize" ]] ; then
            echo "Failed to download ${f}.  cache didn't fill.  filesize=$filesize" >&2
            echo "Wait for next file..." >&2
        else
            return 0
        fi
    done
    echo "ERROR: wait_for_file_to_cache failed to fetch any file in the list" >&2
    return 2
}


###############################################################
# Main
###############################################################

# get the list of mp3 URL synchronously
dump_mp3_urls "http://dlfwissbildung.radio.de/" | tee "$OUTFILE_MP3_LIST" >&2

# fetch them in background; start playing in foreground
cat "$OUTFILE_MP3_LIST" | fetch_mp3_urls &

# build a playlist based on the files fetched
cat "$OUTFILE_MP3_LIST" | make_playlist | tee "$OUTFILE_PLAYLIST" >&2

# wait for cache to fill on the first (valid) file
cat "$OUTFILE_PLAYLIST" | wait_for_file_to_cache

ln -fs "$OUTFILE_PLAYLIST" ./playlist.txt

# start playing as a playlist
mplayer -playlist "./playlist.txt" -loop 0


# ideas to simultaneously record and play:
#        while read url ; do
#                mp3_outfile="${dir}/$(basename "$url")"
#                wget -O- "$url" | tee "$mp3_outfile" | mplayer -cache 8192 -
#                    (disadvantage: interrupting leaves a partial file)
#                  or:
#                wget --output-file="$mp3_outfile" "$url" ; sleep 5 ; mplayer "$mp3_outfile"
#                  or simply:
#                wget "$url" ; sleep 5 ; mplayer "$mp3_outfile"
#                  (must trust that basename returns the same name that wget determines)
#                    (disadvantage: must complete a fetch before play begins)
#        done
