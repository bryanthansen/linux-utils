#!/bin/bash
# Bryant Hansen
# 20110623

# Uses uuencode for generating ascii chars
# This can be found in the following Gentoo package: app-arch/sharutils

# TODO: Here is a method of including Uppercase, Lowercase, and some reasonably-safe symbols
# Symbols can be added or removed to the set in the 'tr -d ...' statements
# head -c 4096 /dev/random | tr -cd '[:print:]' | tr -d '[:space:]|;*!"`' | tr -d "'"
# \^mKtzlYwM?h?\@%51pzt8~$Q+5nAXWBu0:yllfwv_F5Hd2faTN_pQ,l2KI<w<))f6g0<ZK:?3MYOTZN}lD6


USAGE="$0 [ length ]"

LENGTH=256
[[ "$1" ]] && LENGTH=$1

if ! which uuencode > /dev/null 2>&1 ; then
	printf "$0 ERROR: this script depends on uuencode.  If using gentoo, emerge sharutils" >&2
	exit 1
fi

r=$(head -c $LENGTH /dev/random | uuencode -m - | tail -n 2 | head -n 1)
echo "${r:0:${LENGTH}}"
