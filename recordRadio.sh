#!/bin/bash
# Bryant Hansen

ME="$(basename "$0")"
DIR="$(dirname "$0")"

LOGDIR=/home/gink/var/log
LOGFILE="${LOGDIR}/${ME}.log"

DURATION=10
URL=http://128.208.34.80:8010/
OUTFILE="/data/media/radio/$(dt).mp3"

echo "$(date) ${ME}: recording $URL to $OUTFILE (duration=${DURATION})" >> "$LOGFILE"

/projects/utils/dumpUrlStreamToFile.sh "$OUTFILE" "$URL" "$DURATION" \
    >> "$LOGFILE" 2>&1

