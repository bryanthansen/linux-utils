#!/bin/bash -x
# This script builds a new KNOPPIX ISO image.
# Copyright (C) 2004 by Marc Haber <mh+knoppix-remaster@zugschlus.de>
# License: GPL V2

# set -o verbose

ROOT="$PWD"

USAGE="$0 [ ROOT_DIRECTORY ]"
HELP="The root directory is the directory containing the source & master directories.  The current directory will be used if no password is provided."

TWO_STAGE_ISO="1"

if [ -d "$1" ] ; then
   ROOT="$1"
fi
echo "ROOT=$ROOT"
echo "ctrl-C to cancel"
read -n 1

SOURCE="$ROOT/source-20060801"
MASTER="$ROOT/master"
CLOOPTARGET="$ROOT/master/KNOPPIX/KNOPPIX"
TARGET="$ROOT"
EXCLUDELIST="$ROOT/source/excludelist"
FINAL_OUTPUT_ISO=$TARGET/knoppix-autobuild.iso

if [ ! -d "$SOURCE" ] ; then
	echo "$SOURCE not found"
	exit -1
fi

if [ ! -d "$MASTER" ] ; then
	echo "$MASTER not found"
	exit -1
fi

if [ ! -d "$TARGET" ] ; then
	echo "$TARGET not found"
	exit -1
fi

rm -rf $SOURCE/.rr_moved
cd $SOURCE

if [ -f "$CLOOP_TARGET" ] ; then
   echo "deleting old $CLOOPTARGET"
   rm -f "$CLOOP_TARGET"
fi

echo "Creating $CLOOPTARGET from ${SOURCE}/KNOPPIX...."
# echo "WARNING: compression currently disabled with \"-L 0\" arg to create_compressed_fs!"
echo "starting at `date`"

if [ ! -z "$TWO_STAGE_ISO" ] ; then

   echo "2-stage iso build: creating raw uncompressed ISOs..."

   mkisofs -R -U -V "KNOPPIX filesystem - bry" -publisher "Bryant Hansen www.bryanthansen.net" \
		-split-output -o "${CLOOPTARGET}.iso" \
		-hide-rr-moved -cache-inodes -no-bak -pad \
		"$SOURCE/KNOPPIX"

   RESULT="$?"
   if (( "$RESULT" )) ; then
      echo "ERROR: mkisofs exit code = $RESULT!  Exiting abnormally."
      exit $RESULT
   fi

   for iso_file in "${CLOOPTARGET}.iso_"* ; do
        COMPRESSED_ISO="${iso_file/.iso_}"
        NUM="${COMPRESSED_ISO##*iso_0}"
        NUM="`expr "$NUM" + 1`"
	COMPRESSED_ISO="${file%.iso_0[0-9]*}`expr ${file#*0} + 1`"
	if [ "$NUM" = "1" ] ; then
	    COMPRESSED_ISO="${file%.iso_0[0-9]*}"
	fi
       if [ -f "$COMPRESSED_ISO" ] ; then
           echo "$COMPRESSED_ISO exists.  Removing automatically..."
           rm "$COMPRESSED_ISO"
        fi
   	echo -e "\nwriting iso file \"$iso_file\" to compressed iso file \"$COMPRESSED_ISO\"..."
   	nice -5 /usr/bin/create_compressed_fs -s 5g -f ./comp_temp.tmp -B 65536 "$iso_file" "$COMPRESSED_ISO"
        echo "compressed ISO \"$COMPRESSED_ISO\" writing.  you should delete the uncompressed ISO: \"$iso_file\"."
   done

else
   echo "creating single-stage compressed ISO."
   mkisofs -R -U -V "KNOPPIX filesystem - bry" -publisher "Bryant Hansen www.bryanthansen.net" \
		-hide-rr-moved -cache-inodes -no-bak -pad "$SOURCE/KNOPPIX"   \
		| nice -5 /usr/bin/create_compressed_fs -s 5g -f ./comp_temp.tmp -B 65536 - "$CLOOPTARGET"
fi

# verify the integrity of our target compressed image
TARGSIZE=`ls -s "$CLOOPTARGET" | awk '{ print $1 }'`
LS_RESULT="$?"
echo "$CLOOPTARGET: TARGSIZE=$TARGSIZE, LS_RESULT=$LS_RESULT."
echo "completed first stage at `date`"

cd $MASTER
rm -f KNOPPIX/md5sums
find -type f -not -name md5sums -not -name boot.cat -exec md5sum {} \; >> KNOPPIX/md5sums

# mkisofs -pad -l -r -J -v -V "KNOPPIX" -b KNOPPIX/boot.img -c KNOPPIX/boot.cat -hide-rr-moved -o $TARGET/knoppix-autobuild-script.iso $MASTER

mkisofs -pad -l -r -J -v -V "KNOPPIX" -no-emul-boot -boot-load-size 4    \
	-boot-info-table -b boot/isolinux/isolinux.bin -c boot/isolinux/boot.cat    \
	-hide-rr-moved -o "$FINAL_OUTPUT_ISO" $MASTER

echo "completed final stage at `date`"
