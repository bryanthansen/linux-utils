#!/bin/bash
# This script builds a new KNOPPIX ISO image.
# Copyright (C) 2004 by Marc Haber <mh+knoppix-remaster@zugschlus.de>
# License: GPL V2

# set -o verbose

ROOT="$PWD"

USAGE="$0 [ ROOT_DIRECTORY ]"
HELP="The root directory is the directory containing the source & master directories.  The current directory will be used if no password is provided."

TWO_STAGE_ISO="1"

if [ -d "$1" ] ; then
   ROOT="$1"
fi
echo "ROOT=$ROOT"
#echo "ctrl-C to cancel"
#read -n 1

SOURCE="$ROOT/source-20061002"
SOURCE2="$ROOT/source-20061002-2"
MASTER="$ROOT/master"
CLOOPTARGET="$ROOT/master/KNOPPIX/KNOPPIX"
TARGET="$ROOT"
EXCLUDELIST="$ROOT/source/excludelist"
FINAL_OUTPUT_ISO="$TARGET/knoppix-autobuild-`date "+%Y%m%d"`.iso"
COMPRESSED_TEMPFILE="${ROOT}/create_compressed_fs.tmp"

verify_configuration()
{

if [ ! -d "$SOURCE" ] ; then
	echo "$SOURCE not found"
	exit -1
fi

if [ ! -z "$TWO_STAGE_ISO" ] ; then
    if [ ! -d "$SOURCE2" ] ; then
	echo "$SOURCE2 not found"
	exit -1
    fi
fi

if [ ! -d "$MASTER" ] ; then
	echo "$MASTER not found"
	exit -1
fi

if [ ! -d "$TARGET" ] ; then
	echo "$TARGET not found"
	exit -1
fi

rm -rf $SOURCE/.rr_moved
cd $SOURCE

if [ -f "$CLOOP_TARGET" ] ; then
   echo "deleting old $CLOOPTARGET"
   rm -f "$CLOOP_TARGET"
fi

}

make_compressed_images()
{
if [ ! -z "$TWO_STAGE_ISO" ] ; then

   echo "2-stage iso build: creating raw uncompressed ISOs..."

   rm -rf $SOURCE2/.rr_moved

   echo "Creating $CLOOPTARGET from ${SOURCE}...."
   # echo "WARNING: compression currently disabled with \"-L 0\" arg to create_compressed_fs!"
   echo "starting at `date`"

    #TODO: use the KNOPPIX2_INCLUDES.txt list to exclude the files that should *not* be in KNOPPIX - allowing this to be read from a common directory
    # use -exclude-list "${ROOT}/KNOPPIX2_INCLUDES.txt"
#   mkisofs -R -U -V "KNOPPIX filesystem - bry" -publisher "Bryant Hansen www.bryanthansen.net" \
# 		-m "${ROOT}/KNOPPIX2_INCLUDES.txt" \
#		-split-output -o "${CLOOPTARGET}.iso" \
#		-hide-rr-moved -cache-inodes -no-bak -pad \
#		"$SOURCE/KNOPPIX"
   mkisofs -R -U -V "KNOPPIX filesystem - bry" -publisher "Bryant Hansen www.bryanthansen.net" \
		-hide-rr-moved -cache-inodes -no-bak -pad "${SOURCE}"   \
		| nice -5 /usr/bin/create_compressed_fs -f "${COMPRESSED_TEMPFILE}" -B 65536 - "$CLOOPTARGET"
   RESULT="$?"
   if (( "$RESULT" )) ; then
      echo "ERROR: mkisofs exit code = $RESULT!  Exiting abnormally."
      exit $RESULT
   fi

   # verify the integrity of our target compressed image
   TARGSIZE=`ls -s "$CLOOPTARGET" | awk '{ print $1 }'`
   LS_RESULT="$?"
   echo "$CLOOPTARGET: TARGSIZE=$TARGSIZE, LS_RESULT=$LS_RESULT."
   echo "completed cloop compression stage 1 at `date`"

   echo "Creating ${CLOOPTARGET}2 from ${SOURCE2}...."
   # echo "WARNING: compression currently disabled with \"-L 0\" arg to create_compressed_fs!"
   echo "starting at `date`"

   #TODO: use the KNOPPIX2_INCLUDES.txt list for the master list of files to make in the image from a common directory
    # use -path-list "${ROOT}/KNOPPIX2_INCLUDES.txt"
#   mkisofs -R -U -V "KNOPPIX filesystem - bry" -publisher "Bryant Hansen www.bryanthansen.net" \
#		-hide-rr-moved -cache-inodes -no-bak -pad "${SOURCE2}"   \
#		| nice -5 /usr/bin/create_compressed_fs -f "$COMPRESSED_TEMPFILE" -B 65536 - "${CLOOPTARGET}2"
   mkisofs -R -U -V "KNOPPIX filesystem - bry" -publisher "Bryant Hansen www.bryanthansen.net" \
		-hide-rr-moved -cache-inodes -no-bak -pad "${SOURCE2}"   \
		| nice -5 /usr/bin/create_compressed_fs -f "$COMPRESSED_TEMPFILE" -B 65536 - "${CLOOPTARGET}2"
   RESULT="$?"
   if (( "$RESULT" )) ; then
      echo "ERROR: mkisofs exit code = $RESULT!  Exiting abnormally."
      exit $RESULT
   fi

   # verify the integrity of our target compressed image
   TARGSIZE=`ls -s "${CLOOPTARGET}2" | awk '{ print $1 }'`
   LS_RESULT="$?"
   echo "${CLOOPTARGET}2: TARGSIZE=$TARGSIZE, LS_RESULT=$LS_RESULT."
   echo "completed cloop compression stage 2 at `date`"

else

   echo "creating single-stage compressed ISO."

   mkisofs -R -U -V "KNOPPIX filesystem - bry" -publisher "Bryant Hansen www.bryanthansen.net" \
		-hide-rr-moved -cache-inodes -no-bak -pad "${SOURCE}"   \
		| nice -5 /usr/bin/create_compressed_fs -s 5g -f "$COMPRESSED_TEMPFILE" -B 65536 - "$CLOOPTARGET"

   # verify the integrity of our target compressed image
   TARGSIZE=`ls -s "$CLOOPTARGET" | awk '{ print $1 }'`
   LS_RESULT="$?"
   echo "$CLOOPTARGET: TARGSIZE=$TARGSIZE, LS_RESULT=$LS_RESULT."
   echo "completed first stage at `date`"

fi
}

make_final_image()
{
	cd $MASTER
	rm -f KNOPPIX/md5sums
	find -type f -not -name md5sums -not -name boot.cat -exec md5sum {} \; >> KNOPPIX/md5sums
	
	# mkisofs -pad -l -r -J -v -V "KNOPPIX" -b KNOPPIX/boot.img -c KNOPPIX/boot.cat -hide-rr-moved -o $TARGET/knoppix-autobuild-script.iso $MASTER
	
	mkisofs -pad -l -r -J -v -V "KNOPPIX" -no-emul-boot -boot-load-size 4    \
		-boot-info-table -b boot/isolinux/isolinux.bin -c boot/isolinux/boot.cat    \
		-hide-rr-moved -o "$FINAL_OUTPUT_ISO" $MASTER
	echo "completed final stage at `date`"
	echo "creating final md5 sum..."
	md5sum "$FINAL_OUTPUT_ISO" > "${FINAL_OUTPUT_ISO}.md5"
	echo "done."
}

# main

verify_configuration
make_compressed_images
make_final_image
