#!/bin/bash
# Bryant Hansen

# initial 1-liner:
# exec 3>&1 ; equery files xscreensaver | grep /usr/lib64/misc/xscreensaver/ | sort -R | head | while read f ; do 1>&3 ; $f ; echo -n "# keep? (y/n)" ; read -n 1 keep <&3 ; echo "" ; [[ "$keep" == "n" ]] && echo -e "rm $f" ; done

#kde-base/kdeartwork-kscreensaver
#x11-misc/xscreensaver
#x11-misc/rss-glx

# TODO: add command-line iterations

ME="$(basename "$0")"

PACKAGES="kdeartwork-kscreensaver xscreensaver"
#ITERATIONS=20

mkdir -p /data/screensavers

num_screen_savers=0
num_removed=0
num_already_removed=0

exec 3>&1
(
    for pkg in $PACKAGES ; do
        equery files $pkg
    done
) \
| grep "/usr/lib64/misc/xscreensaver/\|/usr/bin/" \
| grep -v "ljlatest\|xscreensaver-demo\|/vidwhacker\|/usr/bin/xscreensaver" \
| sort -R \
| (
    if [[ "$ITERATIONS" ]] && [[ "$ITERATIONS" -gt 0 ]] ; then
        head -n $ITERATIONS
    else
        cat -
    fi
) \
| (
while read f ; do
    [[ ! "$f" ]] && continue
    if [[ ! -f "$f" ]] ; then
        echo "${ME}: $f is already removed" >&2
        (( num_already_removed++ ))
        continue
    fi
    echo "${ME}: Running '$f'..." >&2
    if ! $f 2>/dev/null ; then
        echo "${ME}: Failed to run '$f'.  Skipping..." >&2
        continue
    fi
    (( num_screen_savers++ ))
    echo -n "# keep? (y/n)" >&2
    read -n 1 keep <&3
    echo ""
    if [[ "$keep" == "n" ]] ; then
        echo "# removed by $(whoami) on $(date)"
        echo "[[ -f '$f' ]] && rm -f '$f'"
        echo ""
        (( num_removed++ ))
    fi
done
echo "${ME}: complete.
    total number of screen savers inspected: $num_screen_savers
    number of screen savers removed: $num_removed
    total number of screen savers removed: $num_removed
" >&2
echo "${ME}: complete." >&2

) \
| tee -a /data/screensavers/removed.txt


