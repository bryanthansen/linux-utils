#!/bin/bash
# Bryant Hansen

# rename files that have a Youtube ID as a name to a the title of the HTML page containing the video

# initial 1-liner:
# [[ "${#f}" -gt 22 ]] && continue ; name="$(/projects/utils/getYoutubeVideoNameFromId.sh "$f" 2>/dev/null)" ; ending="${f##*.}" ; newname="$name.$ending" ; newname="${newname//\'/}" ; [[ -f "$newname" ]] && echo "WARNING: $newname already exists" && continue ; echo "mv '$f' '$newname'"

f="$1"
if [[ "${#f}" -gt 22 ]] ; then
    echo "Name too long (greater than 22 characters): $f" >&2
    exit 1
fi
name="$(/projects/utils/getYoutubeVideoNameFromId.sh "$f" 2>/dev/null)"
ending="${f##*.}"
newname="$name.$ending"
newname="${newname//\'/}"
if [[ -f "$newname" ]] ; then
    echo "WARNING: $newname already exists"
    exit 2
fi
echo "mv '$f' '$newname'"
