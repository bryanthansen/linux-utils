#!/bin/bash

TIME=$1
shift

#echo "$@" | /bin/bash &
echo "$@" &
PID=$!
echo -e "\n${0}: launched \"$@\" in background...\n" >&2
echo -e "\n${0}: 1=${1}, 2=${2}, 3=${3}, 4=${4}, 5=${5}, 6=${6}\n" >&2
PROGRAM="$@"
#echo -e "\n${0}: launching \"${PROGRAM}\"...\n" >&2
#$PROGRAM &
sleep $TIME
echo -e "\n${0}: terminating \"${PROGRAM}\"..." >&2
kill $PID
wait
