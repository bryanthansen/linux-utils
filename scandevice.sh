#!/bin/bash

basedev=/dev/sdb
if [[ ! "$1" ]] ; then
    echo "base device not specified; using $basedev default" >&2
fi
[[ "$1" ]] && basedev="$1"

# if it doesn't start with /dev/, then add it
[[ "$basedev" ]] && [[ "${basedev%/dev/*}" ]] && basedev=/dev/$basedev

# show parted info
parted $basedev print

# show partition information, including size, used/free, and fs list of root
blkid \
| grep $basedev \
| sed "s/\:.*//;s/.*\///" \
| while read d ; do
    echo "# Scanning device $d" >&2
    mount /dev/$d /mnt/$d
    df -h /mnt/$d
    ls -Alh /mnt/$d
    umount /dev/$d
    echo ""
done
