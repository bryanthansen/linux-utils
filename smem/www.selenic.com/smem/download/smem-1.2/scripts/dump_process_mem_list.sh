#!/bin/bash
# Bryant Hansen

# top-level script
# passes input options to smem wrapper script

PROGDIR="scripts"

"$PROGDIR"/smem_with_opts.sh $* | \
    "$PROGDIR"/trim_fields.sh | \
    "$PROGDIR"/filter_totals.sh | \
    "$PROGDIR"/reverse_fields.sh | \
    sort -n