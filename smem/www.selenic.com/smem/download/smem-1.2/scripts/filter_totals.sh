#!/bin/bash
# Bryant Hansen

# This will either print out a third column including totals
# or it will print 2 columns, with a unique process name and
# the total for all instances
ONLY_TOTALS=1

lc=""
lp=""
lt="0"
t=0
grep -v "^--" | ( \
while read line ; do
    c="${line%%,*}"
    p="${line##*,}"

    if [[ "$c" == "$lc" ]] ; then
        t=$(echo $t + ${p%\%} | bc)
        [[ ${t:0:1} == "." ]] && t=0$t
    else
        t="${p%\%}"
        lt2="$lt"
        # if p is a percent, we should display it
        [[ ! "${p//*\%*/}" ]] && lt2="${lt2}%"
        [[ "$lc" ]] && [[ "$ONLY_TOTALS" ]] && echo "${lc},${lt2}"
    fi
    [[ ! "$ONLY_TOTALS" ]] && echo "${c},${p},${t}"

    lc="$c"
    lp="$p"
    lt="$t"
done
lt2="$lt"
# if p is a percent, we should display it
[[ ! "${p//*\%*/}" ]] && lt2="${lt2}%"
[[ "$ONLY_TOTALS" ]] && echo "${lc},${lt2}"
)


