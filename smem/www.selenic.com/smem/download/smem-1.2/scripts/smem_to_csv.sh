#!/bin/bash
# Bryant Hansen

# parses the specific 2-field output from the command:
#    ./smem -t --no-header -s command -c "command pss"

while read line ; do
    l2="$(echo $line)"
    l3="${l2% *}"
    echo "${l3},${line##* }"
done
