#!/bin/bash
# Bryant Hansen

SCREENSHOT_DIR="/data/media/screenshots/$(hostname)"
[[ ! -d "$SCREENSHOT_DIR" ]] && mkdir -p "$SCREENSHOT_DIR"

SHUTTER_SOUND="/data/media/sounds/camera-shutter.oga"

#scrot "$(hostname)_%Y-%m-%d_%H-%M-%S_\$wx\$h.png" -e "mv \$f /data/media/screenshots/" && ( \
#scrot "$(hostname)_%Y-%m-%d_%H-%M-%S_\$wx\$h.png" -e "mv \$f $SCREENSHOT_DIR" && ( \
scrot "%Y-%m-%d_%H-%M-%S_\$wx\$h.png" -e "mv \$f $SCREENSHOT_DIR" && ( \
    echo "SCREENSHOT_DIR=$SCREENSHOT_DIR"
    NEW_FILE="$(ls -1tr "${SCREENSHOT_DIR}"/* 2>/dev/null | tail -n 1)"
    echo "NEW_FILE=$NEW_FILE"
    echo "snapshot saved to $(ls -1tr "${SCREENSHOT_DIR}"/* 2>/dev/null | tail -n 1)"
    echo "  use the ViewLastSnapshot command to display"
    ogg123 "$SHUTTER_SOUND" 2> /dev/null
    exit 0
)

ERROR_CODE=$?

[[ "$ERROR_CODE" != "0" ]] && \
    echo "$0 ERROR: failed to take snapshot via the 'scrot' program, error code $ERROR_CODE" >&2

exit $ERROR_CODE
