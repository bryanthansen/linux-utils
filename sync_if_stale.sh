#!/bin/bash
# Bryant Hansen

ME="$(basename "$0")"

sync_pidfile="/run/${ME}.pid"

ts_file=/usr/portage/metadata/timestamp.chk
ts="$(cat "$ts_file")"
ts_s=$(date --date="$ts" +%s)
now=$(date +%s)
elapsed_s=$(expr $now - $ts_s)

S_in_MI=60
S_in_H=$((S_in_MI * 60))
S_in_D=$((S_in_H * 24))
S_in_W=$((S_in_D * 7))

MIN_SYNC_INTERVAL=$((S_in_D * 1))

printf "%20s: %s\n" "timestamp file" "$ts_file" >&2
printf "%20s: %s\n" "timestamp" "$ts" >&2
printf "%20s: %s\n" "now" "$(date)" >&2
printf "%20s: %s\n" "elapsed(s)" "$elapsed_s s" >&2

printf "%20s: " "elapsed" >&2
es=$elapsed_s
for n in $S_in_W $S_in_D $S_in_H $S_in_MI ; do
    (( es < $n )) && continue
    nf=$(((es / $n) % $n))
    es=$((es - nf * n))
    printf "%02d:" "$nf"
done
printf "%02d\n" "$((es % 60))"

if (( elapsed_s > S_in_D )) ; then
    printf "\nmore than 1 day has elapsed since in the timestamp in ${ts_file}: ${ts}\n" >&2
    if [[ ! -f "$sync_pidfile" ]] ; then
        printf "# emerge --sync\n" >&2
        emerge --sync &
        pid="$!"
        # test if we have launched and we can detect
        if [[ ! "$pid" ]] ; then
            printf "ERROR: no PID received from emerge --sync running in background\n" >&2
            error_code=2
        fi
        printf "${pid}\n" >> "$sync_pidfile"
        # TODO: detect if launched
    else
        pid=$(cat "$sync_pidfile")
        printf "sync_pidfile '${sync_pidfile}' exists.  ${ME} seems to be running.  PID: ${pid}\n" >&2
        if [[ "$pid" ]] && (( "$pid" > 0 )) ; then
            printf "looking up process ID ${pid}...\n" >&2
            cmdline_file=/proc/${pid}/cmdline
            if [[ -f $cmdline_file ]] ; then
                eval "cmd=($(cat $cmdline_file))"
                if [[ "$cmd" ]] ; then
                    printf "# process running with process ID ${pid}: ${cmd}\n"
                    if [[ "$cmd" == "$ME" ]] ; then
                        printf "# The process name matches the current running script '${ME}'\n" >&2
                    else
                        printf "# The current process ID is '${cmd}'; we are expecting to find our process named '${ME}'\n" >&2
                        printf "# if it looks like '${cmd}' should not match '${ME}' then execute the following command and re-run the script: \n  rm '${sync_pidfile}'\n" >&2
                    fi
                else
                    printf "WARNING: could not retrieve the name of the running process from the cmdline_file '${cmdline_file}'\n" >&2
                    printf "# recommended: \n  rm '${sync_pidfile}'\n" >&2
                fi
            else
                printf "WARNING: cmdline_file '${cmdline_file}' does not exist.  The process appears to not be running.\n" >&2
                printf "# recommended: \n  rm '${sync_pidfile}'\n" >&2
            fi
        else
            printf "ERROR: expected a numerical process ID; got: '${pid}'.  First strings in file:\n" >&2
            strings "$sync_pidfile" | head | sed "s/^/\ \ \ \ /" >&2
            printf "# recommended: \n  rm '${sync_pidfile}'\n" >&2
        fi
    fi
else
    printf "The repository has already been sync'd within the last day\n" >&2
fi
