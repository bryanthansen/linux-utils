#!/bin/sh
# Bryant Hansen

# backup files in a TNO manner

#tar --create --verbose --to-stdout "$dir" | gzip --best --stdout | gpg --encrypt --recipient gpg2013a@bryanthansen.net | ssh domina "cat - > /home/leopold/temp/busybox.tgz.gpg"
#tar -cvO dir | gzip -9c | gpg -e -r a@b.net | ssh myserver "cat - > /var/archive.tgz.gpg"
#tar -cvO $dir | gzip -9c | gpg -e -r $to | ssh $server "cat - > $archive"
#tar -cvzO $dir | gpg -e -r $to | ssh $server "cat - > $archive"

dir="busybox"
server="backup"
archive="/home/leopold/temp/busybox.tgz.gpg"
to="gpg2013a@bryanthansen.net"

tar --create --verbose --to-stdout "$dir" | \
    gzip --best --stdout | \
    gpg --encrypt --recipient "$to" | \
    ssh domina "cat - > $archive"
