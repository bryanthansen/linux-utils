#!/bin/bash
# Bryant Hansen

# Loop through a list of hosts, toggling the screens on and off via blanking

OFF=4
ON=0

function set_screen() {
    local host="$1"
    local onoff="$2"
    local iter="$3"
    ssh $host "
              start_s=\$(date +%s) ;
              start_ns=\$(date +%N) ;
              for f in /sys/class/graphics/fb[0-9]*/blank ; do
                  echo $onoff > /sys/class/graphics/fb0/blank ;
              done ;
              let elapsed_s=\$(date +%s)-start_s ;
              let elapsed_ns=\$(date +%N)-start_ns ;
              if [[ "\$elapsed_ns" -lt 0 ]] ; then let elapsed_ns=elapsed_ns+1000000000 ; let elapsed_s=elapsed_s-1 ; fi ;
              echo ${h}:iter${iter}:value=${onoff}:elapsed_ns=\$elapsed_s.\$elapsed_ns
            " & \
    echo "# $(date +%H:%M:%S.%N):${h}:Sent value $onoff screen blanking to screen blanking register"
}

function set_all_screens() {
    local onoff="$1"
    local iter=$2
    local screens="$(cat ~/.screens.lst | sed "s/\#.*//;/^[ \t]*$/d")"
    [[ "$screens" ]] || screens="host1\nhost2"

    echo -e "$screens" \
    | while read h ; do
        set_screen $h $onoff $iter
    done
}

ITER=10
for n in $(seq 1 $ITER) ; do
    [[ "$onoff" == $OFF ]] && onoff=$ON || onoff=$OFF
    set_all_screens $onoff $n
    sleep 0.25
done

# insure that we leave screens in an on-state at the end
set_all_screens $ON $(expr $ITER + 1)
