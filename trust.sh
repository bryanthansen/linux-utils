#!/bin/bash
# Bryant Hansen
# 20110220

CHOWN="chown"
CHMOD="chmod"

if [[ "$1" = "-R" ]] ; then
    RECURSIVE=1
    shift
fi

while [[ "$1" ]] ; do
    if [[ -d "$1" ]] || [[ -f "$1" ]] ; then
        if [[ $RECURSIVE ]] ; then
	    $CHOWN -R root:trusted "$1"
    	    $CHMOD -R g+rw "$1" 
        else
            $CHOWN root:trusted "$1"
            $CHMOD g+rw "$1"
        fi
    else
        echo "\"$1\" not found."
	exit -1
    fi
    shift
done
