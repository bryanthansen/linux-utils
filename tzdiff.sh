#!/bin/bash
# Bryant Hansen

TZOFFSET=/projects/utils/tzoffset.sh

offset1="$($TZOFFSET $1)"
offset2="$($TZOFFSET $2)"

expr "$offset1" - "$offset2"
