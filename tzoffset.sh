#!/bin/bash
# from:
#   http://stackoverflow.com/questions/2823845/offset-of-a-given-timezone-from-gmt-in-linux-shell-script

ZONE=$1
TIME=$(date +%s --utc -d "12:00:00 $ZONE")
UTC_TIME=$(date +%s --utc -d "12:00:00")
((DIFF=UTC_TIME-TIME))
echo - | awk -v SECS=$DIFF '{printf "%d",SECS/(60*60)}'


