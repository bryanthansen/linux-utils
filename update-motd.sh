#!/bin/bash
# Bryant Hansen
# created: 20100305
# updated: 20170831

DTS1=$(date +%s)
DTS1F=$(date +%s.%N)
printf "==============================================\n"
printf "Executing $0 on $(date "+%Y%m%d_%H%M%S.%N %Z")\n"
printf "==============================================\n"

if ! which fortune > /dev/null 2>&1 ; then
    echo "$0 ERROR: fortune not found in path" >&2
fi
if ! which cowsay > /dev/null 2>&1 ; then
    echo "$0 WARNING: cowsay not found in path" >&2
fi

# all directories containing cowdir files
# priority: first existing directory wins
COW_DIRS="
    /etc/cows
    /usr/share/cowsay/cows-custom
    /usr/share/cowsay/cows
"
for cowsDir in $COW_DIRS ; do
    [[ -d "$cowsDir" ]] && break
done
if [[ "$cowsDir" ]] && [[ -d "$cowsDir" ]] ; then
    cowFile=""
    f="${cowsDir}/calvin.cow"
    [[ -f "$f" ]] && cowFile="$f"
    numCows="$(ls -1 "$cowsDir" | wc -l)"
    if (( "$numCows" > 0 )) ; then
        number=$RANDOM
        let "number %= $numCows"
        newCowFile="$(ls -1 "$cowsDir" | sed -n "${number}p")"
        printf "Selecting random cow ${number} of ${numCows}: ${cowsDir}/${newCowFile}\n" >&2
        if [[ -f "${cowsDir}/${newCowFile}" ]] ; then
                cowFile="$newCowFile"
        fi
    fi
    fortune | cowsay -f "${cowsDir}/${cowFile}"
else
    fortune
fi

printf "\n"
#uname -a
#printf "uname -snrvm: "
#uname -snrvm
uname -a
printf "\tuptime: $(uptime)\n"

printf "Latest Kernel Log Messages:"
dmesg | tail -n 10 | sed "s/^/\t/"

printf "Network Listeners:\n"
netstat -nap | grep LIST | sed "s/^/\t/;/^[ \t]*unix/d"

days=3
printf "Log files modified in the last $days days\n"
ls -alth --full-time $(find /var/log/* -mtime -${days} -and -type f -and -not -name ".keep*" -and -size +0) \
| sed "s/^/\t/"

TAIL_LOGFILES="
    /var/log/messages
    /var/log/everything/current
"
numlines=10
for f in $TAIL_LOGFILES ; do
    if [[ -f "$f" ]] ; then
        printf "latest entries in ${f}:\n"
        tail -n $numlines "$f" \
        | sed "s/^/\t/"
    fi
done

printf "Latest Emerge Results:\n"
grep -i -a "Merging\|success" /var/log/emerge.log \
| tail -n 10 \
| while read 'line' ; do
    #echo "line: '${line}'" >&2
    echo "`date -d "1970-1-1 ${line/:*/} sec UTC" "+%Y-%m-%d %H:%M:%S"`:${line#*:}"
done \
| sed "s/^/\t/"

printf "Last Sync: "
date +%Y%m%d_%H%M%S%Z --date="$(cat /usr/portage/metadata/timestamp.chk)"

DTS2=$(date +%s)
DTS2F=$(date +%s.%N)
let elapsed=$DTS2-$DTS1
if which bc > /dev/null ; then
    elapsedF=$(printf "scale=2 ; $DTS2F - $DTS1F\n" | bc)
    [[ "$elapsedF" ]] && elapsed=$elapsedF
    [[ "${elapsed:0:1}" == "." ]] && elapsed=0$elapsed
fi
printf "DTS1F=$DTS1F  DTS2F=$DTS2F  elapsed=$elapsed  elapsedF=$elapsedF \n" >&2
printf "=========== Report $0 completed on $(date "+%Y%m%d_%H%M%S %Z") after $elapsed seconds ==========\n"
