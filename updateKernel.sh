#!/bin/bash
# Bryant Hansen

# TODO: build kernel
# Install new kernel
# Update grub.conf with new kernel
# TODO: rebuild dependent packages


kernelTree=$(readlink -f /usr/src/linux)
kernelName=$(basename $kernelTree)
date=$(date "+%Y%m%d_%H%M%S")

kernelSrc=${kernelTree}/arch/x86/boot/bzImage
configSrc=${kernelTree}/.config

kernelDest=/boot/bzImage-${kernelName}-${date}
configDest=/boot/kernel.config.${kernelName#linux-}-${date}

oldGrubConf=$(readlink -f /boot/grub/menu.lst)
newGrubConf=./grub.conf.new.${date}

#kernelDependentPackageList="x11-drivers/nvidia-drivers app-emulation/virtualbox-modules"
kernelDependentPackageList="
    app-emulation/virtualbox-modules
    "
#    x11-drivers/nvidia-drivers

#####################################################################
# sanitize environment
#####################################################################
if [[ -f "$kernelDest" ]] ; then
    echo "ERROR: kernelDest $kernelDest already exists." >&2
    echo "exiting abnormally" >&2
    exit 2
fi
if [[ -f "$configDest" ]] ; then
    echo "ERROR: configDest $configDest already exists." >&2
    echo "exiting abnormally" >&2
    exit 3
fi
if [[ ! -f "$configSrc" ]] ; then
    echo "ERROR: kernel config $configSrc does not exist." >&2
    echo "  need to do a make menuconfig or make oldconfig" >&2
    echo "exiting abnormally" >&2
    exit 4
fi


#####################################################################
# Functions
#####################################################################

buildKernel() {
    echo -e "\n# building kernel $kernelName ..." >&2
    cd $kernelTree
    make && make modules_install
}

installKernel() {
    echo -e "\n# installing kernel $kernelName ..." >&2
    cp -a $kernelSrc $kernelDest
    cp -a $configSrc $configDest
}

# get the currently active default kernel from grub.conf
getDefault() {
    cat $oldGrubConf | \
    sed "s/\#.*$//g" | \
    (
        default=""
        while read a b ; do
            if [[ $a == default ]] ; then
                default="$b"
            fi
        done
        echo "$default"
    )
}

# test function to identify title blocks, assign a sequence number, and pre-annotate
annotateTitles() {
    lineNum=0
    titleNum=0
    cat $oldGrubConf | \
    while read l ; do
        lineNum="$[$lineNum+1]"
        l2="${l//#/*}"
        l3="${l2// /}"
        if [[ "$l3" ]] && [[ ! "${l3%title=*}" ]] ; then
            # title=... line found
            echo "# lineNum=$lineNum  titleNum=$titleNum  line=\"${l}\""
            titleNum="$[$titleNum+1]"
        fi
        echo "${lineNum}: $l"
    done
}

# get the block representing the current default kernel and put it out stdout
getTitleBlock() {
    matchTitleNum=$1
    lineNum=0
    titleNum=0
    titleBlock=""
    inTitleBlock=""
    cat $oldGrubConf | \
    while read l ; do
        lineNum="$[$lineNum+1]"
        l2="${l//#/*}"
        l3="${l2// /}"
        if [[ "$l3" ]] && [[ ! "${l3%title=*}" ]] ; then
            [[ "$titleNum" == "$matchTitleNum" ]] && inTitleBlock=1 || inTitleBlock=""
            #echo "# lineNum=$lineNum  titleNum=$titleNum  line=\"${l}\"" >&2
            titleNum="$[$titleNum+1]"
        fi
        if [[ "$inTitleBlock" ]] ; then
            echo "${l}"
        fi
    done
}

# read a title block from stdin and replace the kernel with the supplied parameters
updateTitleBlock() {
    local newKernel=$1
    while read l ; do
        if [[ ! "${l/*bzImage*/}" ]] ; then
            echo -n "${l% *bzImage*} "
            echo -n "$newKernel"
            echo " ${l#*bzImage* }"
        else
            echo "$l"
        fi
    done
}

# dump all of grub.conf up to (but not including) the current kernel directive
dumpPreTitleBlockConfFile() {
    matchTitleNum=$1
    lineNum=0
    titleNum=0
    inTitleBlock=""
    cat $oldGrubConf | \
    while read l ; do
        lineNum="$[$lineNum+1]"
        l2="${l//#/*}"
        l3="${l2// /}"
        if [[ "$l3" ]] && [[ ! "${l3%title=*}" ]] ; then
            [[ "$titleNum" == "$matchTitleNum" ]] && return 0
            titleNum="$[$titleNum+1]"
        fi
        echo "$l"
    done
}

# dump all of grub.conf after (but not including) the current kernel directive
dumpPostTitleBlockConfFile() {
    matchTitleNum=$1
    lineNum=0
    titleNum=0
    inTitleBlock=""
    afterTitleBlock=""
    cat $oldGrubConf | \
    while read l ; do
        lineNum="$[$lineNum+1]"
        l2="${l//#/*}"
        l3="${l2// /}"
        if [[ "$l3" ]] && [[ ! "${l3%title=*}" ]] ; then
            if [[ "$inTitleBlock" ]] ; then
                afterTitleBlock=1
            fi
            [[ "$titleNum" == "$matchTitleNum" ]] && inTitleBlock=1
            titleNum="$[$titleNum+1]"
        fi
        [[ "$afterTitleBlock" ]] && echo "$l"
    done
}

# produce a new grub.conf with the specified kernel as the default boot option
updateGrubConf() {

    local newKernel=$1

    default=$(getDefault)
    echo "# found default: $default" >&2
    if [[ ! "$default" ]] ; then
        echo "ERROR: default not found." >&2
        echo "Exiting abnormally" >&2
        exit 4
    fi

    titleBlock="$(getTitleBlock $default)"

    echo "# ========   Pre Title Block    ========" >&2
    dumpPreTitleBlockConfFile $default
    echo "# ======================================" >&2

    echo "# ========   New Title Block    ========" >&2
    echo "$titleBlock" | updateTitleBlock "$newKernel"
    echo ""
    echo "# ======================================" >&2

    echo "# ======== Original Title Block ========" >&2
    echo "$titleBlock"
    echo ""
    echo "# ======================================" >&2

    echo "# ========   Post Title Block   ========" >&2
    dumpPostTitleBlockConfFile $default
    echo "# ======================================" >&2

}

# install the newly-updated grub.conf
installGrubConf() {
    mv $oldGrubConf ${oldGrubConf}.$date
    mv $newGrubConf $oldGrubConf
}

# rebuild all packages depending on the new kernel
updatePackages() {
    emerge -v ${kernelDependentPackageList}
}

#####################################################################


#####################################################################
# main
#####################################################################

buildKernel || (
    echo "ERROR: kernel build failed." >&2
    echo "Exiting abnormally." >&2
    exit 5
)

installKernel || (
    echo "ERROR: install kernel failed." >&2
    echo "Exiting abnormally." >&2
    exit 6
)

updateGrubConf $kernelDest 2>/dev/null > $newGrubConf || (
    echo "ERROR: update grub failed." >&2
    echo "Exiting abnormally." >&2
    exit 7
)

installGrubConf || echo "ERROR: install grub failed." >&2

# TODO: only update packages when there's been a change in kernel version; compare to uname or the build dir (with readlink -f .) or what is already in /boot
updatePackages || (
    echo "ERROR: update packages failed failed." >&2
    echo "  The following packages should be rebuilt for the new kernel: " >&2
    echo "$kernelDependentPackageList" >&2
)
