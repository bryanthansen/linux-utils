#!/bin/bash
# Bryant Hansen

# minimal version:
# kn=arch/x86/boot/bzImage ; kv=$(basename $(readlink -f $PWD)) ; dt=$(date +%Y%m%d_%H%M%S) ; echo cp -a $kn /boot/kernel${kv#linux}-${dt}

ME="$(basename "$0")"
USAGE="$ME  [ bootdir ]"

boot_dir=/boot
[[ "$1" ]] && boot_dir="$1"
dt=$(date "+%Y%m%d_%H%M%S")
ver="$(basename "$(readlink -f .)")"
ver="${ver#linux-}"
kernel=${boot_dir}/kernel-${ver}-${dt}
grub_menu=/mnt/sda1
#grub_menu="/mnt/EFI System Partition"

CONF="${0%.sh}.conf"
if [[ -f "/etc/$CONF"  ]] ; then
    echo "# reading settings from /etc/$CONF" >&2
    . "/etc/$CONF"
fi
if [[ -f "~/$CONF"  ]] ; then
    echo "# reading settings from ~/$CONF" >&2
    . "~/$CONF"
fi

[[ ! "$EDITOR" ]] && EDITOR=nano

if [[ "$PWD" != /usr/src/linux ]] ; then
    echo "# WARNING: this script is intended to be executed out of the /usr/src/linux directory!" >&2
    echo ""
fi

unset editor
if which $EDITOR > /dev/null ; then
    editor="$EDITOR"
else
    echo "# WARNING: EDITOR $EDITOR not found in path" >&2
fi

ARCH=x86
if [[ -d "$boot_dir" ]] ; then
    echo "# boot dir found at $boot_dir" >&2
    echo "cp -a arch/${ARCH}/boot/bzImage $kernel || exit 2"
    echo "cp -a .config ${boot_dir}/kernel.config.${ver}-${dt} || exit 3"
    echo "cp -a System.map ${boot_dir}/System.map.${ver}-${dt} || exit 4"
    echo "" >&2
else
    echo "$0 ERROR: boot dir $boot_dir not found" >&2
    exit 2
fi

if [[ -f ${boot_dir}/grub/menu.lst ]] ; then
    # for grub 0.97:
    echo "# menu.lst found at ${boot_dir}/grub/menu.lst" >&2
    echo "echo \"# ${kernel}\" >> ${boot_dir}/grub/menu.lst"
    echo "########################################" >&2
    echo "# for grub 0.97 operation, update ${boot_dir}/grub/menu.lst" >&2
    echo "# the filename of the latest kernel $kernel can be found as a comment at the bottom of the file" >&2
    if [[ "$editor" ]] ; then
        echo "read -n 1 && $editor ${boot_dir}/grub/menu.lst"
    fi
    echo "########################################" >&2
    echo "" >&2
fi


MK_CONFIG_SEARCH_LIST="
    grub-mkconfig
    grub2-mkconfig
"
unset mkcfg
for m in $MK_CONFIG_SEARCH_LIST ; do
    which "$m" > /dev/null && mkcfg=$m && break
done
if [[ "$mkcfg" ]] ; then
    echo "# grub2 mkconfig found at mkconfig" >&2
	# echo "# then grub2-mkconfig -o '${boot_dir}/grub/grub.cfg'"
	# echo "cp -a '${grub_menu}/${boot_dir}/grub/grub.cfg' '${boot_dir}/grub/grub.cfg.${dt}'"
	# echo "cp -a '${grub_menu}/${boot_dir}/grub/grub.cfg' '${grub_menu}/${boot_dir}/grub/grub.cfg.${dt}'"
	echo "cp -a '${boot_dir}/grub/grub.cfg' '${boot_dir}/grub/grub.cfg.${dt}'"
	echo "$mkcfg -o '${boot_dir}/grub/grub.cfg'"
    echo "" >&2
fi

echo "# to execute this sequence, pipe the results to /bin/bash"
echo "# $0 | /bin/bash"
