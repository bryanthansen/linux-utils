#!/bin/bash
# Bryant Hansen

# basically do this
#38914  cp -a arch/x86/boot/bzImage /boot/kernel-4.12.5-gentoo-$(dt)
#38915  cp -a .config /boot/kernel.config.4.12.5-gentoo-20170820_181222 

# detect source and dests
# regen new grub.cfg
#  - consider supporting grub0.97 (menu.lst)
#  - consider supporting other bootloaders

# requirements:
#  - grub2
#  - grub2-mkinstall
#  - get kernel version from current dir
#  - boot dir is located at (or mounted on /boot)
#  - an existing grub.cfg

# TODO:
#  - find boot dir
#  - verify that current boot file matches running kernel

GRUB_VERSION=2
BOOT_DIR=/boot
KERNEL=4.12.5-gentoo
DT=$(dt)

# set/detect parameters
CURDIR="$(readlink -f "$PWD")"
DIRNAME="$(basename "$CURDIR")"
KERNEL_DEF="${DIRNAME#linux-}"
unset GRUB_INSTALL
which grub-install  > /dev/null 2>&1 && GRUB_INSTALL=grub-install
which grub2-install > /dev/null 2>&1 && GRUB_INSTALL=grub2-install

# debug info
echo "INFO: DIRNAME=$DIRNAME" >&2
echo "INFO: KERNEL_DEF=$KERNEL_DEF" >&2

# do checks
if [[ ! "$DT" ]] ; then
    echo "ERROR: failed to generate date string" >&2
    exit 2
fi
if [[ ! "$DIRNAME" ]] ; then
    echo "ERROR: directory must be source of kernel tree" >&2
    exit 3
fi
if [[ "${DIRNAME%%linux-*}" ]] ; then
    echo "ERROR: directory must be source of kernel tree; prefix does not match 'linux-'" >&2
    exit 4
fi
if [[ ! "$GRUB_INSTALL" ]] ; then
    echo "ERROR: GRUB_INSTALL not found" >&2
    exit 5
fi
if [[ ! -f arch/x86/boot/bzImage ]] ; then
    echo "ERROR: arch/x86/boot/bzImage not found" >&2
    exit 6
fi
if [[ ! -f ./.config ]] ; then
    echo "ERROR: ./.config not found" >&2
    exit 7
fi
if [[ ! -d "$BOOT_DIR" ]] ; then
    echo "ERROR: BOOT_DIR $BOOT_DIR does not exist as a directory." >&2
    exit 8
fi
if [[ ! -f "${BOOT_DIR}/grub/grub.cfg" ]] ; then
    echo "ERROR: grub.cfg ${BOOT_DIR}/grub/grub.cfg does not exist as a file." >&2
    exit 9
fi
# verify that the kernel does not exist already
for k in ${BOOT_DIR}/kernel-${KERNEL_DEF}-* ; do
    if diff arch/x86/boot/bzImage $k > /dev/null ; then
        echo "ERROR: this exact kernel 'arch/x86/boot/bzImage' already exists at '${k}'" >&2
        ls -lh arch/x86/boot/bzImage $k | sed "s/^/\ \ \ \ /" >&2
        exit 10
    fi
done

# do copies
cp -a arch/x86/boot/bzImage ${BOOT_DIR}/kernel-${KERNEL_DEF}-${DT}
cp -a .config /boot/kernel.config.${KERNEL_DEF}-${DT}

# backup original grub.cfg
echo "cp -a ${BOOT_DIR}/grub/grub.cfg ${BOOT_DIR}/grub/grub.cfg.replaced.${DT}"

# update config
echo "# To complete the action, run this command or pipe the output to the shell ($0 | sh)" >&2
echo "$GRUB_INSTALL -o ${BOOT_DIR}/grub/grub.cfg"

# if a backup of this grub config does not exist, create it
for c in ${BOOT_DIR}/grub/grub.cfg.* ; do
    if diff ${BOOT_DIR}/grub/grub.cfg $c > /dev/null ; then
        echo "INFO: this exact grub config already exists at '${c}'" >&2
        ls -lh $c | sed "s/^/\ \ \ \ /" >&2
        exit 0
    fi
done
cp -a ${BOOT_DIR}/grub/grub.cfg ${BOOT_DIR}/grub/grub.cfg.replaced.${DT}
