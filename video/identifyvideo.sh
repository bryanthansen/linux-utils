#!/bin/bash
# Bryant Hansen

ME="$(basename "$0")"
USAGE="$ME  video_file"

if [[ ! "$1" ]] ; then
	echo "$ME ERROR: this command requires 1 argument" >&2
	echo "USAGE: $USAGE" >&2
	echo "Exiting abnomally" >&2
	exit 11
fi
if [[ ! -f "$1" ]] ; then
	echo "$ME ERROR: file '${1}' does not exist" >&2
	echo "USAGE: $USAGE" >&2
	echo "Exiting abnomally" >&2
	exit 11
fi

mplayer -vo null -ao null -frames 0 -identify "$1"
