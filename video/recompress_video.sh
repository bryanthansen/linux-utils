#!/bin/bash
# Bryant Hansen

ARGS=$*

#echo "\$*=$*" >&2
#exit 1

IN="$1"
[[ "$2" ]] && OUT="$2"
[[ ! "$OUT" ]] && OUT="${IN}.mpg"
[[ ! "$IN" ]] && echo "at least 1 arg required" >&2 && exit
[[ ! -f "$IN" ]] && echo "$IN missing" >&2 && exit

echo "ffmpeg -i \"$IN\" -target pal-dvd -aspect 16:9 -pass 1 \"$OUT\""
echo "ffmpeg -y -i \"$IN\" -target pal-dvd -aspect 16:9 -pass 2 \"$OUT\""
