

VBoxManage createvm --name webvm2_20140928 --ostype Gentoo --register

VBoxManage modifyvm webvm2_20140928 --memory 512 --macaddress1 080027B5D13F --nictype1 Am79C973
VBoxManage modifyvm webvm2_20140928 --memory 512 --macaddress1 auto --nictype1 Am79C973



VBoxManage storagectl       webvm2_20140928 \
                            --name "SATA Controller" \
                            --add sata \
                            --controller PIIX4 \
                            --sataportcount 1-12 \
                            --hostiocache off \
                            --bootable on

VBoxManage storageattach webvm2_20140928 \
                         --storagectl "SATA Controller" \
                         --device 0 \
                         --port 0 \
                         --type hdd \
                         --medium /data/os/webvm2/webvm2_rootfs_20140915_012804.vdi

VBoxManage storageattach webvm2_20140928 \
                         --storagectl "SATA Controller" \
                         --device 0 \
                         --port 1 \
                         --type hdd \
                         --medium /data/os/webvm2/80GB.vdi

VBoxManage setextradata webvm2_20140928 "VBoxInternal/Devices/pcnet/0/LUN#0/Config/guestssh/Protocol"  TCP                 || exit -5
VBoxManage setextradata webvm2_20140928 "VBoxInternal/Devices/pcnet/0/LUN#0/Config/guestssh/GuestPort" 22   || exit -6
VBoxManage setextradata webvm2_20140928 "VBoxInternal/Devices/pcnet/0/LUN#0/Config/guestssh/HostPort"  18022    || exit -7


VBoxManage storageattach webvm2_20140928 \
                         --storagectl "SATA Controller" \
                         --device 0 \
                         --port 0 \
                         --type hdd \
                         --medium none

VBoxManage storageattach webvm2_20140928 \
                         --storagectl "SATA Controller" \
                         --device 0 \
                         --port 1 \
                         --type hdd \
                         --medium /data/os/webvm2/80GB.vdi

VBoxManage setextradata emailvm2 "VBoxInternal/Devices/pcnet/0/LUN#0/Config/guestssh/Protocol"  TCP                 || exit -5
VBoxManage setextradata emailvm2 "VBoxInternal/Devices/pcnet/0/LUN#0/Config/guestssh/GuestPort" 22   || exit -6
VBoxManage setextradata emailvm2 "VBoxInternal/Devices/pcnet/0/LUN#0/Config/guestssh/HostPort"  20022    || exit -7


VBoxManage modifyvm emailvm2b --natpf1 delete "guesthttp"

VBoxManage modifyvm "VM name" --natpf1 "guestssh,tcp,127.0.0.1,2222,,22"
VBoxManage modifyvm "VM name" --vrde on
VBoxManage modifyvm "VM name" --vrdeport 7000-7012

