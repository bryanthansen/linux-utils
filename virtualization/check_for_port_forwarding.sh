#!/bin/bash
# Bryant Hansen

# list the "extradata" settings of the VirtualBox VM that are specific to port forwarding

ME="$(basename "$0")"
MYDIR="$(dirname "$0")"
USAGE="$ME  [ VM_NAME | -a ]"

if [[ ! "$1" ]] ; then
    echo "USAGE: $USAGE" >&2
    exit 0
fi

VM_NAME="$1"

case "$VM_NAME" in
    "-a")
        num=0
        # get the data from all registered VMs
        VBoxManage list vms | ( \
            while read a b ; do
                b="${a//\"/}"
                #echo "$b"
                $0 "$b"
                echo ""
                num=$(expr $num + 1)
            done
            echo "$num VMs found"
        )
        ;;
    *)
        echo "VirtualBox VM \"${VM_NAME}\" extradata:"
        VBoxManage getextradata "$VM_NAME" enumerate | grep "/Config/"
        ;;
esac
