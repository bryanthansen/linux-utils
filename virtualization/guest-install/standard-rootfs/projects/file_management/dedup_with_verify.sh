#!/bin/sh
# Bryant Hansen

############################################################
ME="$(basename "$0")"
MYDIR="$(dirname "$0")"
USAGE="${ME}  [  DBDIR  ]"

############################################################
# programs (dependencies)

CAT=flexcat2
STAT=stat
VERIFY=1

############################################################
# help
if [[ "$1" == "-h" ]] || [[ "$1" == "--help" ]] ; then
    echo "USAGE: " >&2
    echo "   $USAGE" >&2
    exit 1
fi

############################################################
# Settings

DBDIR=.filedb
if [[ "$1" ]] ; then
    DBDIR="$1"
fi
DBDIR="${DBDIR%%\/}"
if [[ ! -d "$1" ]] ; then
    echo "$ME ERROR: DBDIR $1 does not exist as a directory." >&2
    echo "Exiting abnormally." >&2
    exit 2
fi

############################################################
# Functions

TRACE()
{
    local NO_CR_ARG=""
    local SPECIAL_CHAR_ARG=""
    local ARGS=""
    local OPTION_FOUND="0"
    # local LOG_RECORD_HEADER="`date "+%Y%m%d%H%M%S"`:`basename "$0"`: "
    local LOG_RECORD_HEADER="`date "+%Y%m%d %H%M%S"`: "

    while [[ "$OPTION_FOUND" ]] ; do
        OPTION_FOUND=""
        if [[ "$1" == "-e" ]] ; then
            OPTION_FOUND="-e"
            # ARGS="${ARGS} -e" # use this as default anyway
            shift
        fi
        if [[ "$1" == "-n" ]] ; then
            OPTION_FOUND="-n"
            ARGS="${ARGS} -n"
            LOG_RECORD_HEADER=""
            shift
        fi
    done

    # echo -e ${ARGS} "${*/${ARGS}/}" | while read line ; do ( [[ "$line" ]] && echo -n "${LOG_RECORD_HEADER}" ) ; echo "$line" ; done >&2
    echo -e ${ARGS} "${*/${ARGS}/}" \
    | while read line ; do
        (
            [[ "$line" ]] && echo -n "${LOG_RECORD_HEADER}"
        )
        echo "$line"
    done >&2

    return 0
}

# Description: this function is just a wrapper for 'cat' which will call zcat with the compressed version
# of the same file if the
# uncompressed version doesn't exist
flexcat2() {
    while [[ "$1" ]] ; do
        if [[ -f "$1" ]] ; then
            TRACE "$1 exists.  use cat (no decompression)"
            cat    "$1"
        elif [[ !  "${1#*.gz}" ]] ; then
            TRACE "$f has .gz ending.  use zcat"
            zcat   "$1"
        elif [[ !  "${1#*.bz}" ]] ; then
            TRACE "$f has .bz ending.  use bzcat"
            bzcat  "$1"
        elif [[ !  "${1#*.xz}" ]] ; then
            TRACE "$f has .xz ending.  use xzcat"
            xzcat  "$1"
        elif [[ -f "$1".gz ]] ; then
            TRACE "${f}.gz exists.  use zcat"
            zcat   "$1".gz
        elif [[ -f "$1".bz ]] ; then
            TRACE "${f}.bz exists.  use bzcat"
            bzcat  "$1".bz
        elif [[ -f "$1".xz ]] ; then
            TRACE "${f}.xz exists.  use xzcat"
            xzcat  "$1".xz
        else
            return -1
        fi
        shift
    done
    return 0
}

areLinked() {

    SAME=0
    DIFFERENT=1
    ERROR=2

    i1="$($STAT --format="%i" "$1")" 2>/dev/null
    i2="$($STAT --format="%i" "$2")" 2>/dev/null

    # do 1 retry, sometimes there's an error when analyzing live files
    if [[ ! "$i1" ]] ; then
        TRACE "inode fetch failed.  retry inode fetch for file $f1"
        sleep 1 && i1="$($STAT --format="%i" "${f1//\"/}")"
        if [[ ! "$i1" ]] ; then
            TRACE "Double failure when trying to fetch inode for file $f1"
        fi
    fi
    if [[ ! "$i2" ]] ; then
        TRACE "inode fetch failed.  retry inode fetch for file $f2"
        sleep 1 && i2="$($STAT --format="%i" "${f2//\"/}")"
        if [[ ! "$i2" ]] ; then
            TRACE "Double failure when trying to fetch inode for file $f2"
        fi
    fi

    if [[ "$i1" ]] && [[ "$i2" ]] ; then
        if [[ "$i1" == "$i2" ]] ; then
#            TRACE "# The files are linked"
            return $SAME
        else
#            TRACE "# The files are NOT linked"
            return $DIFFERENT
        fi
    else
        [[ ! "$i1" ]] && TRACE "the inode for ${f1} is null!"
        [[ ! "$i2" ]] && TRACE "the inode for ${f2} is null!"
        return $ERROR
    fi
}

validate_line() {
    INVALID=1
    OK=0
    # TODO: atomic md5 file comparison & hard link
    if [[ "$l" != "$1" ]] ; then
        TRACE "# ERROR: line cannot be passed as parameter!"
        TRACE "#   line: $l"
        TRACE "#   param: $1"
        TRACE "# DO NOT EXECUTE LINE"
        return $INVALID
    fi
    l_parse1="${l#ln*\"}"
    l_parse2="${l_parse1%\"}"
    l1="${l_parse2%\" \"*}"
    if [[ ! -f "$l1" ]] ; then
        TRACE "# ERROR: $l1 doesn't exist"
        TRACE "#  line=$l"
        TRACE "# DO NOT EXECUTE LINE"
        return $INVALID
    fi
    l2="${l_parse2#*\" \"}"
    if [[ ! -f "$l2" ]] ; then
        TRACE "# ERROR: $l2 doesn't exist"
        TRACE "#   line=$l"
        TRACE "# DO NOT EXECUTE LINE"
        return $INVALID
    fi

    if areLinked "$l1" "$l2" ; then
        TRACE "# Files are already linked:"
        TRACE "#   $l1"
        TRACE "#   $l2"
        return $INVALID
    else
        md51="$(md5sum "$l1" | cut -f 1 -d ' ')"
        md52="$(md5sum "$l2" | cut -f 1 -d ' ')"
        if [[ "$md51" != "$md52" ]] ; then
            TRACE "# ERROR: file md5s do not match:"
            TRACE "#  $md51  $l1"
            TRACE "#  $md52  $l2"
            TRACE "# DO NOT EXECUTE LINE"
            return $INVALID
        fi
        #TRACE "# file md5s match: $md51  Execute line."
        #TRACE "# $l"
        links="$($STAT --format="%h" "$l1")"
        if [[ "$links" == 1 ]] ; then
            TRACE "# replacing last instance of \"${l2}\""
            savings="$($STAT --format="%s" "$l2")"
            TRACE "# savings: $savings"
        fi
        return $OK
    fi
    # should not reach this point.  If so, return error
    ERROR=2
    return $ERROR
}

get_available_space() {
    df . \
    | sed 1d \
    | while read a b c d e f ; do
        echo $d
      done
}

perform_operations() {
    TRACE "${ME}: start"
    (
        df -h .
        df .
        $CAT ${DBDIR}/md5_size_report.txt | grep "Total" | tail -n 1
        num_lines="$($CAT ${DBDIR}/duplicate_report.sh | wc -l)"
        TRACE "$num_lines hard link statements to be executed"
        available1="$(get_available_space)"
        if [[ "$VERIFY" ]] ; then
            $CAT ${DBDIR}/duplicate_report.sh \
            | while read l ; do
                validate_line "$l" && echo "$l"
                done \
            | /bin/sh
        else
            $CAT ${DBDIR}/duplicate_report.sh \
            | /bin/sh
        fi
        df -h .
        df .
        available2="$(get_available_space)"
        savings="$(expr $available2 - $available1)"
        TRACE "increase in disk availability since the start of the operation: $savings 1k blocks"
    ) >&2
    TRACE "${ME}: completed $(date)"
    TRACE "It's recommended to execute the following line to archive the reports: "
    TRACE "mv ${DBDIR} ${DBDIR}_$(hostname)_executed_$(date "+%Y%m%d")"
}

############################################################
# Main
EXECUTE_LOG="./${DBDIR}/execute.log"
EXECUTE_LOG="./${DBDIR}/execute_$(dt).log"
TRACE "${ME}: logging to ${EXECUTE_LOG}" >&2
TRACE "${ME}: TODO: check for progress script in ${MYDIR}" >&2

perform_operations >> ${EXECUTE_LOG} 2>&1
#/usr/bin/time -p perform_operations
