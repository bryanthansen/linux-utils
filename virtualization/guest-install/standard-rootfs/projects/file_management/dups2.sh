#!/bin/bash
# Bryant Hansen

[[ ! -d .filedb ]] && mkdir .filedb
nice nohup \
	/projects/file_management/find_duplicates2.sh >> \
	./.filedb/find_dups.log 2>&1 & \
	echo "find_duplicates2.sh: logging to ./.filedb/find_dups.log"
