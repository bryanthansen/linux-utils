#!/bin/bash
# Bryant Hansen
# 20090505
# 20120102

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# find_duplicates.sh

# Algorithm:
#   dump_files
#   check_cache
#   add_md5s to misses
#   combine hits + misses_with_md5s
#   find multiple inodes per md5
#   generate reports
#   execute dedup

# $Id: find_duplicates2.sh 282 2012-09-13 13:27:10Z gink $

# TODO: import caching from next_gen
# TODO: implement smaller files via MD5 dirs ab/cd/files.txt.gz or ab/cd/ef/files.txt.gz
# TODO: must escape filenames with quotes
#       can test with "./backups/volume1/backups/data.verona/media/mp3/compilations/Extreme Clubhits IV/10 - Michael Cretu - Total Normal (7" version).mp3"

ME="$(basename "$0")"
MYDIR="$(dirname "$0")"
USAGE="${ME}  [  DBDIR  ]"

# CAT="cat"
# CAT="head -n 500"
CAT="flexcat"

# the minimum size of the file to replace with a hard link
MIN_SIZE=4096
MIN_SIZE=8192

if [[ "$1" == "-h" ]] || [[ "$1" == "--help" ]] ; then
    echo "USAGE: " >&2
    echo "   $USAGE" >&2
    exit 1
fi

#DBDIR=.filedb
DBDIR=.filedb_$(hostname)_$(date "+%Y%m%d_%H%M%S")
if [[ "$1" ]] ; then
    if [[ ! -d "$1" ]] ; then
        echo "$ME ERROR: DBDIR $1 does not exist as a directory." >&2
        echo "Exiting abnormally." >&2
        exit 2
    fi
    DBDIR="$1"
fi

INFO_FILE=$DBDIR/info.txt
PARAM_FILE=$DBDIR/params.auto.conf
FILE_LIST="$DBDIR/file_list.txt"
FILE_MD5S=$DBDIR/files.txt
CACHE_HITS=$DBDIR/file_cache_hits.txt
CACHE_MISSES=$DBDIR/file_cache_misses.txt
NEW_FILE_MD5S=$DBDIR/new_files.txt
DUPLICATE_FILES=$DBDIR/md5_dup_filelist.txt
DUP_REPORT=$DBDIR/duplicate_report.sh
CHANGE_REPORT=$DBDIR/change_report.txt
MD5_SIZE_REPORT=$DBDIR/md5_size_report.txt

# not currently used
EXCLUDES_FILE=$DBDIR/excludes.txt
UNPROCESSED=$DBDIR/unprocessed.txt
ERRORFILE=$DBDIR/errors.txt

DEFAULT_CONF="/etc/find_duplicates.conf"
USER_CONF="~/find_duplicates.conf"
LOCAL_CONF="${DBDIR}/find_duplicates.conf"


# ERROR CONSTANTS
NO_ERROR=0
ERR_UNSPECIFIED=-1
ERR_INFILE_NOT_FOUND=-2
ERR_WRONG_ARGS=-3
ERR_INPUT_DIR_NOT_FOUND=-4


DebugMessage()
{
    local NO_CR_ARG=""
    local SPECIAL_CHAR_ARG=""
    local ARGS=""
    local OPTION_FOUND="0"
    # local LOG_RECORD_HEADER="`date "+%Y%m%d%H%M%S"`:`basename "$0"`: "
    local LOG_RECORD_HEADER="`date "+%Y%m%d %H%M%S"`: "

    while [[ "$OPTION_FOUND" ]] ; do
        OPTION_FOUND=""
        if [[ "$1" == "-e" ]] ; then
            OPTION_FOUND="-e"
            # ARGS="${ARGS} -e" # use this as default anyway
            shift
        fi
        if [[ "$1" == "-n" ]] ; then
            OPTION_FOUND="-n"
            ARGS="${ARGS} -n"
            LOG_RECORD_HEADER=""
            shift
        fi
    done

    echo -e ${ARGS} "${*/${ARGS}/}" | while read line ; do
        ( [[ "$line" ]] && echo -n "${LOG_RECORD_HEADER}" ) ; echo "$line"
    done >&2

    return $NO_ERROR
}

human_readable()
{
    hr_val=$1

    # very large number example: 1.52519e+10 bytes
    # can be a comma with German language options: 1,52519e+10 bytes
    if [[ `expr match "$hr_val" ".*\([0-9][0-9]e+[0-9][0-9]\).*"` ]] ; then
        val="${hr_val/,/}"
        val="${val/./}"
        base=${val/e+*/}
        exp=${val/*e+/}
        [[ exp == 9 ]] && echo "${base:0:1}.${base:1:3}G" && return
        [[ exp == 10 ]] && echo "${base:0:2}.${base:2:2}G" && return
        [[ exp == 11 ]] && echo "${base:0:3}.${base:3:1}G" && return
        [[ exp == 12 ]] && echo "${base:0:1}.${base:1:3}T" && return
    else
        (( $1 > 100000000000 )) && echo "`expr $1 \/ 1000000000`G" && return
        (( $1 > 100000000 )) && echo "`expr $1 \/ 1000000`M" && return
        (( $1 > 100000 )) && echo "`expr $1 \/ 1000`k" && return
        echo $1
    fi
}

# Description: this function is just a wrapper for 'cat', which will call zcat with the compressed version
# of the same file if the uncompressed version doesn't exist
# inputs: filename of the file to be cat'ed to stdout (either raw text or zipped format with .gz ending)
# output: file contents
flexcat() {
    USAGE="filecat  input_filename"
    while [[ "$1" ]] ; do
        if [[ -f "$1" ]] ; then
            cat "$1"
        else
            if [[ -f "$1".gz ]] ; then
                zcat "$1".gz
            else
                return -1
            fi
        fi
        shift
    done
    return $NO_ERROR
}

gen_sigs_from_tmpfile() {
    if [[ -f "$1".sigs ]] ; then
        DebugMessage "gen_sigs_from_md5_db: ${1}.sigs already exists"
        return 0
    fi
    flexcat "$1" | \
    sort -n | \
    while read inode uid gid mtime ctime driveid size filename; do
        echo "$uid $gid $mtime $ctime $inode $driveid $size \"${filename}\"" | md5sum
    done > "$1".sigs
    return 0
}

gen_sigs_from_md5_db() {
    if [[ -f "$1".sigs ]] ; then
        DebugMessage "gen_sigs_from_md5_db: ${1}.sigs already exists"
        return 0
    fi
    flexcat "$1" | \
    sort -n | \
    while read md5 b ; do
        echo "$b" | md5sum
    done > "$1".sigs
    return 0
}

check_cache() {

    local FILE_LIST="$1"
    local FILE_MD5_CACHE="$2"
    local CACHE_HITS="$3"
    local CACHE_MISSES="$4"

    DebugMessage "enter check_cache"
    echo -n "" > "$CACHE_HITS"
    echo -n "" > "$CACHE_MISSES"
    (
        flexcat "$FILE_LIST" | \
            sort -n | \
            while read inode uid gid mtime ctime driveid size filename ; do
                echo "$uid $gid $mtime $ctime $inode $driveid $size \"${filename}\" 0"
            done
        flexcat "$FILE_MD5_CACHE" | \
            while read md5 uid gid mtime ctime inode driveid size filename ; do
                echo "$uid $gid $mtime $ctime $inode $driveid $size $filename $md5"
            done
    ) | \
    sort -r |
    while read uid gid mtime ctime inode driveid size filename_and_md5 ; do
        md5="${filename_and_md5##*\" }"
        filename="${filename_and_md5%\"*}\""
        if [[ 0 == "$md5" ]] ; then
            if [[ "$uid $gid $mtime $ctime $inode $driveid $size $filename" == "$l_uid $l_gid $l_mtime $l_ctime $l_inode $l_driveid $l_size $l_filename" ]] ; then
                md5="$l_md5"
                echo "$md5 $uid $gid $mtime $ctime $inode $driveid $size $filename" >> "$CACHE_HITS"
            else
                echo "$inode $uid $gid $mtime $ctime $driveid $size ${filename//\"/}" >> "$CACHE_MISSES"
            fi
        fi
        l_uid="$uid"
        l_gid="$gid"
        l_mtime="$mtime"
        l_ctime="$ctime"
        l_inode="$inode"
        l_driveid="$driveid"
        l_size="$size"
        l_md5="$md5"
        l_filename="$filename"
    done

    DebugMessage "$(cat "$CACHE_HITS" | wc -l) cache hits"
    DebugMessage "$(cat "$CACHE_MISSES" | wc -l) cache misses"
    DebugMessage "check_cache complete."
}

#    - must use "inode uid gid mtime ctime driveid size filename" as signature
#    - generate lookup table on older files.txt.gz files
#    - lookup format: signature md5
#    - generate for new files.txt.gz and previous_backup/files.txt.gz
#    - identical number of lines; line number makes association; efficiency in combining
dump_file_data() {
    USAGE="dump_file_data  directory  output_file"
    DebugMessage "enter dump_file_data: dumping all files and inodes in directory tree \"${1}\" larger than ${MIN_SIZE} bytes..."
    [[ ! -d "$1" ]]         && DebugMessage "  ERROR: $1 is not a valid directory.  Exiting abnormally!" && exit $ERR_INPUT_DIR_NOT_FOUND
    [[ ! "$2" ]]            && DebugMessage "  ERROR: this function requires at least 3 arguments!  Exiting abnormally!" && exit $ERR_WRONG_ARGS
    [[ -f "$2" ]]           && DebugMessage "  ${2} already exists"     && return $NO_ERROR
    [[ -f "$2".gz ]]        && DebugMessage "  ${2}.gz already exists"  && return $NO_ERROR
    local FILES_TEMP="`mktemp "$DBDIR/.files.tmp.XXXXXX"`"

    # inode uid gid mtime-epochs ctime-epochs device size-bytes name
    # TODO consider using xargs, rather than find's -exec
    #   time find "$1" -type f -size +${MIN_SIZE}c -mount | \
    #      xargs stat --printf='%i %u %g %Y %Z %D %s %n\n' | \
    #      xargs stat --format='%i %u %g %Y %Z %D %s %n' | \
    time find "$1" \
            -mount \
            -type f \
            -size +${MIN_SIZE}c \
            -exec stat \
            --printf='%i %u %g %Y %Z %D %s %n\n' "{}" \; | \
        tee "$FILES_TEMP" | \
        sort -n | \
        while read inode uid gid mtime ctime driveid size filename; do
            if [[ "$inode" != "$lastinode" ]] ; then
                md5="$(md5sum "$filename" | cut -f 1 -d ' ')"
                #md5="${md5:0:32}"
            fi
            if [[ "${#md5}" != 32 ]] ; then
                DebugMessage "Failed to calculate MD5 sum of $filename"
                lastinode=""
                continue
            fi
            # note: the order of the fields is critical
            # output will be sorted by highest priority file stats first
            echo "$md5 $uid $gid $mtime $ctime $inode $driveid $size \"${filename}\""
            lastinode=$inode
        done > "$2"
    rm "$FILES_TEMP"
}

dump_file_data_new() {
    USAGE="dump_file_data  directory  output_file"
    DebugMessage "\ndump_inodes_in_dir: dumping all files and inodes in directory tree \"${1}\" larger than ${MIN_SIZE} bytes..."
    [[ ! -d "$1" ]]         && DebugMessage "  ERROR: $1 is not a valid directory.  Exiting abnormally!" && exit $ERR_INPUT_DIR_NOT_FOUND
    [[ ! "$2" ]]            && DebugMessage "  ERROR: this function requires at least 3 arguments!  Exiting abnormally!" && exit $ERR_WRONG_ARGS
    [[ -f "$2" ]]           && DebugMessage "  ${2} already exists"     && return $NO_ERROR
    [[ -f "$2".gz ]]        && DebugMessage "  ${2}.gz already exists"  && return $NO_ERROR
    local FILES_TEMP="`mktemp "$DBDIR/.files.tmp.XXXXXX"`"

    # inode uid gid mtime-epochs ctime-epochs device size-bytes name
    # TODO consider using xargs, rather than find's -exec
    #   time find "$1" -type f -size +${MIN_SIZE}c -mount | \
    #      xargs stat --printf='%i %u %g %Y %Z %D %s %n\n' | \
    #      xargs stat --format='%i %u %g %Y %Z %D %s %n' | \
    time find "$1" \
            -mount \
            -type f \
            -size +${MIN_SIZE}c \
            -exec stat --printf='%i %u %g %Y %Z %D %s %n\n' "{}" \; \
            > "$FILES_TEMP"

    DebugMessage "Checking cache for md5 matches to ${FILES_TEMP}..."
    for f in "$CACHE_DIR"/* ; do
        ${MYDIR}/check_cache.sh "$FILES_TEMP" "$f"
    done
    DebugMessage "Cache check complete."

}

# Produce a list of MD5s' which are associated with multiple inodes
find_identical_files_with_multiple_inodes() {
    USAGE="find_identical_files_with_multiple_inodes  input_file  output_file"
    DebugMessage "\nfind_identical_files_with_multiple_inodes: listing all identical files that use different inodes..."
    ERR_INFILE_NOT_FOUND=-2
    [[ ! -f "$1" && ! -f "${1}.gz" ]] && DebugMessage "  ERROR: $1 does not exist.  Exiting abnormally!"    && exit $ERR_INFILE_NOT_FOUND
    [[ ! "$2" ]]        && DebugMessage "  ERROR: this function requires at least 2 arguments!  Exiting abnormally!"    && exit $ERR_WRONG_ARGS
    [[ -f "$2" ]]       && DebugMessage "  $2 already exists"       && return $NO_ERROR
    [[ -f "$2".gz ]]    && DebugMessage "  ${2}.gz already exists"  && return $NO_ERROR
    local DUPLICATE_FILES_TEMP="`mktemp "$DBDIR/.md5_dup_filelist.tmp.XXXXXX"`"

    # echo "$md5 $uid $gid $mtime $ctime $inode $driveid $size \"${filename}\""
    local FIELDNUM_MD5=1
    local FIELDNUM_INODE=6
    time ( \
        # when this is sorted
        $CAT "$1" | sort | cut -d' ' -f ${FIELDNUM_MD5},${FIELDNUM_INODE} | uniq | cut -d' ' -f 1 | uniq -d | tee "$DUPLICATE_FILES_TEMP"
        $CAT "$1"
    ) | \
    sort | \
    while read line ; do
        last_md5="$md5"                     # store the previous MD5 so we know when we start processing a new one
        md5="${line:0:32}"              # the first 32 characters are the MD5
        [[ ! "$md5" == "$last_md5" ]] && unset dup  # new MD5; not a duplicate by definition
        [[ "$dup" ]] && echo "$line"            # output consists of printing the lines containing duplicates
        [[ ! "${line:33}" ]] && dup=1           # empty MD5 label - the following lines are a list of duplicates
    done > "$2"
    rm "$DUPLICATE_FILES_TEMP"
}

# TODO: files containing $ symbols are not properly handled here
#   busybox-compatible sed expression to escape them: sed "s/\\\$/\\\\\$/g"
gen_dup_reports() {
    USAGE="gen_dup_reports  input_file  duplicate_output_file  change_report  size_report"
    DebugMessage "\ngen_dup_reports: generating an executable report that can be used to remove all duplicates (identical files with multiple inodes)..."
    [[ ! -f "$1" && ! -f "${1}.gz" ]] && DebugMessage "  ERROR: $1 does not exist.  Exiting abnormally!"    && exit $ERR_INFILE_NOT_FOUND
    [[ ! "$3" ]]        && DebugMessage "  ERROR: change report filename missing!  This function requires at least 4 arguments!  Exiting abnormally!"   && exit $ERR_INFILE_NOT_FOUND
    [[ ! "$4" ]]        && DebugMessage "  ERROR: md5 size report temp filename missing!  This function requires at least 4 arguments!  Exiting abnormally!"    && exit $ERR_INFILE_NOT_FOUND
    [[ -f "$2" ]]       && DebugMessage "  $2 already exists"       && return $NO_ERROR
    [[ -f "$2".gz ]]    && DebugMessage "  ${2}.gz already exists"  && return $NO_ERROR
    [[ -f "$3" ]]       && DebugMessage "  $3 already exists.  Removing..."         && rm "$3"
    [[ -f "$3".gz ]]    && DebugMessage "  ${3}.gz already exists.  Removing..."    && rm "$3".gz
    [[ -f "$4" ]]       && DebugMessage "  $4 already exists.  Removing..."         && rm "$4"
    [[ -f "$4".gz ]]    && DebugMessage "  ${4}.gz already exists.  Removing..."    && rm "$4".gz
    local lastinode=""
    local DUPFILE_REPORT="$2"
    local CHANGE_REPORT="$3"
    local MD5_SIZE_REPORT="$4"

    # TODO: consider doing away with the MD5_SIZE_REPORT; it's redundant; print size saved in output log

    # EXPERIMENT: use file descriptors in order to avoid repeatedly opening files
    exec 3>"$DUPFILE_REPORT"
    exec 4>"$CHANGE_REPORT"
    exec 5>"$MD5_SIZE_REPORT"

    # Highest priority is 1) root-owned  2) root-group  3) oldest mtime  4) oldest ctime
    # the "sort" should prioritize the list, starting with files that have uid=0, then gid=0, then oldest mtime, then oldest ctime
    time $CAT "$1" | sort | \
    (
        local total_size=0
        local inodes=0
        while read md5 uid gid mtime ctime inode device size filename ; do
            if [[ ! "$md5" == "$last_md5" ]] ; then
                inodes=0
                # new md5
                base_filename="$filename"
                base_inode="$inode"
                base_uid="$uid"
                base_gid="$gid"
                base_mtime="$mtime"
                base_ctime="$ctime"
                base_size="$size"
            else
                if [[ "$inode" != "$base_inode" ]] ; then
                    if [[ "$uid $gid $mtime $ctime $device" != "$base_uid $base_gid $base_mtime $base_ctime $base_device" ]] ; then
                        # log the change in the change report
                        ( \
                            echo -e "\ntarget: ${filename}"
                            echo -e "  stats are being overwritten with the stats of:"
                            echo -e "source: ${base_filename}"
                            [[ $uid != $base_uid ]] && echo "uid: $uid -> $base_uid"
                            [[ $gid != $base_gid ]] && echo "gid: $gid -> $base_gid"
                            [[ $mtime != $base_mtime ]] && echo "mtime: $mtime -> $base_mtime"
                            [[ $ctime != $base_ctime ]] && echo "ctime: $ctime -> $base_ctime"
                        ) >&4
                    fi
                    # document space saved in the size report
                    if [[ "$inode" != "$lastinode" ]] ; then
                        inodes="`expr $inodes + 1`"
                        echo "$md5 $inode $size $base_filename" >&5
                        total_size="`expr $total_size + $size`"
                    fi
                    lastinode="$inode"
                    echo "ln -f ${base_filename} ${filename}"
                fi
            fi
            last_md5="$md5"                     # store the previous MD5 so we know when we start processing a new one
        done >&3
        echo "Total duplicate file size in bytes: ${total_size} $(human_readable ${total_size})" >&5
        DebugMessage "Total duplicate file size in bytes: ${total_size}"
    )
    # close file descriptors
    3>&-
    4>&-
    5>&-
}

compress_results() {
    for f in "$FILE_LIST" "$DUPLICATE_FILES" "$DUP_REPORT" "$CHANGE_REPORT" "$MD5_SIZE_REPORT" ; do
    [[ -f "$f" ]] && [[ ! -f "$f".gz ]] && gzip -9 "$f"
    done
}

# TODO: this is a bit obsolete / human-readable space savings should be reported in the output log
get_space_savings() {
    USAGE="get_space_savings  md5_size_report_inputfile"
    [[ ! -f "$1" && ! -f "${1}.gz" ]] && DebugMessage "  ERROR: $1 does not exist.  Exiting abnormally!"    && exit $ERR_INFILE_NOT_FOUND
    [[ -f "$1" ]] && space_saved="`tail -n 1 "$1"`"
    [[ -f "$1".gz ]] && space_saved="`zcat "$1".gz | tail -n 1`"
    [[ ! "$space_saved" ]] && DebugMessage "get_space_savings ERROR: failed to get space saved from ${1}.  Exiting abnormally!"     && exit -5
    size1="${space_saved//*: /}"
#   size2="$(human_readable "$size1")"
    echo "${size1} (${size2})"
}

dump_dir_info() {
    info_file="$1"
    DIR="."
    DebugMessage "Dumping info to $info_file"
    (
        dev="$(df $DIR | tail -n 1 | cut -f 1 -d ' ')"
        echo "##############################################"
        echo "#  DEVICE NAME: $dev"
        echo "##############################################"
        echo ""
        echo "# hard drive parameters of device ${dev}:"
        echo "hdparm -i $dev"
        hdparm -i "$dev"
        echo ""
        echo "# device attributes of ${dev}:"
        echo "blkid $dev"
        blkid "$dev"
        echo ""
        echo "# Storage space parameters:"
        echo "df -h $DIR"
        df -h .
        # du takes too long
        #echo "du -hs $DIR"
        #du -hs .
        #echo "du -hs *"
        #du -hs *
        echo ""
        echo "# mount records of $dev"
        echo "mount -l | grep $dev"
        mount -l | grep "$dev"
        echo ""
        echo "# instances of $dev in the fstab:"
        echo "grep $dev /etc/fstab"
        grep "$dev" /etc/fstab 2>/dev/null || echo "  not found"
        echo ""
        echo "# instances of $dev in the mtab:"
        echo "grep $dev /etc/mtab"
        grep "$dev" /etc/mtab 2>/dev/null || echo "  not found"
        echo ""
        echo "# dmesg logs of device $(basename "$dev"):"
        echo "dmesg | grep $(basename "$dev")"
        dmesg | grep "$(basename "$dev")"
        echo ""
        echo "# other filedb in directory $DIR"
        echo "find $DIR -maxdepth 2 -type d -name .filedb* -exec ls -ld {} \;"
        find $DIR -maxdepth 2 -type d -name ".filedb*" -exec ls -ld "{}" \;
    ) > $info_file 2>&1
}

perform_operations() {
    DebugMessage "Starting (DIR=$DBDIR)..."
    # ==================================		========================	=======================
    # OPERATION 									INPUT(S)					OUTPUT(S)
    # ==================================		========================	=======================
    dump_dir_info                                                           "$INFO_FILE"
#    dump_file_data_new                          "."                         "$FILE_LIST"
    dump_file_data                              "."                         "$FILE_LIST"
    find_identical_files_with_multiple_inodes 	"$FILE_LIST" 					"$DUPLICATE_FILES"
    gen_dup_reports 							"$DUPLICATE_FILES"			"$DUP_REPORT" "$CHANGE_REPORT" "$MD5_SIZE_REPORT"
    # ==================================		========================	=======================
    DebugMessage "\nTotal space that can be saved by executing \"$DUP_REPORT\": `get_space_savings "$MD5_SIZE_REPORT"`"
    compress_results
    DebugMessage -e "\n${0}: done."

    # TODO: determine how to write progress scripts for each stage
    # step #1: detect which stage we're in
    # dump_file_data stage requires a terribly-long find statement to determine progress
}

perform_operations_test() {
    DebugMessage "Starting (DIR=$DBDIR)..."
    # ==================================        ========================       =======================
    # OPERATION                                     INPUT(S)                       OUTPUT(S)
    # ==================================        ========================       =======================
    dump_dir_info                                                              "$INFO_FILE"
    dump_file_data                              "."                            "$FILE_LIST"
    find_identical_files_with_multiple_inodes   "$FILE_LIST"                   "$DUPLICATE_FILES"
    gen_dup_reports                             "$DUPLICATE_FILES"             "$DUP_REPORT" "$CHANGE_REPORT" "$MD5_SIZE_REPORT"
    # ==================================        ========================       =======================
    DebugMessage "\nTotal space that can be saved by executing \"$DUP_REPORT\": `get_space_savings "$MD5_SIZE_REPORT"`"
    compress_results
    DebugMessage -e "\n${0}: done."

    # TODO: determine how to write progress scripts for each stage
    # step #1: detect which stage we're in
    # dump_file_data stage requires a terribly-long find statement to determine progress
}

# source environment
[[ "$DEFAULT_CONF" ]]   && [[ -f "$DEFAULT_CONF" ]]     && . "$DEFAULT_CONF"
[[ "$USER_CONF" ]]  && [[ -f "$USER_CONF" ]]    && . "$USER_CONF"
LOCAL_CONF="${DBDIR}/find_duplicates.conf"
[[ "$LOCAL_CONF" ]]     && [[ -f "$LOCAL_CONF" ]]   && . "$LOCAL_CONF"

[[ ! -d "$DBDIR" ]] && mkdir -p "$DBDIR"
chmod o-rwx "$DBDIR"

time perform_operations >> ./${DBDIR}/find_dups.log 2>&1 &

echo "${ME}: logging to ./${DBDIR}/find_dups.log" >&2

echo "${ME}: progress can be observed by executing find_dups_progress.sh ${DBDIR} or monitor_progress.sh ${DBDIR}" >&2

exit 0