#!/bin/bash
# Bryant Hansen

ME="$(basename "$0")"

filename="$1"
fileset="$*"

# preload file into cache, to compare timings accurately

human_readable()
{
        hr_val=$1

        # very large number example: 1.52519e+10 bytes
        # can be a comma with German language options: 1,52519e+10 bytes
        if [[ `expr match "$hr_val" ".*\([0-9][0-9]e+[0-9][0-9]\).*"` ]] ; then
                val="${hr_val/,/}"
                val="${val/./}"
                base=${val/e+*/}
                exp=${val/*e+/}
                [[ exp == 9 ]] && echo "${base:0:1}.${base:1:3}G" && return
                [[ exp == 10 ]] && echo "${base:0:2}.${base:2:2}G" && return
                [[ exp == 11 ]] && echo "${base:0:3}.${base:3:1}G" && return
                [[ exp == 12 ]] && echo "${base:0:1}.${base:1:3}T" && return
        else
                (( $1 > 100000000000 )) && echo "`expr $1 \/ 1000000000`G" && return
                (( $1 > 100000000 )) && echo "`expr $1 \/ 1000000`M" && return
                (( $1 > 100000 )) && echo "`expr $1 \/ 1000`k" && return
                echo $1
        fi
}

function displayFileSize() {
    size1=$(stat --format=%s "$filename")
    size2=$(ls -alh "$filename" | while read perm links user group size mon day time file ; do echo $size ; done)
    echo "${size2}/${size1}"
}

function sizeN() {
    stat --format=%s "$1"
}

function sizeH() {
    ls -alh "$1" | while read perm links user group size mon day time file ; do echo $size ; done
}

function ratio() {
    uncompressed="$1"
    compressed="$2"
    expr \( $uncompressed + $compressed / 2 \) / $compressed
}

function ratio2() {
    local uncompressed=$1
    local compressed=$2

    decimalPoints=1

    multiplier=1
    for ((n=0;n<$decimalPoints;n++)) ; do
        ((multiplier\*=10))
    done

    ratio2=$(expr \( \( $uncompressed \* $multiplier \) + $compressed / 2 \) / $compressed )
    digits=${#ratio2}
    decimalPlace=$digits
    ((decimalPlace-=decimalPoints))

    int=${ratio2:0:$decimalPlace}
    dec=${ratio2:$decimalPlace}

    (( "${#dec}" > 0 )) && ratio2="${int}.${dec}"
    # echo "${uncompressed}:${compressed}: decimalPlace=$decimalPlace  digits=$digits  int=$int  dec=$dec  ${ratio2}:1"

    echo "$ratio2"
}

function do_compression_benchmarks() {
    # TODO: if a directory, check recursively
    local program="$1"
    local filename="$2"
    # set heading
    for level in 1 5 9 ; do
        starttime=$(date +%s)
        case "$program" in
        7z)
            #f="$(readlink -f "$filename")"
            command="7za a -an -txz -mx=${level} -so \"${filename}\""
            #echo "command=$command | wc -c" >&2
            #compressed="$(7za a -an -txz -mx=${level} -so \"${f}\" | wc -c)"
            compressed="$(echo "$command" | /bin/bash 2>/dev/null | wc -c 2>&1 | tr '\n' ' ')"
            #echo "compressed=$compressed" >&2
            ;;
        *)
            #compressed="$(${program} -${level} -c "$filename" | wc -c)"
            command="${program} -${level} -c \"${filename}\" | wc -c"
            compressed="$(echo "$command" | /bin/bash)"
            #echo "command=$command  compressed=$compressed" >&2
            ;;
        esac
        endtime=$(date +%s)
        elapsed=$(expr $endtime - $starttime)
        uncompressed="$(cat "$filename" | wc -c)"
        ratio="$(ratio $uncompressed $compressed)"
        (( $ratio < 10 )) && ratio="$(ratio2 $uncompressed $compressed)"

        size_digits="${#uncompressed}"

        printf "%12s -%s:  %${size_digits}s / %s  %3s:1 %5d seconds\n" \
                "${program}" \
                "${level}" \
                "${compressed}" \
                "$(human_readable "${compressed}")" \
                "${ratio}" \
                "${elapsed}"
    done
}

# TODO: take files, directories & globs into account; make tarball?

# if all arguments together are a file, then assume only a single file has been passed in to this script
if [[ -f "$*" ]] ; then
    echo "Filename: \"${filename}\"  uncompressed size: $(sizeH "${filename}")/$(sizeN "${filename}")" >&2

    # preload the file into cache, so the compression timing benchmarks aren't skewed against the first run
    cat "$filename" > /dev/null

    printf "%15s %12s %10s %10s\n" "Method" "Size" "Ratio" "Time"
    echo "#-----------------------------------------------------------------#"

    for program in /bin/gzip pigz /bin/bzip2 lbzip2 lzma xz "xz -e" 7z; do
        do_compression_benchmarks "$program" "$filename"
    done
else
    tarball="/tmp/.${ME}_fileset.tar"

    echo "File set: \"${fileset}\"" >&2

    echo "Creating temporary tarball (${tarball})..." >&2
    tar cvf $tarball $fileset 2>&1 | \
    sed "s/^/\t/"

    echo "Original Size: $(sizeH "${tarball}")/$(sizeN "${tarball}")" >&2

    # preload the file into cache, so the compression timing benchmarks aren't skewed against the first run
    cat "$tarball" > /dev/null

    printf "%15s   %12s   %11s   %7s\n" "Method" "Size" "Ratio" "Time"
    echo "#-----------------------------------------------------------------#"
    for program in /bin/gzip pigz /bin/bzip2 lbzip2 lzma xz "xz -e" 7z; do
        do_compression_benchmarks "$program" "$tarball"
    done

    rm $tarball
fi

exit 0
