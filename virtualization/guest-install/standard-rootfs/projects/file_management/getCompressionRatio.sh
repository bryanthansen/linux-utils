#!/bin/bash
# Bryant Hansen

filename="$1"

#case "${test/*.}" in
case "${filename##*.}" in
	gz)
		compressed="$(cat "$filename" | wc -c)"
		uncompressed="$(zcat "$filename" | wc -c)"
		# ratio="$(expr $uncompressed / $compressed)"
		# with rounding:
		ratio="$(expr \( $uncompressed + $compressed / 2 \) / $compressed)"
		# with 1 decimal point of precision:
		#		ratio2="$(expr $uncompressed \* 10 / $compressed)"
		#		ratio2="${ratio2:0:$digits}.${ratio2:$digits}"
		echo "${filename}: compressed size = ${compressed}, uncompressed = ${uncompressed}, compression ratio = ${ratio}:1"
		;;
	bz2)
		bz2unpack ${SROOT}/${x}
		;;
	tgz)
		echo "tgz files are not yet supported" >&2
		;;
	*)
		echo "file format not recognized: *.${filename##*.}" >&2
		exit
		;;
esac                                      

