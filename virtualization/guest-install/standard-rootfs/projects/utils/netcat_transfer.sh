
# on the receiving side:
#   netcat -l -p 50009 | tar xzvf -
# if the size is known:
#   netcat -l -p 50009 | bar -s 5G | tar xzvf -

# on the sending side
#   tar cvO pathname | netcat host port
