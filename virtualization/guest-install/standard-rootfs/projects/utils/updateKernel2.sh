#!/bin/bash
# Bryant Hansen

dt=$(date "+%Y%m%d_%H%M%S")
ver="$(basename "$(readlink -f .)")"
ver="${ver#linux-}"
kernel=/boot/kernel-${ver}-${dt}

EDITOR=nano

[[ "$PWD" != /usr/src/linux ]] && echo "# WARNING: this script is intended to be executed out of the /usr/src/linux directory!" >&2

if ! which $EDITOR > /dev/null ; then
    echo "# ERROR: EDITOR $EDITOR not found in path" >&2
fi

echo "cp -a arch/x86/boot/bzImage $kernel"
echo "cp -a .config /boot/kernel.config.${ver}-${dt}"
echo "echo \"# ${kernel}\" >> /boot/grub/menu.lst"

echo "" >&2
echo "# to execute this sequence, pipe the results to /bin/bash" >&2
echo "# $0 | /bin/bash" >&2
echo "# then $EDITOR /boot/grub/menu.lst" >&2
