#!/bin/bash
# Bryant Hansen

cd /
/kill_swap.sh

rm -rf /usr/portage/distfiles/*
rm -rf /var/tmp/*
rm -rf /tmp/*
rm -rf ~/tmp/*

time dd if=/dev/zero of=/zero.bin.2 bs=4M
rm -f /zero.bin.2
