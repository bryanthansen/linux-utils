#!/bin/bash
# Bryant Hansen

ME="$(basename "$0")"

BASE_URL=http://bryanthansen.net/linux/install/gentoo-server-1

ALIASES_URL=${BASE_URL}/aliases.sh
ALIASES_FILE="/etc/bash/aliases.sh"

if [[ ! -f "$ALIASES_FILE" ]] ; then

    if pushd /etc/bash > /dev/null ; then
        wget "$ALIASES_URL"

        if [[ -f "$ALIASES_FILE" ]] ; then
            if ! grep -H "aliases\.sh" /etc/bash/bashrc ; then
                echo -e "\n# Added by $0 on $(date)" >> /etc/bash/bashrc
                echo    ". $ALIASES_FILE" >> /etc/bash/bashrc
            fi
        else
            echo "$ME ERROR: failed to download $ALIASES_URL to $ALIASES_FILE" >&2
        fi
        popd
    fi
fi

if ! pushd /root/ > /dev/null ; then
    echo "$ME ERROR: failed to cd to /root/" >&2
    exit 3
fi

[[ ! -d ./.install.tmp ]] && mkdir ./.install.tmp
if ! pushd ./.install.tmp > /dev/null ; then
    echo "$ME ERROR: failed to cd to ${PWD}/.install.tmp"
    exit 4
fi


URL=${BASE_URL}/formatepoch.sh.txt
DIR=/usr/local/bin
DEST=${DIR}/${f%.txt}

f=$(basename $URL)
[[ ! -f "$f" ]] && wget $URL
if [[ ! -f "$f" ]] ; then
    echo "$ME ERROR: failed to download $URL"
    exit 5
fi
if [[ ! -d $DIR ]] ; then
    echo "$ME ERROR: install dir $DIR does not exist" >&2
    exit 6
fi
[[ ${DIR}/${f%.txt} ]] && cp -av ./${f} ${DIR}/${f%.txt}
[[ ! -f ${DIR}/feda ]] && ln -s ${f%.txt} ${DIR}/feda


URL=${BASE_URL}/updateKernel2.sh.txt
DIR=/usr/local/sbin
f=$(basename $URL)
DEST=${DIR}/${f%.txt}

[[ ! -f "$f" ]] && wget $URL
if [[ ! -f "$f" ]] ; then
    echo "$ME ERROR: failed to download $URL"
    exit 5
fi
if [[ ! -d $DIR ]] ; then
    echo "$ME ERROR: install dir $DIR does not exist" >&2
    exit 6
fi
[[ ! ${DEST} ]] && cp -av ./${f} ${DEST}
chmod 700 ${DEST}

popd
popd
