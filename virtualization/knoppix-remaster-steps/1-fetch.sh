#!/bin/sh
# Bryant Hansen

printf "
    # Example:
    # rtorrent http://torrent.unix-ag.uni-kl.de/torrents/KNOPPIX_V7.7.1DVD-2016-10-22-EN.torrent
" >&2

USAGE="$0  host  version"

host="$1"
version="$2"

if [[ -d "$version" ]] ; then
    printf "${version} already exists\n" >&2
    exit 2
fi

if [[ "$host" ]] && [[ "$version" ]] ; then
    printf "rtorrent http://${host}/torrents/${version}.torrent $2\n"
fi
printf "\nUSAGE:\n\t $USAGE\n" >&2

exit 0
