#! /bin/sh
# krhowto_1
# Path to partition you will work on

# script defaults
#OUT_DIR="/media/sdb1"

# conf file (overrides script defaults)
CONF=./remaster.conf
source "$CONF"

if [[ ! "$OUT_DIR" ]] ; then
    echo "ERROR: OUT_DIR must be specified." >&2
    echo "Disk usage of the current directory (.): $(df -h . | tr '\n' ' ')" >&2
    echo "Expect to need 9GB" >&2
    exit 2
fi
if [[ ! -d "$OUT_DIR" ]] ; then
    echo "ERROR: OUT_DIR $OUT_DIR must be a directory." >&2
    exit 3
fi

START=$(date +'%s')
# Disable screensaver
xscreensaver-command -exit
# One sub-directory will be used for the Master-CD
mkdir -p $OUT_DIR/knx/master
cd $OUT_DIR/knx

# You will need a swapfile
dd if=/dev/zero of=swapfile bs=1M count=500
mkswap swapfile ; swapon swapfile

# Make a sub-directory for the source
mkdir -p $OUT_DIR/knx/source/KNOPPIX
echo "Copy the KNOPPIX files to your source directory."
echo "This will take a long time!"
cp -rp /KNOPPIX/* $OUT_DIR/knx/source/KNOPPIX
# Additionally, copy the files to build the ISO later
rsync -aH --exclude="KNOPPIX/KNOPPIX*" /mnt-system/* $OUT_DIR/knx/master
# gunzip inital RAM-disk
mkdir -p $OUT_DIR/knx/minirt/minirtdir
cp $OUT_DIR/knx/master/boot/isolinux/minirt.gz $OUT_DIR/knx/minirt/
cd $OUT_DIR/knx/minirt/
gunzip minirt.gz
cd minirtdir
cpio -imd --no-absolute-filenames < ../minirt
# Enable screensaver
su knoppix -c "xscreensaver -nosplash &"
echo -e "\nFinished! Used time: $(expr $(expr $(date +'%s') - $START) / 60) min. \
  and $(expr $(expr $(date +'%s') - $START) % 60) sec."

