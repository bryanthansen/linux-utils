#! /bin/sh
# krhowto_2
# Path to partition you will work on

# script defaults
OUT_DIR="/media/sdb1"

# conf file (overrides script defaults)
CONF=./remaster.conf
source "$CONF"

# To use the Internet add your nameserver into the chroot folder
cp /etc/resolv.conf $OUT_DIR/knx/source/KNOPPIX/etc/resolv.conf

# Allow X-based programs in chroot
[ -e $OUT_DIR/knx/source/KNOPPIX/home/knoppix/.Xauthority ] \
   && rm $OUT_DIR/knx/source/KNOPPIX/home/knoppix/.Xauthority
cp /home/knoppix/.Xauthority $OUT_DIR/knx/source/KNOPPIX/home/knoppix
chown knoppix:knoppix $OUT_DIR/knx/source/KNOPPIX/home/knoppix/.Xauthority

# prepare enviroment for chroot
mount --bind /dev $OUT_DIR/knx/source/KNOPPIX/dev
mount -t proc proc $OUT_DIR/knx/source/KNOPPIX/proc
mount -t sysfs sysfs $OUT_DIR/knx/source/KNOPPIX/sys
mount --bind /dev/pts $OUT_DIR/knx/source/KNOPPIX/dev/pts
mount --bind /tmp $OUT_DIR/knx/source/KNOPPIX/tmp

chroot /media/sda1/knx/source/KNOPPIX

