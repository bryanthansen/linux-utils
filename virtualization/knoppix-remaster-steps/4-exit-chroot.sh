#! /bin/sh
# krhowto_3
# Path to partition you will work on

# script defaults
OUT_DIR="/media/sdb1"

# conf file (overrides script defaults)
CONF=./remaster.conf
source "$CONF"

for i in dev/pts proc sys dev tmp; do
  umount $OUT_DIR/knx/source/KNOPPIX/$i
done
sed -i '2,$d' $OUT_DIR/knx/source/KNOPPIX/etc/resolv.conf


