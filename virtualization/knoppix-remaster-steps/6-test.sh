#!/bin/bash
# Customized by Bryant Hansen
# Content is licensed under GPL 1.2

# If your processor supports virtualization and it is enabled by the BIOS you can test the new ISO by kvm-qemu. Check for support by:

egrep '(vmx|svm)' --color=always /proc/cpuinfo

flags_in_cpuinfo() {
    flag=$1

    cat /proc/cpuinfo \
    | grep 'svm'      \
    | sed "s/.*://"   \
    | tr ' ' '\n'     \
    | sed 's/\ //g'   \
    | sort            \
    | uniq            \
    | grep 'svm'      \
    | wc -l
}

vmx_flags_in_cpuinfo() {
    flags_in_cpuinfo vmx
}

svm_flags_in_cpuinfo() {
    flags_in_cpuinfo svm
}


echo 'If you get the result "vmx" type'

if [[ "$(vmx_flags_in_cpuinfo)" -gt 0 ]] ; then
    echo "$0 INFO: vmx flag found in cpuinfo; loading kvm-intel" >&2
    modprobe kvm-intel
else if [[ "$(svm_flags_in_cpuinfo)" -gt 0 ]] ; then
    echo "$0 INFO: svm flag found in cpuinfo; loading kvm-amd" >&2
    modprobe kvm-amd
else
    echo "$0 WARNING: no module acceleration found in cpu flags" >&2
fi

echo "INFO: test the new ISO" >&2
kvm -m 512 -cdrom $OUT_DIR/knx/remastered.iso
