#!/bin/bash
# Bryant Hansen

#
# time mkisofs -l -r -J -V "BROPPIX_$(dt)"   -b boot/isolinux/isolinux.bin -no-emul-boot -boot-load-size 4   -boot-info-table -c boot/isolinux/boot.cat   -o /mnt/DATA_004/data/os/knoppix/knoppix_remastered_$(dt).iso /mnt/knoppixmerged

# /etc/fstab:
# overlay /mnt/knoppixmerged overlay rw,relatime,lowerdir=/mnt/knx,upperdir=/mnt/knoppixupper,workdir=/mnt/knoppixwork 0 0

# mount /mnt/DATA_004/data/os/knoppix/KNOPPIX_V7.6.1DVD-2016-01-16-EN.iso /mnt/knx
# mount -t overlay overlay -olowerdir=/mnt/knx,upperdir=/mnt/knoppixupper,workdir=/mnt/knoppixwork /mnt/knoppixmerged

lower=/mnt/knx
upper=/mnt/knoppixupper
workdir=/mnt/knoppixwork
rwdir=/mnt/knoppixmerged
outdir=/mnt/DATA_004/data/os/knoppix

for d in $lower $upper $merged $outdir $rwdir ; do
    [[ ! -d $d ]] && mkdir -p $d
    if [[ ! -d $d ]] ; then
        echo "overlay dir $d does not exist and cannot be created" >&2
        exit 2
    fi
done


cat /etc/mtab | cut -f 2 -d ' ' | grep '^/mnt/knx$' || sudo mount /mnt/knx
if ! cat /etc/mtab | cut -f 2 -d ' ' | grep '^/mnt/knx$' ; then
    echo "/mnt/knx is not mounted and cannot be" >&2
    exit 2
fi

cat /etc/mtab | cut -f 2 -d ' ' | grep '^/mnt/knoppixmerged$' || sudo mount /mnt/knoppixmerged
if ! cat /etc/mtab | cut -f 2 -d ' ' | grep '^/mnt/knoppixmerged$' ; then
    echo "/mnt/knoppixmerged is not mounted and cannot be" >&2
    exit 3
fi

time mkisofs -l -r -J \
    -V "BROPPIX_$(dt)"   \
    -b boot/isolinux/isolinux.bin \
    -no-emul-boot \
    -boot-load-size 4 \
    --boot-info-table \
    -c boot/isolinux/boot.cat \
    -o /mnt/DATA_004/data/os/knoppix/knoppix_remastered_$(dt).iso \
    /mnt/knoppixmerged
