#!/bin/bash
# Bryant Hansen

# /mnt/DATA_004/data/os
# gink@verona2 -> qemu-system-x86_64 -m 2048M -cdrom knoppix/KNOPPIX_V7.4.2DVD-2014-09-28-EN.iso -boot d -hdb 30GBa.qcow2

# To pass all available host processor features to the guest, use the command line switch
#  qemu -cpu host
# if you wish to retain compatibility, you can expose selected features to your guest. If all your hosts have these features, compatibility is retained:
#  qemu -cpu qemu64,+ssse3,+sse4.1,+sse4.2,+x2apic


DIR=/mnt/DATA_004/data/os
ISO="$DIR"/tails/tails-i386-2.4/tails-i386-2.4.iso
HD="$DIR"/30GBa.qcow2

#su - gink -c \

    pushd "$DIR" && \
    (
        set -o verbose
        qemu-system-x86_64 \
            -enable-kvm \
            -m 2048M \
            -boot d \
            -cdrom "$ISO" \
            -hdb "$HD"
        set +o verbose
        popd > /dev/null
    )

#            -full-screen \
#            -enable-kvm \
#            -cpu host \

# http://www.linux-kvm.org/page/Tuning_KVM
# https://wiki.archlinux.org/index.php/QEMU#Enabling_KVM
# https://wiki.archlinux.org/index.php/KVM
# http://www.linux-kvm.org/page/FAQ#General_KVM_information
# https://en.wikibooks.org/wiki/QEMU/Monitor

