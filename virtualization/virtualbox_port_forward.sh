#!/bin/bash
# Bryant Hansen

USAGE="$0  VM_NAME  PROTOCOL  GUEST_PORT  HOST_PORT  [ NETWORK_INTERFACE [ BIND_IP ] ]"
VERBOSE=1

if [[ "$#" -lt 4 ]] ; then
    echo "$0 ERROR: this function requires at least 4 arguments." >&2
    echo "USAGE: $USAGE" >&2
    echo "" >&2
    exit 2
fi

VM_NAME="$1"

# PROTOCOL=UDP
# PROTOCOL=ICMP
# PROTOCOL=TCP
PROTOCOL="$2"

# GUEST_PORT=80
# GUEST_PORT=22
GUEST_PORT="$3"

# HOST_PORT=8080
# HOST_PORT=8022
HOST_PORT="$4"

# NETWORK_INTERFACE=pcnet
NETWORK_INTERFACE=e1000
[[ "$5" ]] && NETWORK_INTERFACE=$5

# BIND_IP="10.0.0.1"
[[ "$6" ]] && BIND_IP=$6


######################################################
# Validation
######################################################

# check for executable
if ! which VBoxManage ; then
    echo "$0 ERROR: VBoxManage not found in path.  Install or update PATH variable." >&2
    echo "  Exiting abnormally" >&2
    exit 3
fi

# check for VM Name
# vbm list vms | while read l ; do l2="${l#\"}" ; l3=${l2%\"*} ; echo "$l3" ; done
VBoxManage list vms | ( \
    unset found
    while read l ; do
        l2="${l#\"}"
        l3="${l2%\"*}"
        [[ "$l3" == "$VM_NAME" ]] && echo "VM name $VM_NAME found!" >&2 && found=1 && break
    done
    if [[ ! "$found" ]] ; then
        echo "VM_NAME is not found in the list of vm names provided by 'VBoxManage list vms'" >&2
        exit 4
    fi
) || exit 4

# check if protocol is valid
case "$PROTOCOL" in
    TCP|UDP|ICMP)
        ;;
    *)
        echo "$0 ERROR: Unknown protocol: $PROTOCOL" >&2
        echo "USAGE: $USAGE" >&2
        echo "Exiting abnormally" >&2
        echo "" >&2
        exit 5
esac

# check if guest port seems valid
if [[ "$GUEST_PORT" -lt 1 ]] || [[ "$GUEST_PORT" -gt 65536 ]] ; then
    echo "$0 ERROR: Invalid Guest Port: $GUEST_PORT" >&2
    echo "USAGE: $USAGE" >&2
    echo "Exiting abnormally" >&2
    echo "" >&2
    exit 6
fi

# check if host port seems valid
if [[ "$HOST_PORT" -lt 1 ]] || [[ "$HOST_PORT" -gt 65536 ]] ; then
    echo "$0 ERROR: Invalid Host Port: $HOST_PORT" >&2
    echo "USAGE: $USAGE" >&2
    echo "Exiting abnormally" >&2
    echo "" >&2
    exit 7
fi

# check if network interface seems valid
case "$NETWORK_INTERFACE" in
    e1000|pcnet)
        ;;
    *)
        echo "$0 ERROR: Unknown network interface: $NETWORK_INTERFACE" >&2
        echo "USAGE: $USAGE" >&2
        echo "Exiting abnormally" >&2
        echo "" >&2
        exit 8
esac

# TODO: validate BIND_IP address

######################################################
# Main
######################################################

[[ "$VERBOSE" ]] && [[ "$VERBOSE" -ne 0 ]] && VBoxManage getextradata "$VM_NAME" enumerate

set -o verbose
echo ""
# now do it!
VBoxManage setextradata "$VM_NAME" "VBoxInternal/Devices/${NETWORK_INTERFACE}/0/LUN#0/Config/guestssh/Protocol"  $PROTOCOL
VBoxManage setextradata "$VM_NAME" "VBoxInternal/Devices/${NETWORK_INTERFACE}/0/LUN#0/Config/guestssh/GuestPort" $GUEST_PORT
VBoxManage setextradata "$VM_NAME" "VBoxInternal/Devices/${NETWORK_INTERFACE}/0/LUN#0/Config/guestssh/HostPort"  $HOST_PORT

set +o verbose
[[ "$VERBOSE" ]] && [[ "$VERBOSE" -ne 0 ]] && VBoxManage getextradata "$VM_NAME" enumerate

exit 0


######################################################
# Notes
######################################################

# Some examples:

# https://forums.virtualbox.org/viewtopic.php?f=5&t=37576
# VBoxManage setextradata "Guest VM" "VBoxInternal/Devices/e1000/0/LUN#0/Config/guestssh/Protocol" TCP
# VBoxManage setextradata "Guest VM" "VBoxInternal/Devices/e1000/0/LUN#0/Config/guestssh/GuestPort" 22
# VBoxManage setextradata "Guest VM" "VBoxInternal/Devices/e1000/0/LUN#0/Config/guestssh/HostPort" 2222
# VBoxManage setextradata "Guest VM" "VBoxInternal/Devices/e1000/0/LUN#0/Config/guestssh/BindIP" "10.0.0.1"

# http://sethkeiper.com/2008/01/05/virtualbox-port-forwarding-with-linux-host/
# VBoxManage setextradata "name of vm" "VBoxInternal/Devices/pcnet/0/LUN#0/Config/ssh/HostPort" 2222
# VBoxManage setextradata "name of vm" "VBoxInternal/Devices/pcnet/0/LUN#0/Config/ssh/GuestPort" 22
# VBoxManage setextradata "name of vm" "VBoxInternal/Devices/pcnet/0/LUN#0/Config/ssh/Protocol" TCP
