#!/bin/bash
# Bryant Hansen

#Notes on waking the screen up

#didn't work; try qdbus Application quit and xset ??? && xset -reset
#setterm -blank poke | tee /dev/tty1
#echo -ne "\033[9;0]" >/dev/tty1

export DISPLAY=:0
xset dpms force on
xset s reset
qdbus \
| grep kscreenlocker_greet \
| xargs -I {} qdbus {} /MainApplication quit

