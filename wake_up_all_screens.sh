#!/bin/sh
# Bryant Hansen
# wf8c -> echo 0 > /sys/class/graphics/fb0/blank

cat  ~/.screens.lst \
| sed "s/\#.*//;/^[ \t]*$/d" \
| while read host ; do
    if ls /sys/class/graphics/fb[0-9]*/blank ; then
        ssh $host "echo 0 > /sys/class/graphics/fb0/blank"
#            for fb in /sys/class/graphics/fb[0-9]*/blank
#                echo 0 > $fb
#            done
#        ' &
    else
        echo "ERROR: no /sys/class/graphics/fb[0-9]*/blank interface found" >&2
        exit 2
    fi
done



