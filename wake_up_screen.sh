#!/bin/sh
# Bryant Hansen
#wf8c -> echo 0 > /sys/class/graphics/fb0/blank 

if ls /sys/class/graphics/fb[0-9]*/blank ; then
    for fb in /sys/class/graphics/fb[0-9]*/blank
        echo 0 > $fb
    done
else
    echo "ERROR: no /sys/class/graphics/fb[0-9]*/blank interface found" >&2
    exit 2
fi
