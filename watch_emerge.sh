#!/bin/bash
# Bryant Hansen

t=6
[[ "$1" ]] && t=$1

f="$(ls -1t emerge_*.log | head -n 1)"
echo "f=$f" >&2

#ps -eo uname,pid,ppid,nlwp,pcpu,pmem,psr,start_time,tty,time,args --sort -pcpu,-pmem | head -n 20

watch -n $t " \
    tail -n 5 /var/log/emerge.log | feda ; \
    echo '' ; \
    top -b -n 1 | head -n 12 ; \
    echo '' ; \
    echo '### TOP MEM USERS ###' ; \
    ps -eo pid,uname,ppid,nlwp,pcpu,pmem,psr,start_time,tty,time,args --sort -pmem,-pcpu | head -n 4 ; \
    echo '' ; \
    echo '### EMERGE LOGFILE: ${f} ###' ; \
    tail -n 30 $f | tac | sed 's/^/\ /' \
"

#echo '### TOP CPU USERS ###' ; \
    #ps -eo uname,pid,ppid,nlwp,pcpu,pmem,psr,start_time,tty,time,args --sort -pcpu,-pmem | head -n 5 ; \
    #echo '' ; \
    #echo '### TOP MEM USERS ###' ; \
    #ps -eo uname,pid,ppid,nlwp,pcpu,pmem,psr,start_time,tty,time,args --sort -pmem,-pcpu | head -n 3 ; \
