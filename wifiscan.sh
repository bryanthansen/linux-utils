#!/bin/bash
# Bryant Hansen

# initial 1-liner:
# iwlist scan 2>/dev/null | grep "SSID\|crypt" | while read l ; do [[ ! "${l%Encryption*}" ]] && echo -e -n  "$l \t" ; [[ ! "${l%ESSID*}" ]] && echo "$l" ; done

iwlist scan 2>&1 | \
    grep "SSID\|crypt" | \
    while read l ; do
        [[ ! "$1" ]] && continue
        [[ ! "${l%%Encryption*}" ]] && echo -e -n  "$l \t"
        [[ ! "${l%%ESSID*}" ]] && echo "$l"
    done
