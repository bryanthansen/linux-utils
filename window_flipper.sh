#!/bin/bash
# Bryant Hansen

# Description:
# Launch a series of windows and flip through them

ME="$(basename "$0")"
CHROME="chromium --kiosk --new-window"
URLS="
    http://thehill.com/
    http://politico.com/
    http://guardian.co.uk/
    http://motherjones.com/
    http://washingtonpost.com/
    http://ams.com/eng/Investor/Share-Trading-Information
"
VERBOSE=""
LOAD_DELAY=3
LOOP_DELAY=6

function log() {
    printf "${ME}: $(date "+%Y%m%d_%H%M%S.%N"): $*\n" >&2
}

# This function launches launches and application and tries to determine the window ID of the newly-created window
# The only known way to do this is a bit unreliable; this waits for the first change in the activeWindow and assumes that this is the newly-created one
# TODO: determine a more-reliable method for this
function launch_and_wait_for_window_change() {
    ret=1
    startwin=$(xdotool getactivewindow)
    log "> $CHROME $1  --  startwin=$startwin"
    $CHROME $1 >> chrome.log 2>&1 &
    pid=$!
    log "'${CHROME} ${1}' launched with pid $pid"
    # TODO: something more-deterministic *must* be done to get the window that this process is launched in
    for i2 in {1..10} ; do
        wid=$(xdotool getactivewindow)
        if [[ "$wid" != "$startwin" ]] ; then
            #log " *** Window change detected $startwin -> $wid" >&2
            echo "$wid"
            ret=0
            break
        else
            printf " $i2 " >&2
        fi
        log "launch_and_wait_for_window_change: wait 0.5s and try again"
        sleep 0.5
    done
    if [[ "$ret" == 1 ]] ; then
        log " *** Window change NOT detected from $startwin"
    fi
    return $ret
}

MAX_URLS=10
function launch_windows() {
    num_urls=0
    log "launch_windows $1"
    printf "${1}\n" \
    | while read url ; do
        [[ ! "$url" ]] && continue
        log "Launching ${url}..."
        wid=$(launch_and_wait_for_window_change "$url")
        echo "$wid "
        wpid="$(xdotool getwindowpid $wid)"
        wname="$(xdotool getwindowname $wid)"
        log "activewindow2 (${url}): wid=${wid}, wpid=${wpid}"
        sleep "$LOAD_DELAY"
        (( num_urls++ ))
        (( $num_urls > $MAX_URLS )) && break
    done
}

# This function loops through windows, activating them one at a time and then waiting, providing a slideshow-like effect
# TODO: for browser windows, consider a background refresh
function loop_windows() {
    log "loop $1"
    for n in {1..5} ; do
        printf "${1}\n" \
        | while read wid ; do
            [[ ! "$wid" ]] && continue
            log "activate $wid"
            # TODO: detect death and terminate
            if ! xdotool windowactivate $wid ; then
                log "xdotool windowactivate failed, code $?"
                return 1
                break
            fi
            sleep $LOOP_DELAY
        done || break
        read -t 0 -n 1 'keystroke'
        [[ "$keystroke" ]] && echo "keystroke=$keystroke" >&2
        unset keystroke
    done
}

# Main

# Turn off blanking
setterm -blank 0
#setterm -powerdown 0
#setterm -powersave 0

window_ids=$(launch_windows "$URLS")
if [[ "$VERBOSE" ]] && [[ "$VERBOSE" != "0" ]] ; then
    log "window_ids=$window_ids"
    ps auwxw 2>&1 | grep chrom | cut -c1,180
fi
loop_windows "$window_ids"
